<?php
/*
    Read XML structure to associative array
    --
    Using:
    $xml = new XMLReader();
    $xml->open([XML file]);
    $assoc = xml2assoc($xml);
    $xml->close();
*/
function xml2assoc($xml) {
    $assoc = null;
    while($xml->read()){
        switch ($xml->nodeType) {
            case XMLReader::END_ELEMENT: return $assoc;
            case XMLReader::ELEMENT:
                $assoc[$xml->name][] = array('value' => $xml->isEmptyElement ? '' : xml2assoc($xml));
                if($xml->hasAttributes){
                    $el =& $assoc[$xml->name][count($assoc[$xml->name]) - 1];
                    while($xml->moveToNextAttribute()) $el['attributes'][$xml->name] = $xml->value;
                }
                break;
            case XMLReader::TEXT:
            case XMLReader::CDATA: $assoc .= $xml->value;
        }
    }
    return $assoc;
}
$xml = new XMLReader();
$xml->open('php://input');
    $assoc = xml2assoc($xml);
    $xml->close();
//file_put_contents('test3.txt',fopen('php://input', 'r'));
file_put_contents('test3.txt',var_export($assoc['operation'][0]['value']['state'][0]['value'],1));
?>
OK