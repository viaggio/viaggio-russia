<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('../../tcpdf/examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print
$html = <<<EOD
<table border="1" style="padding: 10px;">
	<tr>
		<td>
			<p><b>Инвойс № 1497369481 от «13 » июня 2017 г.</b></p>

<p>по договору о реализации туристского продукта № <b>1497369481</b> от «13» июня 2017 г. (далее – Договор)</p>

<p><b>Туроператор:</b> ООО Международная Компания Русский Тур</p>

<p><b>Заказчик:</b> Nome e Gongnome</p>

<p>В соответствии с Договором оплате подлежат следующие услуги</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№ п/п
</td>
<td>
Наименование услуги
</td>
<td>
Цена, Евро
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Визовая поддержка
</td>
<td>
5000
</td>
</tr>
<tr>
<td colspan="2">
ИТОГО к оплате
</td>
<td>
5000
</td>
</tr>
</table>
</p>

<p>Срок оплаты: в соответствии с условиями Договора.</p>

<p><b>Банковские реквизиты:</b></p>

<p><b>Russian Tour International Ltd.</b>
Account: 40702978880000032988<br/>
JSB «ROSEVROBANK» (JSC), Moscow, Russia<br/>
IN FAVOUR OF ST. PETERSBURG BRANCH OF ROSEVROBANK; SWIFT: COMKRUMM080
Correspondent Bank: UBS Switzerland AG, Zurich, Switzerland; SWIFT: UBSWCHZH80A</p>

<p>В назначении платежа должен быть указан № инвойса и его дата.</p>

<p><b>Настоящий инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором и признается Сторонами в качестве акта выполненных работ.</b></p>

<p>Генеральный директор ООО Международная Компания Русский Тур</p>

<p>___________________О. Н. Черемшенко</p>
		</td>
		<td>
		
		
		<p><b>Fattura № 1497369481 del “13” giugno 2017</b></p>

<p>in base al contratto di realizzazione di un prodotto turistico № <b>1497369481</b> del “13” giugno 2017 (di seguito: Contratto).</p>

<p><b>Tour operator: Russian Tour International Ltd.</b></p>

<p><b>Committente:</b> Nome e Gongnome</p>

<p>Ai sensi del Contratto sono soggetti a pagamento i seguenti servizi</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№
</td>
<td>
Denominazione del servizio
</td>
<td>
Prezzo, Euro
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Assistenza visto
</td>
<td>
5000
</td>
</tr>
<tr>
<td colspan="2">
TOTALE da pagare
</td>
<td>
5000
</td>
</tr>
</table>
</p>

<p>Termine di pagamento: in ottemperanza alle condizioni del Contratto.</p>

<p><b>Coordinate bancarie:</b></p>

<p><b>Russian Tour International Ltd.</b><br/>
Account : 40702978880000032988<br/>
JSB «ROSEVROBANK» (JSC), Moscow, Russia
IN FAVOUR OF ST. PETERSBURG BRANCH OF ROSEVROBANK; SWIFT: COMKRUMM080
Correspondent Bank: UBS Switzerland AG, Zurich, Switzerland; SWIFT: UBSWCHZH80A</p>

<p>Nella causale del pagamento deve essere indicato il № della fattura e la data.</p>

<p><b>
La presente fattura dal momento del suo pagamento totale da parte del Committente conferma la prestazione dei servizi da parte del Tour Operator ed è riconosciuta dalle Parti come atto di esecuzione dei lavori.</b></p>

<p>Il Direttore Generale di Russian Tour International Ltd.</p>

<p>___________________O. N. Cheremshenko</p>
		
		
		
		</td>
	</tr>
</table>
EOD;

$pdf->writeHTML($html, true, false, true, false, '');
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
