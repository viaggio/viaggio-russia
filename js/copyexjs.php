<?php
 
function downloadJs($file_url, $save_to)
{
$arrContextOptions=array(
"ssl"=>array(
"verify_peer"=>false,
"verify_peer_name"=>false,
),
);
$content = file_get_contents($file_url, false, stream_context_create($arrContextOptions));
file_put_contents($save_to, $content);
}
// Указываем URL, затем папку от корня сайта и имя файла с расширением.
// Проверьте чтобы на папке были права на запись 777/755
// Метрика
downloadJs('https://mc.yandex.ru/metrika/watch.js', realpath("./js") . '/home/viaggio/web/viaggio-russia.com/public_html/js/watch.js');
 
// Google Analytics
downloadJs('http://www.google-analytics.com/analytics.js', realpath("./js") . '/home/viaggio/web/viaggio-russia.com/public_html/js/analytics.js');
 
// Для скриптов без расширения
downloadJs('http://code.jivosite.com/script/widget/NuT1gBLsC6', realpath("./js") . '/home/viaggio/web/viaggio-russia.com/public_html/js/NuT1gBLsC6');
 
?>