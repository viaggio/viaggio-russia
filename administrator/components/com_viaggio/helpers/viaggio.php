<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Viaggio helper.
 *
 * @since  1.6
 */

class ViaggioHelpersViaggio
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_VIAGGIO_TITLE_TOURS'),
			'index.php?option=com_viaggio&view=tours',
			$vName == 'tours'
		);

		JHtmlSidebar::addEntry(
			JText::_('JCATEGORIES') . ' (' . JText::_('COM_VIAGGIO_TITLE_TOURS') . ')',
			"index.php?option=com_categories&extension=com_viaggio",
			$vName == 'categories'
		);
		if ($vName=='categories') {
			JToolBarHelper::title('Viaggio: JCATEGORIES (COM_VIAGGIO_TITLE_TOURS)');
		}
		
		JHtmlSidebar::addEntry(
			JText::_('COM_VIAGGIO_TITLE_ORDERS'),
			'index.php?option=com_viaggio&view=orders',
			$vName == 'orders'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_VIAGGIO_TITLE_MANUALPAYMENTS'),
			'index.php?option=com_viaggio&view=manualpayments',
			$vName == 'manualpayments'
		);

		
		JHtmlSidebar::addEntry(
			JText::_('COM_VIAGGIO_TITLE_CLIENTS'),
			'index.php?option=com_viaggio&view=clients',
			$vName == 'clients'
		);
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_viaggio';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}

    public static function sendMail($data=array(),$path='',$visaId=0,$prepareOnly=false,$sendOnly=0,$useShablon=true,$body=false,$subject=false,$copyToAdmin=true){
        if ($path) {
            include($path);
        }
        if ($body)
            if ($useShablon) include('/home/viaggio/web/viaggio-russia.com/public_html/components/com_viaggio/views/mails/shablon.php');
        if (isset($_GET['debug']))
        {
            var_dump($body);
            exit;
        }
        $db=JFactory::getDBO();
        if (!$prepareOnly)
        {
            if ($sendOnly){
                $query = 'SELECT * FROM #__viaggio_email WHERE `prepare` = 1 AND visa_id = '.$sendOnly. ' LIMIT 1' ;
                $db->setQuery($query);
                $email = $db->loadAssoc();
                if (isset($email['email']))
                {
                    $data['email'] = $email['email'];
                    $body = $email['body'];

                    $subject = $email['subject'];
                }
            }

            if (isset($data['email']))
            {
                $mailer = JFactory::getMailer();
                $config = JFactory::getConfig();
                $sender = array(
                    $config->get( 'mailfrom' ),
                    $config->get( 'fromname' )
                );

                $mailer->setSender($sender);
                $mailer->isHTML(true);
                if (is_string($data['email']))
                    $mailer->addRecipient($data['email']);
                elseif (is_array($data['email']))
                {
                    foreach ($data['email'] as $email)
                        $mailer->addRecipient($email);
                }
                if ($copyToAdmin)
                    $mailer->addRecipient('mail@russiantour.com');
                $mailer->setSubject($subject);
                $mailer->setBody($body);
                if (isset($data['attachment']))
                {
                    if (!is_array($data['attachment']))
                    {
                        $mailer->addAttachment($data['attachment']);
                    }
                    else
                    {
                        foreach ($data['attachment'] as $path)
                        {
                            $mailer->addAttachment($path);
                        }
                    }
                }
                $send = $mailer->Send();

                if ($sendOnly && isset($email['id']) && $send){
                    $db = JFactory::getDBO();
                    $query = 'UPDATE #__viaggio_email '.
                        'SET `prepare` = 0 '.
                        'WHERE id = '.intval($email['id']);
                    $db->setQuery($query);
                    $db->execute();
                }
            }
        }
        if (!$sendOnly && $data['email'])
        {
            if (is_array($data['email']))
                $data['email'] = $data['email'][0];
            if ($prepareOnly){
                $query = 'UPDATE #__viaggio_email SET `email` = \''.$data['email'].'\' WHERE `email` <> \''.$data['email'].'\' AND visa_id = '.$visaId. ' AND `prepare` = 1';
                $db->setQuery($query);
                $db->execute();

                if (isset($subject))
                {
                    $query = 'SELECT * FROM #__viaggio_email WHERE visa_id = '.$visaId. ' AND `subject`=\''.addslashes($subject).'\' AND `email` <> \''.$data['email'].'\' AND `prepare` = 0 ORDER BY id DESC LIMIT 1' ;
                    $db->setQuery($query);
                    $email = $db->loadAssoc();
                }
                if (!isset($subject) || isset($email['email'])){
                    $body = '';
                    $subject = '';
                    $prepareOnly = false;
                }
            }

            $logEmail = new stdClass();
            $logEmail->subject = $subject;
            $logEmail->body = $body;
            $logEmail->visa_id = $visaId;
            $logEmail->email = $data['email'];
            if ($prepareOnly)
                $logEmail->prepare = 1;
            JFactory::getDbo()->insertObject('#__viaggio_email', $logEmail, 'id');
        }
    }

    public static function getCost($item)
    {
        $subtype = 0;
        $count = 2;
        $age1 = 18;
        $age2 = 18;
        $halfboard = 0;
        $needvisamongolia = 0;
        $needvisachina = 0;
        $formula = 0;

        //38 параметров калькулятора
        //hotel
        $inn4cost = $item->inn4cost;
        $inn3cost = $item->inn3cost;
        $halfboard4 = $item->halfboard4;
        $halfboard3 = $item->halfboard3;
        $singola3 = $item->singola3;
        $singola4 = $item->singola4;

        //cruise
        $nostell1 = $item->nostell1;
        $nostell2 = $item->nostell2;

        //transib
        $classic = $item->classic;
        $classicsingola = $item->classicsingola;
        $superior = $item->superior;
        $superiorsingola = $item->superiorsingola;
        $superiorplus = $item->superiorplus;
        $superiorplussingola = $item->superiorplussingola;
        $nostalgic = $item->nostalgic;
        $nostalgicsingola = $item->nostalgicsingola;
        $bolshoy4 = $item->bolshoy4;
        $bolshoy4singola = $item->bolshoy4singola;
        $platinum5 = $item->platinum5;
        $platinum5singola = $item->platinum5singola;
        $russiandays = $item->russiandays;

        $params = json_decode($item->params);
        //component settings
        $servicecost = $params->servicecost;//стоимость услуг
        $statndarvisa = $params->statndarvisa;//консульский сбор
        $urgentevisa = $params->urgentevisa;//срочный конс сбор
        $sendvisa = $params->sendvisa;//доставка
        $visa7day = $params->visa7day; //Стандартная страховка выдаеться на 7 дней. Она входит в формулу. Очень важно в формуле :  от 0(лет) до 3(лет)  умножаем на 2 _____ от 3(лет) до 65(лет)   умножаем на 0 ____  от 65(лет) до 80(лет) умножаем на 2 ___  от 80 (лет) до  вечности умножаем на 3
        $visa1day = $params->visa1day;
        $visamongolia = $params->visamongolia;
        $visachina = $params->visachina;

        //drive
        $type = 'hotel';
        $urgent = 0; //срочность

        //дней до начала тура
        $daysTo = (strtotime( $item->from )-time())/3600/24;

        //продолжительность тура
        $daysFromTo = ( ( strtotime( $item->to )-strtotime( $item->from ) ) / 3600 / 24 ) +1;

        //if($daysTo < 10) return 0;

        //срочность, 20 или меньше дней, то срочно
        if($daysTo <= 20 && $daysTo >= 10) $urgent = 1;

        //тип тура
        if($nostell1 > 1) $type = 'cruise';
        if($russiandays > 1){ $type = 'transib'; }else{ $russiandays = $daysFromTo; }


        if($subtype == NULL)
        {
            if($type == 'cruise'){
                $subtype = '';
                if($nostell1 == 0 OR $nostell2 == 0) return 0;
            }else if($type == 'transib')
            {
                if($classic > 0){
                    $subtype = 'classic';
                }else{
                    if($superior > 0){
                        $subtype = 'superior';
                    }else{
                        if($superiorplus > 0){
                            $subtype = 'superiorplus';
                        }else{
                            if($nostalgic > 0){
                                $subtype = 'nostalgic';
                            }else{
                                if($bolshoy4 > 0){
                                    $subtype = 'bolshoy4';
                                }else{
                                    if($platinum5 > 0){
                                        $subtype = 'platinum5';
                                    }else{
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }else if($type == 'hotel')
            {
                if($inn3cost > 0){
                    $subtype = '3';
                }else{
                    if($inn4cost > 0){
                        $subtype = '4';
                    }else{
                        return 0;
                    }
                }
            }
        }

        return self::getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula);
    }

    public static function getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula)
    {
        /*echo "<script type='text/javascript'>console.log('inn4cost:$inn4cost,inn3cost:$inn3cost,halfboard4:$halfboard4,halfboard3:$halfboard3,singola4:$singola4,singola3:$singola3, type:$type, subtype:$subtype, count:$count, halfboard:$halfboard, nostell1:$nostell1, nostell2:$nostell2, classic:$classic,classicsingola:$classicsingola,superior:$superior,superiorsingola:$superiorsingola,superiorplus:$superiorplus,superiorplussingola:$superiorplussingola,nostalgic:$nostalgic,nostalgicsingola:$nostalgicsingola,bolshoy4:$bolshoy4,bolshoy4singola:$bolshoy4singola,platinum5:$platinum5,platinum5singola:$platinum5singola,russiandays:$russiandays, servicecost:$servicecost,statndarvisa:$statndarvisa,urgentevisa:$urgentevisa,sendvisa:$sendvisa,visa7day:$visa7day,visa1day:$visa1day,visamongolia:$visamongolia,visachina:$visachina, age1:$age1, age2:$age2, needvisamongolia:$needvisamongolia, needvisachina:$needvisachina, urgent:$urgent');</script>";*/
        $result = 0;
        $log = "type($type)|subtype($subtype)|";
        if($type == 'hotel')
        {
            //$result = 'hotel';
            if($subtype == 3)
            {
                //$result = '3*';
                if($count == 1)
                {
                    $log .= "+ singola3($singola3)";
                    $result = $result + $singola3;
                    if($halfboard){
                        $result = $result + $halfboard3;
                        $log .= "+ halfboard3($halfboard3)";
                    }
                }else if($count == 2)
                {
                    $log .= "+ inn3cost*2($inn3cost*2)";
                    $result = $result + ( $inn3cost * 2 );
                    if($halfboard){
                        $result = $result + ( $halfboard3*2 );
                        $log .= "+ halfboard3*2($halfboard3*2)";
                    }
                }
            }else if($subtype == 4)
            {
                //$result = '4*';
                if($count == 1)
                {
                    $log .= "+ singola4($singola4)";
                    $result = $result + $singola4;
                    if($halfboard){
                        $result = $result + $halfboard4;
                        $log .= "+ halfboard4($halfboard4)";
                    }
                }else if($count == 2)
                {
                    $log .= "+ inn4cost*2($inn4cost*2)";
                    $result = $result + ( $inn4cost * 2 );
                    if($halfboard){
                        $result = $result + ( $halfboard4*2 );
                        $log .= "+ halfboard4*2($halfboard4*2)";
                    }
                }
            }
        }else if($type == 'cruise')
        {
            //$result = 'cruise';
            if($count == 1)
            {
                $log .= "+( nostell1($nostell1) )";
                $result = $result + $nostell1;
            }else if($count == 2)
            {
                $log .= "+( nostell2($nostell2) )";
                $result = $result + ( $nostell2 * 2 );
            }
        }else if($type == 'transib')
        {
            //$result = 'transib';
            if($subtype == 'classic')
            {
                //$result = 'classic';
                if($count == 1)
                {
                    $log .= "+( classicsingola($classicsingola) )";
                    $result = $result + $classicsingola;
                }else if($count == 2)
                {
                    $log .= "+( classic*2($classic*2) )";
                    $result = $result + ( $classic * 2 );
                }
            }else if($subtype == 'superior')
            {
                //$result = 'superior';
                if($count == 1)
                {
                    $log .= "+( superiorsingola($superiorsingola) )";
                    $result = $result + $superiorsingola;
                }else if($count == 2)
                {
                    $log .= "+( superior*2($superior*2) )";
                    $result = $result + ( $superior * 2 );
                }
            }else if($subtype == 'superiorplus')
            {
                //$result = 'superiorplus';
                if($count == 1)
                {
                    $log .= "+( superiorplussingola($superiorplussingola) )";
                    $result = $result + $superiorplussingola;
                }else if($count == 2)
                {
                    $log .= "+( superiorplus*2($superiorplus*2) )";
                    $result = $result + ( $superiorplus * 2 );
                }
            }else if($subtype == 'nostalgic')
            {
                //$result = 'nostalgic';
                if($count == 1)
                {
                    $log .= "+( nostalgicsingola($nostalgicsingola) )";
                    $result = $result + $nostalgicsingola;
                }else if($count == 2)
                {
                    $log .= "+( nostalgic*2($nostalgic*2) )";
                    $result = $result + ( $nostalgic * 2 );
                }
            }else if($subtype == 'bolshoy4')
            {
                //$result = 'bolshoy4';
                if($count == 1)
                {
                    $log .= "+( bolshoy4singola($bolshoy4singola) )";
                    $result = $result + $bolshoy4singola;
                }else if($count == 2)
                {
                    $log .= "+( bolshoy4*2($bolshoy4*2) )";
                    $result = $result + ( $bolshoy4 * 2 );
                }
            }else if($subtype == 'platinum5')
            {
                //$result = 'platinum5';
                if($count == 1)
                {
                    $log .= "+( platinum5singola($platinum5singola) )";
                    $result = $result + $platinum5singola;
                }else if($count == 2)
                {
                    $log .= "+( platinum5*2($platinum5*2) )";
                    $result = $result + ( $platinum5 * 2 );
                }
            }
        }

        //стоимость услуг
        if($count == 1)
        {
            $log .= "+servicecost($servicecost)";
            $result = $result + $servicecost;
        }else if($count == 2)
        {
            $log .= "+servicecost*2($servicecost*2)";
            $result = $result + ( $servicecost * 2 );
        }

        //доставка
        $result = $result + $sendvisa;
        $log .= "+sendvisa($sendvisa)";

        if($needvisamongolia)
        {
            if($count == 1)
            {
                $log .= "+visamongolia($visamongolia)";
                $result = $result + $visamongolia;
            }else if($count == 2)
            {
                $log .= "+visamongolia*2($visamongolia*2)";
                $result = $result + ( $visamongolia * 2 );
            }
        }

        if($needvisachina)
        {
            if($count == 1)
            {
                $log .= "+visachina($visachina)";
                $result = $result + $visachina;
            }else if($count == 2)
            {
                $log .= "+visachina*2($visachina*2)";
                $result = $result + ( $visachina * 2 );
            }
        }

        //если срочный консульский сбор
        if($urgent)
        {
            if($count == 1)
            {
                $log .= "+urgentevisa($urgentevisa)";
                $result = $result + $urgentevisa;
            }else if($count == 2)
            {
                $log .= "+urgentevisa*2($urgentevisa*2)";
                $result = $result + ( $urgentevisa * 2 );
            }
        }

        //консульский сбор
        if($count == 1)
        {
            $log .= "+statndarvisa($statndarvisa)";
            $result = $result + $statndarvisa;
        }else if($count == 2)
        {
            $log .= "+statndarvisa*2($statndarvisa*2)";
            $result = $result + ( $statndarvisa * 2 );
        }

        $log .= "|russiandays:$russiandays|";

        //если стандартная виза или если стандартная виза + доп. виза
        if($russiandays < 8)
        {
            $log .= "|стандартная виза 7 дней|";
            if($age1 < 3)
            {
                $log .= "+visa7day*2($visa7day*2)";
                $result = $result + ($visa7day * 2);
            }else if($age1 < 65 && $age1 >= 3)
            {
                $log .= "+visa7day*1($visa7day*1)";
                $result = $result + ($visa7day * 1);
            }else if($age1 < 80 && $age1 >= 65)
            {
                $log .= "+visa7day*2($visa7day*2)";
                $result = $result + ($visa7day * 2);
            }else if($age1 >= 80)
            {
                $log .= "+visa7day*3($visa7day*3)";
                $result = $result + ($visa7day * 3);
            }

            if($count == 2)
            {
                if($age2 < 3)
                {
                    $log .= "+visa7day*2($visa7day*2)";
                    $result = $result + ($visa7day * 2);
                }else if($age2 < 65 && $age2 >= 3)
                {
                    $log .= "+visa7day*1($visa7day*1)";
                    $result = $result + ($visa7day * 1);
                }else if($age2 < 80 && $age2 >= 65)
                {
                    $log .= "+visa7day*2($visa7day*2)";
                    $result = $result + ($visa7day * 2);
                }else if($age2 >= 80)
                {
                    $log .= "+visa7day*3($visa7day*3)";
                    $result = $result + ($visa7day * 3);
                }
            }
        }else if($russiandays > 7)
        {
            $log .= "|стандартная виза 7 дней+доп|";
            if($age1 < 3)
            {
                $log .= "+visa7day*2($visa7day*2)";
                $result = $result + ($visa7day * 2);
                $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
                $result = $result + ( ($russiandays - 7) * $visa1day * 2 );
            }else if($age1 < 65 && $age1 >= 3)
            {
                $log .= "+visa7day*1($visa7day*1)";
                $result = $result + ($visa7day * 1);
                $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
                $result = $result + ( ($russiandays - 7) * $visa1day * 1 );
            }else if($age1 < 80 && $age1 >= 65)
            {
                $log .= "+visa7day*2($visa7day*2)";
                $result = $result + ($visa7day * 2);
                $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
                $result = $result + ( ($russiandays - 7) * $visa1day * 2 );
            }else if($age1 >= 80)
            {
                $log .= "+visa7day*3($visa7day*3)";
                $result = $result + ($visa7day * 3);
                $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
                $result = $result + ( ($russiandays - 7) * $visa1day * 3 );
            }

            if($count == 2)
            {
                if($age2 < 3)
                {
                    $log .= "+visa7day*2($visa7day*2)";
                    $result = $result + ($visa7day * 2);
                    $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
                    $result = $result + ( ($russiandays - 7) * $visa1day * 2 );
                }else if($age2 < 65 && $age2 >= 3)
                {
                    $log .= "+visa7day*1($visa7day*1)";
                    $result = $result + ($visa7day * 1);
                    $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
                    $result = $result + ( ($russiandays - 7) * $visa1day * 1 );
                }else if($age2 < 80 && $age2 >= 65)
                {
                    $log .= "+visa7day*2($visa7day*2)";
                    $result = $result + ($visa7day * 2);
                    $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
                    $result = $result + ( ($russiandays - 7) * $visa1day * 2 );
                }else if($age2 >= 80)
                {
                    $log .= "+visa7day*3($visa7day*3)";
                    $result = $result + ($visa7day * 3);
                    $log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
                    $result = $result + ( ($russiandays - 7) * $visa1day * 3 );
                }
            }
        }
        $user = JFactory::getUser();
        if(!$user->guest && in_array(8,$user->groups)){
            if(isset($_REQUEST['view']) ){
                if($_REQUEST['view'] == 'showtour') return "<script>console.log('$log');jQuery('#Kdata').html('$log');jQuery('#Kdata').show();</script>$result";
            }
            if(isset($_REQUEST['task']) ){
                if($_REQUEST['task'] == 'calcTourCost') return "<script>console.log('$log');</script>$result";
            }
            if(isset($formula) ){
                if($formula) return $log.' = '.$result;
            }
        }
        return $result;
    }
}
