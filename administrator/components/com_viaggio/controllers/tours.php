<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;

/**
 * Tours list controller class.
 *
 * @since  1.6
 */
class ViaggioControllerTours extends JControllerAdmin
{
	/**
	 * Method to clone existing Tours
	 *
	 * @return void
	 */
	public function duplicate()
	{
		// Check for request forgeries
		Jsession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get id(s)
		$pks = $this->input->post->get('cid', array(), 'array');

		try
		{
			if (empty($pks))
			{
				throw new Exception(JText::_('COM_VIAGGIO_NO_ELEMENT_SELECTED'));
			}

			ArrayHelper::toInteger($pks);
			$model = $this->getModel();
			$model->duplicate($pks);
			$this->setMessage(Jtext::_('COM_VIAGGIO_ITEMS_SUCCESS_DUPLICATED'));
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
		}

		$this->setRedirect('index.php?option=com_viaggio&view=tours');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'tour', $prefix = 'ViaggioModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function saveOrderAjax()
	{
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks   = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		ArrayHelper::toInteger($pks);
		ArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}

    /**
     * Gets the list of IDs from the request data
     *
     * @return array
     */
    protected function getIDsFromRequest()
    {
        // Get the ID or list of IDs from the request or the configuration
        $cid = $this->input->get('cid', array(), 'array');
        $id  = $this->input->getInt('id', 0);

        $ids = array();

        if (is_array($cid) && !empty($cid))
        {
            $ids = $cid;
        }
        elseif (!empty($id))
        {
            $ids = array($id);
        }

        return $ids;
    }

    public function publish()
    {
        $db = JFactory::getDbo();
        $ids = implode(',',$_POST['cid']);
        if ($_POST['task']=='tours.publish')
            $query2 = 'UPDATE `#__viaggio_tours` set state = 1 WHERE id IN ('.$ids.')';
        else
            $query2 = 'UPDATE `#__viaggio_tours` set state = 0 WHERE id IN ('.$ids.')';
        $db->setQuery($query2);
        $db->execute();
        if ($_POST['task']=='tours.publish')
            echo '<img src="https://s.tcdn.co/883/185/883185b3-d156-3d57-8cef-84f35062c23c/2.png">';
        else
            echo '<img src="http://about-telegram.ru/wp-content/uploads/2018/03/putin-stickers-telegram_24.png">';
        echo '<script>setTimeout(function(){window.location.href = "https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=tours";},1000);</script>';
        exit;
        if (!$status)
        {
            $this->setRedirect($url, $error, 'error');
        }
        else
        {
            $this->setRedirect($url);
        }
    }
}
