<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Hotel controller class.
 */
class ViaggioControllerManualpayment extends JControllerForm
{

    function __construct() {
        $this->view_list = 'manualpayments';
        parent::__construct();
    }
}