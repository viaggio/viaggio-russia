<?php
$subject = 'Link per procedere al pagamento dei servizi richiesti';
$body = 'Gentile Cliente,<br>

La ringraziamo per la richiesta, come da accordi le inviamo il link per procedere al pagamento dei servizi richiesti. <a href="https://www.viaggio-russia.com/index.php?option=com_viaggio&task=buyManual&payment_id='.$data['paymentID'].'">Cliccare qui per procedere al pagamento</a><br>
Il link per il pagamento e` attivo 24 ore. Se non effettuate il pagamento entro 24 ore dalla ricezione della mail  dovrete richiedere all\'operatore un nuovo link.<br>
<br>Cordiali saluti';
?> 