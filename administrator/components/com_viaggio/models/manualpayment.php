<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 */
class ViaggioModelManualpayment extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_TOURISTINVITE';
    protected $event_after_save = 'onExtensionAfterSave';

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Manualpayment', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_viaggio.Manualpayment', 'Manualpayment', array('control' => 'jform', 'load_data' => $loadData));
        
        
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_viaggio.edit.manualpayment.data', array());

		if (empty($data)) {
			$data = $this->getItem();
            
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed

		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_manualpayments');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}


    public function save($data){
        if (!$data['id'])
        {
            $data['paymentID'] = 'm'.time();
            $data['timeCreatedLinl'] = date('Y-m-d H:i:s');
        }
        $user = JFactory::getUser();
        $data['userId'] = $user->get('id');

        $curs = false;
        $cursFile = file_get_contents('curs.txt');
        if ($cursFile)
        {
            $cursFile = explode('|',$cursFile);
            if ((date('H')+3)<14 || date('d.m.Y')==date('d.m.Y',$cursFile[1]))
            {
                $curs = $cursFile[0];
            }
        }
        if (!$curs)
        {
            $xml = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y'));
            if ($xml)
            {
                $movies = new SimpleXMLElement($xml);
                foreach ($movies->Valute as $valute){
                    if ($valute->CharCode == 'EUR')
                    {
                        $curs = floatval(str_replace(',','.',$valute->Value));
                        file_put_contents('curs.txt',$curs.'|'.time().'|'.date('d.m.Y H:i:s'));
                        break;
                    }
                }
            }
            elseif ($cursFile[0])
                $curs = $cursFile[0];
            else
                $curs = 85;
        }
        $data['curs'] = $curs;
        $data['amountRub'] = $data['amountEuro']*100*$curs;

        if (!$data['id'] && (!$data['order_id'] || $data['order_id'] == ''))
        {
            $insert_details = true;

            $db = JFactory::getDbo();
            $order_id = time();
            $data['order_id'] = $order_id;

            $total_eur = $_POST['total_amount'];
            $total_rub = round( $curs * 1.00 * $total_eur * 100 );

            $clients = $_POST['clients'];

            $doOrderQuery = "INSERT INTO `#__viaggio_orders` (`id`, `asset_id`, `created_by`, `currency`, `orderstatus`, `errorcode`, `errormessage`, `amount`, `valute_amount`, `valute_rate`, `tour_id`, `clientcount`, `insurance`, `stell`, `nomecgome`, `telephone`, `email`) VALUES (".$order_id.", '0', '".$user->get('id')."', '', '-1', '0', '', '".$total_rub."', '".$total_eur."', '".$curs."', '".$data['tour_id']."', ".count($clients).", '".$_POST['insurance']."', '".$_POST['stellcount']."', '".$data['fio']."', '".$data['telefon']."', '".$data['email']."');";
            $db->setQuery($doOrderQuery);
            $db->execute();

            if (count($clients))
            {
                foreach ($clients as $client)
                    self::createClient($client, $order_id, $data['userId'], $data['tour_id']);
            }
        }
        else
        {
            $insert_details = false;

            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM #__viaggio_orders WHERE id = '".$data['order_id']."' LIMIT 1");
            $order = $db->loadObject();
            $data['tour_id'] = $order->tour_id;
        }

        $result = parent::save($data);
        if ($insert_details && $result)
        {
            $db = JFactory::getDbo();
            $db->setQuery("DELETE FROM #__viaggio_manualpayments_details WHERE payment_id = '".$data['paymentID']."'");
            $db->execute();

            foreach ($_POST['air-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','air-ticket','".$data['paymentID']."')");
                    $db->execute();
                }
            }
            foreach ($_POST['rail-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','rail-ticket','".$data['paymentID']."')");
                    $db->execute();
                }
            }
            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-amount','".$_POST['other-amount']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-rus','".$_POST['other-rus']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-ita','".$_POST['other-ita']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-amount','".$_POST['visa-amount']."','0','visa','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-rus','".$_POST['visa-rus']."','0','visa','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-ita','".$_POST['visa-ita']."','0','visa','".$data['paymentID']."')");
            $db->execute();
        }
        JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_viaggio&view=manualpayments', false));
    }

    private static function createClient($clientArray, $order_id, $user_id, $tour_id){
        $query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `tour_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`, `numero_di_passaporto`,`indirizzo_di_residenza`,`luogo_di_nascita`,`date_from`,`date_to`,`numero_di_visite`,`last_visit_from`,`last_visit_to`,`nome_dell_organizzazione`,`ufficio_dell_organizzazione`,`indirizzo_dell_organizzazione`,`telefono_dell_organizzazione`) VALUES (NULL, '', '".$user_id."', '$tour_id', '".$clientArray['cognome']."', '".$clientArray['nome']."', '".$clientArray['sex']."', '".self::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '".$clientArray['telephone']."', '".$clientArray['email']."', '$order_id', '".$clientArray['numero_di_passaporto']."','".$clientArray['indirizzo_di_residenza']."','".$clientArray['luogo_di_nascita']."','".$clientArray['date_from']."','".$clientArray['date_to']."','".$clientArray['numero_di_visite']."','".$clientArray['last_visit_from']."','".$clientArray['last_visit_to']."','".$clientArray['nome_dell_organizzazione']."','".$clientArray['ufficio_dell_organizzazione']."','".$clientArray['indirizzo_dell_organizzazione']."','".$clientArray['telefono_dell_organizzazione']."');";// return $query;
        $db = JFactory::getDbo();
        $db->setQuery($query);
        $result = $db->execute();

        return $db->insertid();
    }

    private static function dateToSQLformat($date)
    {
        $date = str_replace('/','-',$date);
        $date_timestamp = strtotime($date);
        return date('Y-m-d', $date_timestamp);
    }
}