<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelOrders extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'created_by', 'a.`created_by`',
				'currency', 'a.`currency`',
				'orderstatus', 'a.`orderstatus`',
				'errorcode', 'a.`errorcode`',
				'errormessage', 'a.`errormessage`',
				'amount', 'a.`amount`',
				'tour_id', 'a.`tour_id`',
				'clientcount', 'a.`clientcount`',
				'insurance', 'a.`insurance`',
				'stell', 'a.`stell`',
				'telephone', 'a.`telephone`',
				'email', 'a.`email`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering currency
		$this->setState('filter.currency', $app->getUserStateFromRequest($this->context.'.filter.currency', 'filter_currency', '', 'string'));

		// Filtering orderstatus
		$this->setState('filter.orderstatus', $app->getUserStateFromRequest($this->context.'.filter.orderstatus', 'filter_orderstatus', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_viaggio');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__viaggio_orders` AS a');


		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		$user = JFactory::getUser();
        $isAdmin = in_array(8,$user->groups);
        if (!$isAdmin)
        {
            $query->where('a.created_by = '.$user->id);
        }

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.errorcode LIKE ' . $search . '  OR  a.errormessage LIKE ' . $search . '  OR  a.amount LIKE ' . $search . '  OR  a.tour_id LIKE ' . $search . '  OR  a.clientcount LIKE ' . $search . '  OR  a.telephone LIKE ' . $search . '  OR  a.email LIKE ' . $search . ' )');
			}
		}


		//Filtering currency
		$filter_currency = $this->state->get("filter.currency");
		if ($filter_currency)
		{
			$query->where("a.`currency` = '".$db->escape($filter_currency)."'");
		}

		//Filtering orderstatus
		$filter_orderstatus = $this->state->get("filter.orderstatus");
		if ($filter_orderstatus)
		{
			$query->where("a.`orderstatus` = '".$db->escape($filter_orderstatus)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering','a.id');
		$orderDirn = $this->state->get('list.direction','desc');
var_dump($orderCol);
var_dump($orderDirn);
		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
        $db = JFactory::getDbo();
		foreach ($items as $k=>$oneItem) {
            $oneItem->currency = JText::_('COM_VIAGGIO_ORDERS_CURRENCY_OPTION_' . strtoupper($oneItem->currency));
            $oneItem->orderstatus = JText::_('COM_VIAGGIO_ORDERS_ORDERSTATUS_OPTION_' . strtoupper($oneItem->orderstatus));

			if (isset($oneItem->tour_id)) {
				$values = explode(',', $oneItem->tour_id);

				$textValue = array();
				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$query = "SELECT * FROM `#__viaggio_tours` where id = $oneItem->tour_id ";
						$db->setQuery($query);
						$results = $db->loadObject();
						
						if ($results) {
							$textValue[] = $results->name;
							$items[$k]->tour = $results;
						}
					}
				}

			    $oneItem->tour_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->tour_id;
			}

			$query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = ".$oneItem->id;
			$db->setQuery($query);
            $items[$k]->clients = $db->loadObjectList();
		}
		return $items;
	}
}
