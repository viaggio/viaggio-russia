<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelOrder extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_VIAGGIO';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_viaggio.order';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Order', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_viaggio.order', 'order',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_viaggio.edit.order.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;

			// Support for multiple or not foreign key field: order_id
			$array = array();
			foreach((array)$data->order_id as $value): 
				if(!is_array($value)):
					$array[] = $value;
				endif;
			endforeach;
			$data->order_id = implode(',',$array);

			// Support for multiple or not foreign key field: tour_id
			$array = array();
			foreach((array)$data->tour_id as $value): 
				if(!is_array($value)):
					$array[] = $value;
				endif;
			endforeach;
			$data->tour_id = implode(',',$array);
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
            $db = JFactory::getDbo();

            $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE order_id = '".$item->id."' ORDER BY id ASC LIMIT 1";
            $db->setQuery($query1);
            $manualPayment = $db->loadObject();



            if (isset($manualPayment->id))
            {
                $item->manualpayment = $manualPayment;

                $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
                $db->setQuery($query);
                $manualPaymentDetails = $db->loadObjectList();

                if (count($manualPaymentDetails))
                {
                    $data = array();

                    foreach ($manualPaymentDetails as $manualPaymentDetail)
                    {
                        $data[$manualPaymentDetail->field_group_name][$manualPaymentDetail->field_group][$manualPaymentDetail->field_name] = $manualPaymentDetail->field_value;
                    }

                    if (count($data))
                    {
                        if (isset($data['air-ticket']))
                            $item->airTickets = $data['air-ticket'];
                        if (isset($data['rail-ticket']))
                            $item->railTickets = $data['rail-ticket'];
                        if (isset($data['other']))
                            $item->otherDetails = $data['other'];
                    }
                }

                $query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = ".$item->id." ORDER BY id ASC";
                $db->setQuery($query);
                $clients = $db->loadAssocList();

                $item->clients = array();
                if (count($clients))
                {
                    $item->clients = $clients;
                }
            }
		}

		return $item;
	}

	/**
	 * Method to duplicate an Order
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_viaggio'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_orders');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

    public function save($data){
        parent::save($data);

        $db = JFactory::getDbo();
        $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$data['id']."' LIMIT 1");
        $result = $db->loadObject();

        if ($result)
        {
            $db = JFactory::getDbo();
            $db->setQuery("DELETE FROM #__viaggio_manualpayments_details WHERE payment_id = '".$result->paymentID."'");
            $db->execute();

            foreach ($_POST['air-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','air-ticket','".$result->paymentID."')");
                    $db->execute();
                }
            }
            foreach ($_POST['rail-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','rail-ticket','".$result->paymentID."')");
                    $db->execute();
                }
            }
            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-amount','".$_POST['other-amount']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-rus','".$_POST['other-rus']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-ita','".$_POST['other-ita']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-amount','".$_POST['visa-amount']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-rus','".$_POST['visa-rus']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-ita','".$_POST['visa-ita']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $clients = $_POST['clients'];

            if (count($clients))
            {
                $query = "DELETE FROM `#__viaggio_clients` WHERE order_id = ".$data['id'];
                $db->setQuery($query);
                $db->execute();
                foreach ($clients as $client)
                    self::createClient($client, $data['id'], $data['created_by'], $data['tour_id']);
            }
        }

        return true;
    }

    private static function createClient($clientArray, $order_id, $user_id, $tour_id){
        $query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `tour_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`, `numero_di_passaporto`,`indirizzo_di_residenza`,`luogo_di_nascita`,`date_from`,`date_to`,`numero_di_visite`,`last_visit_from`,`last_visit_to`,`nome_dell_organizzazione`,`ufficio_dell_organizzazione`,`indirizzo_dell_organizzazione`,`telefono_dell_organizzazione`) VALUES (NULL, '', '".$user_id."', '$tour_id', '".$clientArray['cognome']."', '".$clientArray['nome']."', '".$clientArray['sex']."', '".self::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '".$clientArray['telephone']."', '".$clientArray['email']."', '$order_id', '".$clientArray['numero_di_passaporto']."','".$clientArray['indirizzo_di_residenza']."','".$clientArray['luogo_di_nascita']."','".self::dateToSQLformat($clientArray['date_from'])."','".self::dateToSQLformat($clientArray['date_to'])."','".$clientArray['numero_di_visite']."','".self::dateToSQLformat($clientArray['last_visit_from'])."','".self::dateToSQLformat($clientArray['last_visit_to'])."','".$clientArray['nome_dell_organizzazione']."','".$clientArray['ufficio_dell_organizzazione']."','".$clientArray['indirizzo_dell_organizzazione']."','".$clientArray['telefono_dell_organizzazione']."');";// return $query;
        $db = JFactory::getDbo();
        $db->setQuery($query);
        $result = $db->execute();

        return $db->insertid();
    }

    private static function dateToSQLformat($date)
    {
        $date = str_replace('/','-',$date);
        $date_timestamp = strtotime($date);
        return date('Y-m-d', $date_timestamp);
    }
}
