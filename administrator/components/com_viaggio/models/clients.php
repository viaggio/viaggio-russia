<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelClients extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'created_by', 'a.`created_by`',
				'tour_id', 'a.`tour_id`',
				'cognome', 'a.`cognome`',
				'nome', 'a.`nome`',
				'sex', 'a.`sex`',
				'birthday', 'a.`birthday`',
				'nazionalita', 'a.`nazionalita`',
				'telephone', 'a.`telephone`',
				'email', 'a.`email`',
				'order_id', 'a.`order_id`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering sex
		$this->setState('filter.sex', $app->getUserStateFromRequest($this->context.'.filter.sex', 'filter_sex', '', 'string'));

		// Filtering birthday
		$this->setState('filter.birthday.from', $app->getUserStateFromRequest($this->context.'.filter.birthday.from', 'filter_from_birthday', '', 'string'));
		$this->setState('filter.birthday.to', $app->getUserStateFromRequest($this->context.'.filter.birthday.to', 'filter_to_birthday', '', 'string'));

		// Filtering nazionalita
		$this->setState('filter.nazionalita', $app->getUserStateFromRequest($this->context.'.filter.nazionalita', 'filter_nazionalita', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_viaggio');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.tour_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__viaggio_clients` AS a');


		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.tour_id LIKE ' . $search . '  OR  a.cognome LIKE ' . $search . '  OR  a.nome LIKE ' . $search . '  OR  a.telephone LIKE ' . $search . '  OR  a.email LIKE ' . $search . '  OR  a.order_id LIKE ' . $search . ' )');
			}
		}


		//Filtering sex
		$filter_sex = $this->state->get("filter.sex");
		if ($filter_sex)
		{
			$query->where("a.`sex` = '".$db->escape($filter_sex)."'");
		}

		//Filtering birthday
		$filter_birthday_from = $this->state->get("filter.birthday.from");
		if ($filter_birthday_from) {
			$query->where("a.`birthday` >= '".$db->escape($filter_birthday_from)."'");
		}
		$filter_birthday_to = $this->state->get("filter.birthday.to");
		if ($filter_birthday_to) {
			$query->where("a.`birthday` <= '".$db->escape($filter_birthday_to)."'");
		}

		//Filtering nazionalita
		$filter_nazionalita = $this->state->get("filter.nazionalita");
		if ($filter_nazionalita)
		{
			$query->where("a.`nazionalita` = '".$db->escape($filter_nazionalita)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->tour_id)) {
				$values = explode(',', $oneItem->tour_id);

				$textValue = array();
				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "SELECT id as tour_id, name as value FROM `#__viaggio_tours`";
						$db->setQuery($query);
						$results = $db->loadObject();
						if ($results) {
							$textValue[] = $results->value;
						}
					}
				}

			$oneItem->tour_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->tour_id;

			}
					$oneItem->sex = JText::_('COM_VIAGGIO_CLIENTS_SEX_OPTION_' . strtoupper($oneItem->sex));
					$oneItem->nazionalita = JText::_('COM_VIAGGIO_CLIENTS_NAZIONALITA_OPTION_' . strtoupper($oneItem->nazionalita));

			if (isset($oneItem->order_id)) {
				$values = explode(',', $oneItem->order_id);

				$textValue = array();
				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "SELECT id as order_id, id as value FROM `#__viaggio_orders`";
						$db->setQuery($query);
						$results = $db->loadObject();
						if ($results) {
							$textValue[] = $results->value;
						}
					}
				}

			$oneItem->order_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->order_id;

			}
		}
		return $items;
	}
}
