<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class ViaggioController
 *
 * @since  1.6
 */
class ViaggioController extends JControllerLegacy
{
    public $paymentUrl = '3dsec.paymentgate.ru/ipay';
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return   JController This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$view = JFactory::getApplication()->input->getCmd('view', 'tours');
		JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}

	private function manualPayment()
    {
        if (isset($_GET['manualPayment']))
        {
            $curs = false;
            $cursFile = file_get_contents('curs.txt');
            if ($cursFile)
            {
                $cursFile = explode('|',$cursFile);
                if ((date('H')+3)<14 || date('d.m.Y')==date('d.m.Y',$cursFile[1]))
                {
                    $curs = $cursFile[0];
                }
            }
            if (!$curs)
            {
                $xml = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y'));
                if ($xml)
                {
                    $movies = new SimpleXMLElement($xml);
                    foreach ($movies->Valute as $valute){
                        if ($valute->CharCode == 'EUR')
                        {
                            $curs = floatval(str_replace(',','.',$valute->Value));
                            file_put_contents('curs.txt',$curs.'|'.time().'|'.date('d.m.Y H:i:s'));
                            break;
                        }
                    }
                }
                elseif ($cursFile[0])
                    $curs = $cursFile[0];
                else
                    $curs = 85;
            }
            $code = strip_tags(addslashes($_GET['code']));
            $db = JFactory::getDBO();
            $query = 'SELECT *  FROM #__viaggio_manualpayments WHERE status = 0 AND '.
                'paymentID = \''.$code.'\' LIMIT 1' ;
            $db->setQuery($query);
            $payment = $db->loadAssoc();
            if ($payment['id']){
                $payTime = strtotime($payment['timeCreatedLinl']);

                $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                $db->setQuery($query);
                $curTime = $db->loadAssoc();
                $curTime = strtotime($curTime['curTime']);

                if ($curTime >= $payTime+3600*24){
                    echo '24 hours expired';
                    exit;
                }
                $cost = intval($payment['amountEuro']*100*$curs*1.00);

                $query = 'UPDATE #__viaggio_manualpayments SET'.
                    ' amountRub = '.$cost.','.
                    ' curs = '.$curs.','.
                    ' status = 0 ,'.
                    ' payTime = CURRENT_TIMESTAMP'.
                    ' WHERE paymentID = \''.$payment['paymentID'].'\'';
                $db->setQuery($query);
                $db->execute();

                $userName = JFactory::getApplication()->get('REBuserName');
                $password = JFactory::getApplication()->get('REBpassword');

                $req = 'https://'.$this->paymentUrl.'/rest/registerPreAuth.do?'.//dvuhstadiyniy platezh
                    'userName='.$userName.
                    '&password='.$password.
                    '&orderNumber='.$payment['paymentID'].
                    '&amount='.$cost.
                    '&description='.urlencode($payment['description']).
                    '&language=en'.
                    //'&returnUrl=http://www.visto-russia.com/step2?getResponse=1';
                    '&returnUrl='.JFactory::getApplication()->get('REBreturnUrlDomen').'/modulo-visto-turistico?getResponse=1';
                $resp = file_get_contents($req);
                $resp = json_decode($resp);
                if ($resp->formUrl)
                {
                    $db = JFactory::getDBO();
                    $query = "UPDATE #__viaggio_manualpayments SET outOrderID = '".$resp->orderId."' WHERE  id = ".intval($payment['id']);
                    $db->setQuery($query);
                    if ($db->execute())
                    {
                        header('Location: '.$resp->formUrl);
                        exit;
                    }
                    echo 'error!';
                }
            }
            else{
                echo 'ERROR! Payment not found!';
            }
            echo 'ERROR LAST!';
            exit;
        }
    }

    public function sendLink($cachable = false, $urlparams = false){
        $db = JFactory::getDBO();

        $query = 'SELECT * FROM #__viaggio_manualpayments WHERE id = '.intval($_GET['id']).' limit 1';

        $db->setQuery($query);
        $rez = $db->loadAssocList();
        ViaggioHelpersViaggio::sendMail($rez[0],'../administrator/components/com_viaggio/mailTemplates/sendLink.php');
        $this->display($cachable = false, $urlparams = false);
    }

    public function cancelmanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 2 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

    public function uncancelmanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 0 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

    public function deletemanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 3 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

	public function printpdf()
	{
		//получаем запрос
		$app  = JFactory::getApplication();
        $order_id = $app->input->getInt('order_id', 0);
		$sendmail = $app->input->getString('sendmail', '');
		$template = $app->input->getString('template', '');
		
		//подключаем хелпер фронта для использования фукнции почты и печати pdf
		include_once JPATH_COMPONENT_SITE.'/helpers/frontend.php';
		
		//подключаем настройки
		include_once JPATH_COMPONENT_SITE.'/helpers/settings.php';
		
		//получаем данные заказа
		$order = ViaggioHelpersFrontend::getOrderObject($order_id);
		$order->number = $order_id;
		
		//echo '<pre>'.print_r($order,1).'</pre>'; die();
		
		//печать пдф
		$filename = ViaggioHelpersFrontend::printPdf($template, $order);
		
		if($sendmail) ViaggioHelpersFrontend::sendEmailByTemplate($sendmail,$order->email,$settings, $order, JPATH_COMPONENT_SITE.'/pdf/'.$filename.'.pdf');
		
		//echo JPATH_COMPONENT_SITE.'/pdf/'.$filename.'.pdf'; 
		die();
	}

	public function payedPartial()
    {
        $db = JFactory::getDBO();
        $app  = JFactory::getApplication();
        $payment_id = $app->input->getInt('payment_id', 0);
        $query = 'UPDATE #__viaggio_manualpayments SET'.
            ' status = 2 ,'.
            ' payTime = CURRENT_TIMESTAMP'.
            ' WHERE id = '.$payment_id;
        $db->setQuery($query);
        $db->execute();
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    public function sendToVisto()
    {
        $order_id = isset($_GET['order_id'])?intval($_GET['order_id']):false;
        //echo 'Start<br/>';
        if ($order_id)
        {
            //echo 'Step 2<br/>';
            //var_dump($order_id);
            //echo '<br/>';
            $arr = array();

            $db = JFactory::getDbo();
            $query2 = "SELECT * FROM `#__viaggio_orders` where id = ".$order_id;
            $db->setQuery($query2);
            $result = $db->loadObject();
            if($result){
                //echo 'Step 3<br/>';
                //получаем списки клиентов
                $query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = $order_id";
                $db->setQuery($query);
                $result->clients = $db->loadObjectList();
                //получаем информацию о туре
                $query = "SELECT * FROM `#__viaggio_tours` WHERE id = $result->tour_id";
                $db->setQuery($query);
                $result->tour = $db->loadObject();

                $query = "SELECT * FROM `#__viaggio_manualpayments` vm JOIN `#__viaggio_manualpayments_details` vmd ON vmd.payment_id = vm.paymentID WHERE vm.order_id = ".$result->id." AND vmd.field_group = 0";
                $db->setQuery($query);
                $result->details = $db->loadObjectList();

                $total_cost = 0;
                foreach ($result->details as $row)
                {
                    if ($row->field_name == 'visa-amount')
                        $total_cost = $row->field_value;
                    elseif ($row->field_group_name == 'air-ticket' && $row->field_name == 'begin')
                        $arr['visa']['date_from'] = $row->field_value;
                    elseif ($row->field_group_name == 'air-ticket' && $row->field_name == 'end')
                        $arr['visa']['date_to'] = $row->field_value;
                }

                $arr['visa']['cities_to_visit'] = $result->tour->citys;
                $arr['visa']['total_cost'] = $total_cost;
                $arr['visa']['user_id'] = 0;

                if (count($result->clients))
                {
                    //echo 'Step 4<br/>';
                    foreach ($result->clients as $row)
                    {
                        $arr['participants'][] = array(
                            'first_name' => $row->nome,
                            'second_name' => $row->cognome,
                            'gender' => $row->sex,
                            'birthdate' => $row->birthday,
                            'nationality' => 'Italy',
                            'passport' => $row->numero_di_passaporto
                        );
                    }
                }

                JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');
                $customFields = FieldsHelper::getFields('com_users.user', JFactory::getUser(), true);
                if (count($customFields))
                {
                    foreach ($customFields as $field)
                    {
                        if ($field->name == 'visto-user-id')
                            $arr['visa']['user_id'] = intval($field->value);
                    }
                }
            }
            if (isset($_GET['kaktus']))
                echo json_encode($arr);
            //echo 'Step 5<br/>';
            $url = "https://www.visto-russia.com/index.php?option=com_touristinvite&task=createVisaApi";
            $request = curl_init($url);
            curl_setopt($request, CURLOPT_POST, true);
            $query=array('data' => json_encode($arr));
            curl_setopt($request, CURLOPT_POSTFIELDS, $query);
            curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($request);
            curl_close($request);
            //echo 'Step 6<br/>';
            //var_dump($result);
            if (intval($result))
            {
                $query = "UPDATE `#__viaggio_orders` SET visa_id = ".intval($result)." where id = ".$order_id;
                $db->setQuery($query);
                $db->execute();
            }
            $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
            //echo 'End';
        }
    }

    public function test()
    {
        JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');
        $customFields = FieldsHelper::getFields('com_users.user', JFactory::getUser(), true);
        if (count($customFields))
        {
            foreach ($customFields as $field)
            {
                if ($field->name == 'visto-user-id')
                    echo '<b>'.intval($field->value).'</b>';
            }
        }
    }

    public function hideManualPayment()
    {
        $id = intval($_GET['id']);
        if ($id)
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET hidden = 1 '.
                'WHERE id = '.$id;
            $db->setQuery($query);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }
}
