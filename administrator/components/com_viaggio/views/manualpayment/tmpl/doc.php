<?php
/**
 * Convert HTML to MS Word file for PHP 4.2.x or earlier
 * @author Dale Attree
 * @version 1.0.1
 * @name HTML_TO_DOC
 */

/**
 * Convert HTML to MS Word file
 * @author Harish Chauhan
 * @version 1.0.0
 * @name HTML_TO_DOC
 */

class HTML_TO_DOC
{
    var $docFile="";
    var $title="";
    var $htmlHead="";
    var $htmlBody="";

    /**
     * Constructor
     *
     * @return void
     */
    function HTML_TO_DOC()
    {
        $this->title="Untitled Document";
        $this->htmlHead="";
        $this->htmlBody="";
    }

    /**
     * Set the document file name
     *
     * @param String $docfile
     */

    function setDocFileName($docfile)
    {
        //echo 'setDocFileName Entered.<br>';
        $this->docFile=$docfile;
        if(!preg_match("/\.doc$/i",$this->docFile))
            $this->docFile.=".doc";
        return;
    }

    function setTitle($title)
    {
        echo 'setTitle Entered.<br>';
        $this->title=$title;
    }

    /**
     * Return header of MS Doc
     *
     * @return String
     */
    function getHeader()
    {
        //echo 'getHeader Entered.<br>';
        $return  = <<<EOH
		 <html xmlns:v="urn:schemas-microsoft-com:vml"
		xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:w="urn:schemas-microsoft-com:office:word"
		xmlns="http://www.w3.org/TR/REC-html40">
		
		<head>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
		<meta name=ProgId content=Word.Document>
		<meta name=Generator content="Microsoft Word 9">
		<meta name=Originator content="Microsoft Word 9">
		<!--[if !mso]>
		<style>
		v\:* {behavior:url(#default#VML);}
		o\:* {behavior:url(#default#VML);}
		w\:* {behavior:url(#default#VML);}
		.shape {behavior:url(#default#VML);}
		</style>
		<![endif]-->
		<title>$this->title</title>
		<!--[if gte mso 9]><xml>
		 <w:WordDocument>
		  <w:View>Print</w:View>
		  <w:DoNotHyphenateCaps/>
		  <w:PunctuationKerning/>
		  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
		  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
		 </w:WordDocument>
		</xml><![endif]-->
		<style>
		<!--
		 /* Font Definitions */
		@font-face
			{font-family:Verdana;
			panose-1:2 11 6 4 3 5 4 4 2 4;
			mso-font-charset:0;
			mso-generic-font-family:swiss;
			mso-font-pitch:variable;
			mso-font-signature:536871559 0 0 0 415 0;}
		 /* Style Definitions */
		p.MsoNormal, li.MsoNormal, div.MsoNormal
			{mso-style-parent:"";
			margin:0in;
			margin-bottom:.0001pt;
			mso-pagination:widow-orphan;
			font-size:7.5pt;
				mso-bidi-font-size:8.0pt;
			font-family:"Verdana";
			mso-fareast-font-family:"Verdana";}
		p.small
			{mso-style-parent:"";
			margin:0in;
			margin-bottom:.0001pt;
			mso-pagination:widow-orphan;
			font-size:1.0pt;
				mso-bidi-font-size:1.0pt;
			font-family:"Verdana";
			mso-fareast-font-family:"Verdana";}
		@page Section1
			{size:8.5in 11.0in;
			margin:1.5cm 1.5cm 1.5cm 1.5cm;
			mso-header-margin:.5in;
			mso-footer-margin:.5in;
			mso-paper-source:0;}
		div.Section1
			{page:Section1;}
		-->
		</style>
		<!--[if gte mso 9]><xml>
		 <o:shapedefaults v:ext="edit" spidmax="1032">
		  <o:colormenu v:ext="edit" strokecolor="none"/>
		 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
		 <o:shapelayout v:ext="edit">
		  <o:idmap v:ext="edit" data="1"/>
		 </o:shapelayout></xml><![endif]-->
		 $this->htmlHead
		</head>
		<body>
EOH;
        return $return;
    }

    /**
     * Return Document footer
     *
     * @return String
     */
    function getFotter()
    {
        //echo 'getFotter Entered.<br>';
        return "</body></html>";
    }

    /**
     * Create The MS Word Document from given HTML
     *
     * @param String $html :: URL Name like http://www.example.com
     * @param String $file :: Document File Name
     * @param Boolean $download :: Wheather to download the file or save the file
     * @return boolean
     */

    function createDocFromURL($url,$file,$download=false)
    {
        echo 'createDocFromURL Entered.<br>';
        if(!preg_match("/^http:/",$url))
            $url="http://".$url;
        $f = fopen($url,'rb');
        while(!feof($f)){
            $html= fread($f,8192);
        }
        return $this->createDoc($html,$file,$download);
    }

    /**
     * Create The MS Word Document from given HTML
     *
     * @param String $html :: HTML Content or HTML File Name like path/to/html/file.html
     * @param String $file :: Document File Name
     * @param Boolean $download :: Wheather to download the file or save the file
     * @return boolean
     */

    function createDoc($html,$file,$download=false)
    {
        //echo 'createDoc Entered.<br>';
        if(is_file($html))
            $html=@file_get_contents($html);

        $this->_parseHtml($html);
        $this->setDocFileName($file);
        $doc=$this->getHeader();
        $doc.=$this->htmlBody;
        $doc.=$this->getFotter();

        if($download)
        {
            //$this->write_file($this->docFile,$doc);
            @header("Cache-Control: ");// leave blank to avoid IE errors
            @header("Pragma: ");// leave blank to avoid IE errors
            @header("Content-type: application/octet-stream");
            //@header("Content-type: application/vnd.ms-word");
            @header("Content-Disposition: attachment; filename=\"$this->docFile\"");
            echo $doc;
            return true;
        }
        else
        {
            return $this->write_file($this->docFile,$doc);
        }
    }

    /**
     * Parse the html and remove <head></head> part if present into html
     *
     * @param String $html
     * @return void
     * @access Private
     */

    function _parseHtml($html)
    {
        //echo '_parseHtml Entered.<br>';
        $html=preg_replace("/<!DOCTYPE((.|\n)*?)>/ims","",$html);
        $html=preg_replace("/<script((.|\n)*?)>((.|\n)*?)<\/script>/ims","",$html);
        preg_match("/<head>((.|\n)*?)<\/head>/ims",$html,$matches);
        $head=$matches[1];
        preg_match("/<title>((.|\n)*?)<\/title>/ims",$head,$matches);
        $this->title = $matches[1];
        $html=preg_replace("/<head>((.|\n)*?)<\/head>/ims","",$html);
        $head=preg_replace("/<title>((.|\n)*?)<\/title>/ims","",$head);
        $head=preg_replace("/<\/?head>/ims","",$head);
        $html=preg_replace("/<\/?body((.|\n)*?)>/ims","",$html);
        $this->htmlHead=$head;
        $this->htmlBody=$html;
        return;
    }

    /**
     * Write the content int file
     *
     * @param String $file :: File name to be save
     * @param String $content :: Content to be write
     * @param [Optional] String $mode :: Write Mode
     * @return void
     * @access boolean True on success else false
     */

    function write_file($file,$content,$mode="w")
    {
        //echo 'write_file entered!<br>';
        $fp=@fopen($file,$mode);
        if(!is_resource($fp)){
            return false;
        }
        fwrite($fp,$content);
        fclose($fp);
        return true;
    }

}
$paymentInfo = $this->getManualPayment();

$html='
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <div class=Section1>
                <table class="first" width="100%">
                    <tr>
                        <td width="30%"><img width=151 height=88 src="https://www.russiantour.com/components/com_touristinvite/assets/php/pdf/logo3.png" /><br>
Тел.:        <b>+7 812 6470690</b><br>
Факс:   <b>+7 812 6470690*3</b>
</td>
			<td width="70%">
<b> ООО "Международная Компания «Русский Тур»,</b><br>
ИНН 7802853888, КПП 780201001<br>
Юридический адрес: 194044, Санкт-Петербург, Финляндский проспект, дом 4, лит. А, офис 424<br>
Банк  Ф-л «Санкт-Петербург»  АКБ «РосЕвроБанк» (АО)<br>
р/сч 40702810980000030469<br>
к/сч 30101810200000000762<br>
БИК 044030762, ОГРН 1147847089532, ОКПО 35460198<br>

</td>
                    </tr>
                </table>
<hr>


            <div style="font-family:Arial;font-size:11pt;">
<table border="0" cellpadding="1" cellspacing="1" style="width:100%">
    <tbody>
	<tr>
	    <td><b>INVOICE # V '.$paymentInfo['paymentID'].'</b></td>
	    <td style="text-align: right;"><b> '.date('m/d/Y',strtotime($paymentInfo['timeCreated'])).'</b> </td>
	</tr>
    </tbody>
</table><br><br><br><br>
<table border="1" cellpadding="1" cellspacing="0" style="width:300px">
    <tbody>
	<tr>
	    <td><b>CLIENT:</b> </td>
	</tr>
	<tr>
	    <td>'.$paymentInfo['fio'].'</td>
	</tr>
    </tbody>
</table><br><br>
<table border="1" cellpadding="1" cellspacing="0" style="width:100%">
    <tbody>
	<tr>
	    <td style="text-align: center;"><b>SERVICE DESCRIPTION</b></td>
	    <td style="text-align: center;"><b>PRICE IN RUBLES</b></td>
	    <td style="text-align: center;"><b>PRICE IN EURO</b></td>
	</tr>
	<tr>
	    <td style="text-align: center;">'.$paymentInfo['description'].'</td>
	    <td style="text-align: center;">
	    <p style="text-align:center"><br><br>'.round($paymentInfo['amountRub']/100,2).' RUB</p>

	    <p style="text-align: left;">Exchange rate: '.$paymentInfo['curs'].' RUB </p>
	    </td>
	    <td style="text-align: center;">'.$paymentInfo['amountEuro'].' EUR</td>
	</tr>
	<tr>
	    <td style="text-align: center;"><b>Total paid</b></td>
	    <td style="text-align: center;"> </td>
	    <td style="text-align: center;">'.$paymentInfo['amountEuro'].' EUR </td>
	</tr>
    </tbody>
</table><br>
<b>VAT NOT APPLICABLE</b><br><br>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
	<tr>
	    <td><b>GENERAL MANAGER</b></td>
	    <td> Olga N. Сheremshenko<br>
<img width=151 height=151 src="https://www.visto-russia.com/images/pe.jpg" /></td>
	</tr>
    </tbody>
</table>

';
$html .= '</body></html>';
/*if ($_GET['html'])
    exit('<a href="doc.php">word</a>'.$html);*/
$go = new HTML_TO_DOC;
$go->createDoc($html,$_GET['id'],true);
exit;