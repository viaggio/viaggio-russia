<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/viaggio.css');

$db    = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('a.id as tour_id, a.name as value, a.from, a.to, a.inn4cost, a.inn3cost,a.halfboard3,a.singola3,a.singola4,a.nostell1,a.nostell2,a.classic,a.classicsingola,a.superior,a.superiorsingola,a.superiorplus,a.superiorplussingola,a.nostalgic,a.nostalgicsingola,a.bolshoy4,a.bolshoy4singola,a.platinum5,a.platinum5singola,a.russiandays,c.params');
$query->from('`#__viaggio_tours` AS a');
$query->join('LEFT', '#__categories AS `c` ON `c`.id = a.`category`');
$query->where('a.state <> -2');
//$query = "SELECT * FROM `#__viaggio_tours` WHERE id = '$tour_id';";
$db->setQuery($query);
$tours = $db->loadObjectList();

$user   = JFactory::getUser();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_orders');
$query->where('created_by = '.$user->id);
$query->order('id DESC');
$db->setQuery($query);
$orders = $db->loadObjectList();

?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'manualpayment.cancel') {
            Joomla.submitform(task, document.getElementById('manualpayment-form'));
        }
        else {
            
            if (task != 'manualpayment.cancel' && document.formvalidator.isValid(document.id('manualpayment-form'))) {
                
                Joomla.submitform(task, document.getElementById('manualpayment-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<?php $itemId = (isset($this->item->id)?$this->item->id:0); ?>
<link rel="stylesheet" href="/images/css/uikit.css" />
  
 <script src="/images/js/uikit.min.js"></script>
 <script src="/images/js/uikit-icons.min.js"></script>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . $itemId); ?>" method="post" name="adminForm" id="manualpayment-form" class="form-validate">

    <div class="form-horizontal to-set-read-only">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_TOURISTINVITE_TITLE_HOTEL', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                 <div class="span4  ">
				<fieldset class="adminform">

                    <input type="hidden" name="jform[id]" value="<?php echo $itemId; ?>" />
                    <div class="control-group tour_id">
                        <div class="control-label">tour_id</div>
                        <div class="controls">
                            <select name="jform[tour_id]">
                            <?php
                            foreach ($tours as $tour)
                            {
                                echo '<option value="'.$tour->tour_id.'"> '.date('d/m/Y',strtotime($tour->from)).'   -  '.date('d/m/Y',strtotime($tour->to)).'  '.$tour->value.'  '.ViaggioHelpersViaggio::getCost($tour).'€ </option>';
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group order_id">
                        <div class="control-label">order_id</div>
                        <div class="controls">
                            <script>var orders = [];</script>
                            <select name="jform[order_id]" id="order_id">
                                <option value=""> --- select order id --- </option>
                                <?php
                                foreach ($orders as $order)
                                {
                                    echo '<option value="'.$order->id.'">'.$order->id.' (amount: '.$order->valute_amount.'€)</option>';
                                    $query = $db->getQuery(true);
                                    $query->select('*');
                                    $query->from('#__viaggio_clients');
                                    $query->where('order_id = '.$order->id);
                                    $query->order('id ASC');
                                    $db->setQuery($query);
                                    $clients = $db->loadObjectList();
                                    $arr=array();
                                    foreach ($clients as $client)
                                    {
                                        $arr[] = '{"cognome":"'.$client->cognome.'","nome":"'.$client->nome.'","numero_di_passaporto":"'.$client->numero_di_passaporto.'","sex":"'.$client->sex.'","nascita":"'.$client->birthday.'"}';
                                    }
                                    echo '<script>orders['.$order->id.'] = {"total_amount":"'.$order->valute_amount.'","stellcount":"'.$order->stell.'","insurance":"'.$order->insurance.'","fio":"'.$order->nomecgome.'","telefon":"'.$order->telephone.'","email":"'.$order->email.'","clients":['.implode(',',$arr).']};</script>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Цена за тур</div>
                        <div class="controls"><input type="text" id="tour_price" need-to-calc></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Общая сумма</div>
                        <div class="controls"><input type="text" name="total_amount" id ="total_amount" readonly></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">stellcount</div>
                        <div class="controls"><input type="text" name="stellcount" id="stellcount"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Mezza pensione (insurance)</div>
                        <div class="controls"><input name="insurance" type="checkbox"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">amountEuro</div>
                        <div class="controls"><?php echo $this->form->getInput('amountEuro'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">fio</div>
                        <div class="controls"><?php echo $this->form->getInput('fio'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">telefon</div>
                        <div class="controls"><?php echo $this->form->getInput('telefon'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">email</div>
                        <div class="controls"><?php echo $this->form->getInput('email'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">description</div>
                        <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                    </div>
                </fieldset>
            </div>
			
			<div class="span4 not-show-on-order-set">
<div class="uk-inline uk-width-1-1">
            
            <div id="toggle-animation1" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                <div class="air-ticket  uk-grid " block-id="0">
									<div class="uk-grid  uk-margin-remove-top uk-text-center" >
					
                    <div class="uk-width-1-1 uk-text-center">
					 <span class="uk-h3 uk-text-center">Авиабилет</span>
                    </div>

                    </div>
				<div class="uk-margin-remove  uk-grid">
				 <div style=" padding-left: 3px; " class="uk-margin-remove uk-width-1-4 ">
                    <input  style=" width: 100px; "class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="air-ticket[0][amount]">
					</div>
					 <div class="uk-margin-remove">
                    <?php echo JHtml::_('calendar', '', "air-ticket[0][begin]", "air-ticket[0][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>
                    <?php echo JHtml::_('calendar', '', "air-ticket[0][end]", "air-ticket[0][end]", '%d/%m/%Y',  array('placeholder'=>"Дата завершение тура")); ?>
					</div> </div>
					<div class="uk-grid  uk-margin-remove-top" >
					
                    <div class="uk-width-1-5">
					Откуда? </div>
					 <div class="uk-width-2-5">
                        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][to-city-rus]">
                    </div>
					  <div class="uk-width-2-5" >
                        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][from-city-rus]">
                    </div>

                    </div>
					
						<div class="uk-grid uk-margin-remove-top" >
                    <div class="uk-width-1-5">
					Куда?</div>
					                    <div class="uk-width-2-5">
                        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][to-city-ita]">
                    </div>
                    <div class="uk-width-2-5"> 
                        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][from-city-rus]">
                    </div>

					</div>
					<div class="uk-grid  uk-margin-remove-top" >
                    <div class="uk-width-1-5">
   Обратно</div>
                       <div class="uk-width-2-5"> 
                        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][back-city-ita]">
                    </div>
   <div class="uk-width-2-5"> 
                        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][back-city-rus]">
                    </div>

                </div>
				<div class="uk-grid  uk-margin-remove-top" ><div class="uk-width-1-1"> 
                <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('air-ticket')">Добавить билет</button>
				</div></div>
            </div>
        </div>
		
		
        <div class="uk-inline uk-width-1-1">
           
            <div id="toggle-animation2" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                <div class="rail-ticket" block-id="0">
													<div class="uk-grid  uk-margin-remove-top uk-text-center" >
					
                    <div class="uk-width-1-1 uk-text-center">
					 <span class="uk-h3 uk-text-center">РЖД</span>
                    </div>

                    </div>
					<div class="uk-margin-remove  uk-grid">
				 <div style=" padding-left: 3px; " class="uk-margin-remove uk-width-1-4 ">
                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="rail-ticket[0][amount]"></div>
					 <div class="uk-margin-remove">
                    <?php echo JHtml::_('calendar', '', "rail-ticket[0][begin]", "rail-ticket[0][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>
                    <?php echo JHtml::_('calendar', '', "rail-ticket[0][end]", "rail-ticket[0][end]", '%d/%m/%Y', array('placeholder'=>"Дата завершение тура")); ?>
					</div> </div>
                    <div class="uk-grid  uk-margin-remove-top" >
                    <div class="uk-width-1-5">Откуда?</div>
                       <div class="uk-width-2-5"> 
                    
			   
                        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[0][from-city-ita]">
                    </div>
                     <div class="uk-width-2-5"> 
                        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[0][from-city-rus]">
                    </div>
   </div>  <div class="uk-grid  uk-margin-remove-top" >
                    <div class="uk-width-1-5">
                     Куда? </div>
                       <div class="uk-width-2-5"> 
					                   
                        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[0][to-city-ita]">
                    </div>
                     <div class="uk-width-2-5"> 
                        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[0][to-city-rus]">
                    </div>

                </div>
                <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('rail-ticket')">Добавить билет</button>
            </div>
        </div>
		        <div class="uk-inline uk-width-1-1">

            <div id="toggle-animation-visa" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
			            													<div class="uk-grid  uk-margin-remove-top uk-text-center" >
					
                    <div class="uk-width-1-1 uk-text-center">
					 <span class="uk-h3 uk-text-center">Визы</span>
                    </div>

                    </div>
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="visa-amount"><br>
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="2" placeholder="Русский текст" name="visa-rus"></textarea>
                </div>
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="2" placeholder="Итальянский текст" name="visa-ita"></textarea>
                </div>
            </div>
        </div>
        <div class="uk-inline uk-width-1-1">
            
            <div id="toggle-animation3" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
			            													<div class="uk-grid  uk-margin-remove-top uk-text-center" >
					
                    <div class="uk-width-1-1 uk-text-center">
					 <span class="uk-h3 uk-text-center">Прочее</span>
                    </div>

                    </div>
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="other-amount"><br>
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="2" placeholder="Русский текст" name="other-rus"></textarea>
                </div>
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="2" placeholder="Итальянский текст" name="other-ita"></textarea>
                </div>
            </div>
        </div>

			</div>
			</div>
        </div>
		<div class="span4 not-show-on-order-set">
        <div class="uk-inline uk-width-1-1">
            <b>Клиенты</b><br/>
            <div class="client" block-id="0">
                <input type="text" name="clients[0][cognome]" placeholder="cognome">
                <input type="text" name="clients[0][nome]" placeholder="nome">
                <input type="text" name="clients[0][numero_di_passaporto]" placeholder="numero_di_passaporto">
                <select name="clients[0][sex]"><option value="m" selected>Мужик</option><option value="f">Леди</option></select>
                <?php echo JHtml::_('calendar', '', "clients[0][nascita]", "clients[0][nascita]", '%d/%m/%Y', array('placeholder'=>"nascita")); ?>
                <input type="text" name="clients[0][telephone]" placeholder="Telephone"/>
                <input type="text" name="clients[0][indirizzo_di_residenza]" placeholder="indirizzo_di_residenza"/>
                <input type="text" name="clients[0][luogo_di_nascita]" placeholder="luogo_di_nascita"/>
                <?php echo JHtml::_('calendar', '', "clients[0][date_from]", "clients[0][date_from]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_from")); ?>
                <?php echo JHtml::_('calendar', '', "clients[0][date_to]", "clients[0][date_to]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_to")); ?>
                <input type="text" name="clients[0][numero_di_visite]" placeholder="numero_di_visite" value="0"/>
                <?php echo JHtml::_('calendar', '', "clients[0][last_visit_from]", "clients[0][last_visit_from]", '%d/%m/%Y', array('placeholder'=>"last_visit_from")); ?>
                <?php echo JHtml::_('calendar', '', "clients[0][last_visit_to]", "clients[0][last_visit_to]", '%d/%m/%Y', array('placeholder'=>"last_visit_to")); ?>
                <input type="text" name="clients[0][nome_dell_organizzazione]" placeholder="organization"/>
                <input type="text" name="clients[0][ufficio_dell_organizzazione]" placeholder="position"/>
                <input type="text" name="clients[0][indirizzo_dell_organizzazione]" placeholder="address"/>
                <input type="text" name="clients[0][telefono_dell_organizzazione]" placeholder="telefono_dell_organizzazione"/>
                <input type="email" name="clients[0][email]" class="validate-email" placeholder="Email"/>
                <hr/>
            </div>
            <button class="uk-button uk-button-primary client-button" type="button" onclick="cloneBlock('client')">Добавить клиента</button>
        </div>
	</div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>



        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
<script>
    jQuery( document ).ready(function() {
        jQuery('#order_id').on('change',function () {
            if (jQuery(this).val()!='')
            {
                jQuery('.tour_id').hide();
                jQuery('.to-set-read-only input').attr('readonly','readonly');
                jQuery('.to-set-read-only select').attr('readonly','readonly');
                jQuery('.to-set-read-only select').trigger("chosen:updated");
                jQuery('.to-set-read-only [name="jform[tour_id]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[order_id]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[description]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[amountEuro]"]').removeAttr('readonly');
                jQuery('.field-calendar button').hide();
                jQuery('.client-button').hide();

                var order = orders[jQuery(this).val()];
                jQuery('#total_amount').val(order.total_amount);
                jQuery('#stellcount').val(order.stellcount);
                jQuery('#insurance').attr('checked',(order.insurance=='on'));
                jQuery('#jform_fio').val(order.fio);
                jQuery('#jform_telefon').val(order.telefon);
                jQuery('#jform_email').val(order.email);

                jQuery('.client[block-id!=0]').remove();
                var needCreate = false;

                order.clients.forEach(function (row) {
                    if (needCreate)
                        cloneBlock('client');
                    else
                        needCreate = true;
                    var block = jQuery('.client:last');
                    var block_num = jQuery('.client:last').attr('block-id');

                    block.find('[name="clients['+block_num+'][cognome]"]').val(row.cognome);
                    block.find('[name="clients['+block_num+'][nome]"]').val(row.nome);
                    block.find('[name="clients['+block_num+'][numero_di_passaporto]"]').val(row.numero_di_passaporto);
                    block.find('[name="clients['+block_num+'][sex]"]').val(row.sex);
                    block.find('[name="clients['+block_num+'][nascita]"]').val(row.nascita);
                });

                if (document.location.search.indexOf('data=') > 0)
                {
                    jQuery('#tour_price').hide();
                    jQuery('#order_id').next().after(data[0]);
                    jQuery('#order_id').next().remove();
                }

                jQuery('.not-show-on-order-set').hide();
            }
            else
            {
                jQuery('.tour_id').show();
                jQuery('.to-set-read-only input').removeAttr('readonly');
                jQuery('.to-set-read-only select').removeAttr('readonly');
                jQuery('.to-set-read-only select').trigger("chosen:updated");
                jQuery('.client-button').show();

                jQuery('.not-show-on-order-set').show();
            }
        });
        if (document.location.search.indexOf('data=') > 0)
        {
            var data = document.location.search.split('&data=')[1].split('|');
            jQuery('#order_id').val(data[0]);
            jQuery('#order_id').change();
            jQuery('#jform_amountEuro').val(data[1]);
        }

        jQuery('[need-to-calc]').on('change',function(){
            var total_amount = 0;
            jQuery('[need-to-calc]').each(function(){
                var amount = parseInt(jQuery(this).val());
                if (amount)
                    total_amount += amount;
            });
            jQuery('#total_amount').val(total_amount);
        });
    });
    function cloneBlock(name)
    {
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        jQuery('.'+name+':last').after(jQuery('.'+name+':first').clone());
        jQuery('.'+name+':last').attr('block-id',lastBlockId+1);
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] input').each(function(){
            var inputName = jQuery(this).attr('name');
            if (inputName)
            {
                if (inputName.indexOf('amount') > 0)
                    jQuery(this).remove();
                else
                {
                    jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
                    jQuery(this).val('');
                }
            }
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] select').each(function(){
            jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
            jQuery(this).next().remove();
            jQuery(this).chosen();
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] .field-calendar').each(function(){
            JoomlaCalendar.init(this);
        });
    }
</script>