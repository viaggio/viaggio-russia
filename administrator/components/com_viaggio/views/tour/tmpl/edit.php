<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');

$path = $this->item->image_path;

if (!$path)
{
    $path = explode('/',$this->item->category_img);
    array_pop($path);
    array_pop($path);
    $path[] = $this->item->alias;
    $path = implode('/',$path);
}
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'tour.cancel') {
			Joomla.submitform(task, document.getElementById('tour-form'));
		}
		else {
			
			if (task != 'tour.cancel' && document.formvalidator.isValid(document.id('tour-form'))) {
				
				Joomla.submitform(task, document.getElementById('tour-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="tour-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_VIAGGIO_TITLE_TOUR', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>
                <?php echo $this->form->renderField('name'); ?>
                <?php echo $this->form->renderField('name_rus'); ?>
				<?php echo $this->form->renderField('alias'); ?>
				<?php echo $this->form->renderField('category'); ?>
				<?php echo $this->form->renderField('widgetkit'); ?>
				<?php echo $this->form->renderField('russiantour_trip_id'); ?>
				<hr/>
				<?php echo $this->form->renderField('single_img'); ?>
				<?php echo $this->form->renderField('single_img_alt'); ?>
				<?php echo $this->form->renderField('single_img_title'); ?>
				<hr/>
				<?php echo $this->form->renderField('category_img'); ?>
				<?php echo $this->form->renderField('category_img_alt'); ?>
				<?php echo $this->form->renderField('category_img_title'); ?>
				<hr/>
				<?php echo $this->form->renderField('desc'); ?>
				<hr/>
				<?php echo $this->form->renderField('tab4'); ?>
				<?php echo $this->form->renderField('fulltext'); ?>
				<?php echo $this->form->renderField('tab2'); ?>
				<?php echo $this->form->renderField('tab3'); ?>
				<?php echo $this->form->renderField('comment_id'); ?>


					<?php /* if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; */ ?>
				</fieldset>
			</div>
		</div>
		
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'settings', JText::_('Hotel', true)); ?>
			<?php echo $this->form->renderField('from'); ?>
				<?php echo $this->form->renderField('to'); ?>
				<?php echo $this->form->renderField('inn4count'); ?>
				<?php echo $this->form->renderField('inn3count'); ?>
				<?php echo $this->form->renderField('inn4cost'); ?>
				<?php echo $this->form->renderField('inn3cost'); ?>
				<?php echo $this->form->renderField('halfboard4'); ?>
				<?php echo $this->form->renderField('halfboard3'); ?>
				<?php echo $this->form->renderField('singola4'); ?>
				<?php echo $this->form->renderField('singola3'); ?>
				<?php echo $this->form->renderField('insurance_cost'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'transib', JText::_('Transib', true)); ?>	 
				<?php echo $this->form->renderField('russiandays'); ?>
				<?php echo $this->form->renderField('classic'); ?>
				<?php echo $this->form->renderField('classicsingola'); ?>
				<?php echo $this->form->renderField('superior'); ?>
				<?php echo $this->form->renderField('superiorsingola'); ?>
				<?php echo $this->form->renderField('superiorplus'); ?>
				<?php echo $this->form->renderField('superiorplussingola'); ?>
				<?php echo $this->form->renderField('nostalgic'); ?>
				<?php echo $this->form->renderField('nostalgicsingola'); ?>
				<?php echo $this->form->renderField('bolshoy4'); ?>
				<?php echo $this->form->renderField('bolshoy4singola'); ?>
				<?php echo $this->form->renderField('platinum5'); ?>
				<?php echo $this->form->renderField('platinum5singola'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'settings2', JText::_('Круизы', true)); ?>
				<?php echo $this->form->renderField('nostell1'); ?>
				<?php echo $this->form->renderField('nostell2'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('Meta', true)); ?>
			<?php echo $this->form->renderField('title'); ?>
			<?php echo $this->form->renderField('metadesc'); ?>
			<?php echo $this->form->renderField('metakey'); ?>
			<?php echo $this->form->renderField('author'); ?>
			<?php echo $this->form->renderField('robots'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'images', 'Фотки'); ?>
            <?php echo $this->form->renderField('image_path'); ?>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_1'); ?>
                    <?php echo $this->form->renderField('image_title_1'); ?>
                </div>
                <img src="/<?php echo $path; ?>/1_small.jpg" />
            </div>
            <br/>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_2'); ?>
                    <?php echo $this->form->renderField('image_title_2'); ?>
                </div>
                <img src="/<?php echo $path; ?>/2_small.jpg" />
            </div>
            <br/>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_3'); ?>
                    <?php echo $this->form->renderField('image_title_3'); ?>
                </div>
                <img src="/<?php echo $path; ?>/3_small.jpg" />
            </div>
            <br/>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_4'); ?>
                    <?php echo $this->form->renderField('image_title_4'); ?>
                </div>
                <img src="/<?php echo $path; ?>/4_small.jpg" />
            </div>
            <br/>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_5'); ?>
                    <?php echo $this->form->renderField('image_title_5'); ?>
                </div>
                <img src="/<?php echo $path; ?>/5_small.jpg" />
            </div>
            <br/>

            <div class="img-meta">
                <div style="float: left">
                    <?php echo $this->form->renderField('image_alt_6'); ?>
                    <?php echo $this->form->renderField('image_title_6'); ?>
                </div>
                <img src="/<?php echo $path; ?>/6_small.jpg" />
            </div>

        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'citys', JText::_('Города посещение', true)); ?>
            <?php echo $this->form->renderField('citys',null,($this->item->citys)?explode(',',$this->item->citys):null); ?>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php //if (JFactory::getUser()->authorise('core.admin','viaggio')) : ?>
	<?php //echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php //echo $this->form->getInput('rules'); ?>
	<?php //echo JHtml::_('bootstrap.endTab'); ?>
<?php //endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<style>
    .img-meta img {
        padding-left: 30px;
        height: 92px;
    }
</style>