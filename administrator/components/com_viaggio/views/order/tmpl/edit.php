<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');
ini_set('display_errors',1);
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.order_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('order_idhidden')){
			js('#jform_order_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_order_id").trigger("liszt:updated");
	js('input:hidden.tour_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('tour_idhidden')){
			js('#jform_tour_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_tour_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'order.cancel') {
			Joomla.submitform(task, document.getElementById('order-form'));
		}
		else {
			
			if (task != 'order.cancel' && document.formvalidator.isValid(document.id('order-form'))) {
				
				Joomla.submitform(task, document.getElementById('order-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="order-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_VIAGGIO_TITLE_ORDER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<input type="hidden" name="jform[subid]" value="<?php echo $this->item->subid; ?>" />
				<?php echo $this->form->renderField('order_id'); ?>

			<?php
				foreach((array)$this->item->order_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="order_id" name="jform[order_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('currency'); ?>
				<?php echo $this->form->renderField('orderstatus'); ?>
				<?php echo $this->form->renderField('errorcode'); ?>
				<?php echo $this->form->renderField('errormessage'); ?>
				<?php echo $this->form->renderField('amount'); ?>
				<?php echo $this->form->renderField('valute_amount'); ?>
				<?php echo $this->form->renderField('valute_rate'); ?>
				<?php echo $this->form->renderField('tour_id'); ?>

			<?php
				foreach((array)$this->item->tour_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="tour_id" name="jform[tour_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('clientcount'); ?>
				<?php echo $this->form->renderField('insurance'); ?>
				<?php echo $this->form->renderField('stell'); ?>
				<?php echo $this->form->renderField('nomecgome'); ?>
				<?php echo $this->form->renderField('telephone'); ?>
				<?php echo $this->form->renderField('email'); ?>
				<?php echo $this->form->renderField('description'); ?>


					<?php /*if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif;*/ ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php if (isset($this->item->manualpayment)) { ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'paymentDetails', JText::_('COM_VIAGGIO_TITLE_DETAILS', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <!-- АВИА -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Авиа</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <?php $n = 0; include ('blocks/air-ticket-block.php'); ?>
                            <?php
                            if (isset($this->item->airTickets) && count($this->item->airTickets) > 1)
                                {
                                    foreach ($this->item->airTickets as $n => $v)
                                    {
                                        if ($n > 0)
                                            include ('blocks/air-ticket-block.php');
                                    }
                                }
                            ?>
                            <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('air-ticket')">Добавить билет</button>
                            <button class="uk-button uk-button-primary air-ticket-remove" type="button" onclick="removeBlock('air-ticket')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последний билет</button>
                        </div>
                    </div>
                    <!-- ЖД -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>ЖД</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <?php $n = 0; include ('blocks/rail-ticket-block.php'); ?>
                            <?php
                            if (isset($this->item->railTickets) && count($this->item->railTickets) > 1)
                            {
                                foreach ($this->item->railTickets as $n => $v)
                                {
                                    if ($n > 0)
                                        include ('blocks/rail-ticket-block.php');
                                }
                            }
                            ?>
                            <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('rail-ticket')">Добавить билет</button>
                            <button class="uk-button uk-button-primary rail-ticket-remove" type="button" onclick="removeBlock('rail-ticket')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последний билет</button>
                        </div>
                    </div>
                    <!-- ПРОЧЕЕ -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Прочее</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="other-amount" value="<?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-amount']:''; ?>"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Русский текст" name="other-rus"><?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-rus']:''; ?></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Итальянский текст" name="other-ita"><?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-ita']:''; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- Виза -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Виза</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="visa-amount" value="<?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-amount']))?$this->item->otherDetails[0]['visa-amount']:''; ?>"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Русский текст" name="visa-rus"><?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-rus']))?$this->item->otherDetails[0]['visa-rus']:''; ?></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Итальянский текст" name="visa-ita"><?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-ita']))?$this->item->otherDetails[0]['visa-ita']:''; ?></textarea>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php } ?>
        <?php if (isset($this->item->clients) && count($this->item->clients)) { ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'clients', JText::_('COM_VIAGGIO_TITLE_CLIENTS', true)); ?>
            <div class="row-fluid">
                <div class="span10 form-horizontal">
                    <fieldset class="adminform">
                        <!-- КЛИЕНТЫ -->
                        <div class="uk-inline uk-width-1-1">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                                <?php $n = 0; include ('blocks/clients-block.php'); ?>
                                <?php
                                if (count($this->item->clients) > 1)
                                {
                                    foreach ($this->item->clients as $n => $v)
                                    {
                                        if ($n > 0)
                                            include ('blocks/clients-block.php');
                                    }
                                }
                                ?>
                                <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('client')">Добавить клиента</button>
                                <button class="uk-button uk-button-primary client-remove" type="button" onclick="removeBlock('client')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последнего клиента</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php } ?>
		<?php /* if (JFactory::getUser()->authorise('core.admin','viaggio')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); */?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<script>
    function cloneBlock(name)
    {
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        jQuery('.'+name+':last').after(jQuery('.'+name+':first').clone());
        jQuery('.'+name+':last').attr('block-id',lastBlockId+1);
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] input').each(function(){
            var inputName = jQuery(this).attr('name');
            if (inputName)
            {
                if (inputName.indexOf('amount') > 0)
                    jQuery(this).remove();
                else
                {
                    jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
                    jQuery(this).val('');
                }
            }
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] select').each(function(){
            jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
            jQuery(this).next().remove();
            jQuery(this).chosen();
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] .field-calendar').each(function(){
            JoomlaCalendar.init(this);
        });
        jQuery('.'+name+'-remove').show();
    }

    function removeBlock(name) {
        jQuery('.'+name+':last').remove();
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        if (lastBlockId < 1)
            jQuery('.'+name+'-remove').hide();
    }
</script>