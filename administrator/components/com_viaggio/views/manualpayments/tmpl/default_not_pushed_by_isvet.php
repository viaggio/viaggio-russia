<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/viaggio.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_viaggio');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_viaggio&task=manualpayments.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'hotelList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>
<link rel="stylesheet" href="/images/css/uikit.css" />
  
 <script src="/images/js/uikit.min.js"></script>
 <script src="/images/js/uikit-icons.min.js"></script>
Нажал кнопку "Оплатить по банку", то она оплачена у уходит сюда https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=orders<br>
Если оплатили то по ссылки то она уходит сюда https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=orders  <br>
Если нажили кнопку "Отправить ссылку", то отправляеться ссылка и pdf с invoce но почту.<br>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=manualpayments'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
		</div>        
		<table class="table table-striped" id="hotelList">
			<thead>
				<tr>
                    <th width="1%" class="">
                        <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                    </th>
					<th width="1%" class="nowrap center">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
					<th width="1%" class="nowrap center">
						<?php //echo JText::_('COM_TOURISTINVITE_PAYMENTS_STATUS');?>
                        data\id\user
                    </th>
					<th width="1%" class="nowrap center">
                        FIO telefon
					</th>
                    <th width="1%" class="nowrap center">
                        описание
                    </th>
                    <th width="1%" class="nowrap center">
                        email
                    </th>
                    <th width="1%" class="nowrap center">
                        деньги
<?php echo JText::_('COM_TOURISTINVITE_LINK');?>
                    </th>
                    <th width="1%" class="nowrap center">
                        status
                    </th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
                $payTime = strtotime($item->timeCreatedLinl);

                $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                $db=JFactory::getDBO();
                $db->setQuery($query);
                $curTime = $db->loadAssoc();
                $curTime = strtotime($curTime['curTime']);
                if ($item->userId)
                    $rowUser = JFactory::getUser($item->userId);
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_viaggio');
                $canEdit	= $user->authorise('core.edit',			'com_viaggio');
                $canCheckin	= $user->authorise('core.manage',		'com_viaggio');
                $canChange	= $user->authorise('core.edit.state',	'com_viaggio');
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
                <?php if (isset($this->items[0]->ordering)): ?>
					<td class="order nowrap center hidden-phone">
					<?php if ($canChange) :
						$disableClassName = '';
						$disabledLabel	  = '';
						if (!$saveOrder) :
							$disabledLabel    = JText::_('JORDERINGDISABLED');
							$disableClassName = 'inactive tip-top';
						endif; ?>
						<span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
							<i class="icon-menu"></i>
						</span>
						<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
					<?php else : ?>
						<span class="sortable-handler inactive" >
							<i class="icon-menu"></i>
						</span>
					<?php endif; ?>
					</td>
                <?php endif; ?>
					<td class="">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>

                <?php if (isset($this->items[0]->id)): ?>
					<td class="center">
						<?php echo (int) $item->id; ?>
					</td>
					<td class="center">
                        Дата создания <br/>
						<?php echo $item->timeCreated; ?><br/><br/>
                        <?php if ($item->userId) : ?>
                        Создал <br/>
                        <?php echo $rowUser->get('username'); ?><br/><br/>
                        id <?php echo $item->paymentID.'/'.$rowUser->get('id'); ?>
                        <?php endif; ?>
					</td>
					<td class="center">
						<?php echo $item->fio; ?><br/>
                        <?php echo $item->telefon; ?>
					</td>
                    <td class="center">
                        <?php echo $item->description; ?>
<a href="?option=com_viaggio&view=manualpayment&layout=doc&id=<?php echo $item->id; ?>">Отправить Cart credito word </a>
                    </td>
                    <td class="center">
                        <?php echo $item->email; ?>
                        <?php if ($item->status == 0 && $payTime>0 && $curTime < $payTime+3600*24) : ?>
                        <a onclick="return confirm('Вы хотите отправить ссылку?');" href="?option=com_viaggio&view=manualpayments&task=sendLink&id=<?php echo $item->id; ?>">
                            (Отправить ссылку)
                        </a>

                        <?php endif; ?>
                    </td>


                    <td class="center">
                        <?php echo $item->amountEuro; ?> евро<br/>
                      
					   <?php
                        if ($item->curs) {
                          echo ($item->amountRub/100).'<br/>';
							
                            echo 'курс '.$item->curs.' от '.$item->payTime.'<br/>';
                        }
                        ?>
                    </td>
                    <td class="center">
                        <?php
                        if ($item->status == 0 && $payTime>0 && $curTime < $payTime+3600*24){
                            echo 'Ждет оплаты<br/>'.
                                'https://www.'.JText::_('COM_TOURISTINVITE_LINK').'?option=com_viaggio&view=step3manual&code='.$item->paymentID.'<br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать Заказ</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=cancelmanual&id='.$item->id.'">Отменить Заказ</a><br/>'.
                                'от '.$item->timeCreatedLinl.'<br/>'.
                                'до '.date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+3600*24);
                        }
                        elseif ($item->status == 0 && $payTime>0 && $curTime >= $payTime+3600*24){
                            echo '<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать ссылку</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=deletemanual&id='.$item->id.'">Удалить заявку.</a><br/>'.
                                'закончился<br/>'.
                                date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+3600*24);
                        }
                        elseif ($item->status == 1){
                            echo 'Оплатил<a href="?option=com_viaggio&view=visaandinivte&makeCheck=1&paymentID='.$item->paymentID.'&email='.$item->email.'&amount='.($item->amountRub/100).'&date='.date('d.m.Y H:i:s',strtotime($item->payTime)).'">.</a>';
                        }
                        elseif ($item->status == 2){
                            echo '<a href="?option=com_viaggio&view=manualpayments&task=uncancelmanual&id='.$item->id.'">Оформить заказ</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=deletemanual&id='.$item->id.'">Удалить заявку.</a>';
                        }
                        ?>
                     <button class="uk-button uk-button-primary" type="button">Редактировать</button><br>
					 <button class="uk-button uk-button-primary" type="button">Оплатили по банку</button>
					  <button class="uk-button uk-button-primary" type="button">Отправить ссылку</button>

					</td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>