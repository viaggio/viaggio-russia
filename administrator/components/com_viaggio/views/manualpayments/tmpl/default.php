<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/viaggio.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_viaggio');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_viaggio&task=manualpayments.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'hotelList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
 <script src="/images/js/uikit.min.js"></script>
 <script src="/images/js/uikit-icons.min.js"></script>
 <link rel="stylesheet" href="/images/css/uikit.css" />
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=manualpayments'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
		</div>         
		<a href="#modal-example" uk-toggle class="uk-icon-link uk-margin-small-right" uk-icon="file-edit"></a>
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p class="uk-text-muted">Cделай чтоб вот эти поля заполнялись 		<br>
 АВИАБИЛЕТЫ? ЖД? ПРОЧЕЕ?<br>
		https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit </p>
        
    </div>
</div>

		 
		<table class="uk-table table-striped" id="hotelList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
					<th width="1%" class="nowrap center">
						<?php //echo JText::_('COM_TOURISTINVITE_PAYMENTS_STATUS');?>
                        data\id\user
                    </th>
					<th width="1%" class="nowrap center">
                        FIO telefon
					</th>
                    <th width="1%" class="nowrap center">
                        описание
                    </th>
                    <th width="1%" class="nowrap center">
                        email
                    </th>
                    <th width="1%" class="nowrap center">
                        деньги
<?php echo JText::_('COM_TOURISTINVITE_LINK');?>
                    </th>
                    <th width="1%" class="nowrap center">
                        status
                    </th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
                $db = JFactory::getDbo();
                $db->setQuery("SELECT * FROM #__viaggio_manualpayments_details WHERE payment_id = '".$item->paymentID."'");
                $details = $db->loadObjectList();
                $payTime = strtotime($item->timeCreatedLinl);

                $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                $db=JFactory::getDBO();
                $db->setQuery($query);
                $curTime = $db->loadAssoc();
                $curTime = strtotime($curTime['curTime']);
                if ($item->userId)
                    $rowUser = JFactory::getUser($item->userId);
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_viaggio');
                $canEdit	= $user->authorise('core.edit',			'com_viaggio');
                $canCheckin	= $user->authorise('core.manage',		'com_viaggio');
                $canChange	= $user->authorise('core.edit.state',	'com_viaggio');
				?>
				<tr class="row<?php echo $i % 2; ?>">

                <?php if (isset($this->items[0]->id)): ?>
					<td class="center">
						<?php echo (int) $item->id; ?>
                        <br/><br/>
                        <a href="?option=com_viaggio&task=hideManualPayment&id=<?php echo $item->id; ?>">
                            Удалить
                        </a>
					</td>
					<td class="center">
                        Дата создания <br/>
						<?php echo $item->timeCreated; ?><br/><br/>
                        <?php if ($item->userId) : ?>
                        Создал <br/>
                        <?php echo $rowUser->get('username'); ?><br/><br/>
                        id <?php echo $item->paymentID.'/'.$rowUser->get('id'); ?>
                        <?php endif; ?>
					</td>
					<td class="center">
						<?php echo $item->fio; ?><br/>
                        <?php echo $item->telefon; ?>
					</td>
                    <td class="center">
                        <?php echo $item->description; ?>		 
                        <?php
                        foreach ($details as $row) {
                            if ($row->field_value != '')
                                echo /*$row->field_group_name.': '.$row->field_name.'['.$row->field_group.'] = '.*/$row->field_value.'<br/>';
                        }
                        ?>
                    </td>
                    <td class="center">
                        <?php echo $item->email; ?>
                        <?php if ($item->status == 0 && $payTime>0 && $curTime < $payTime+3600*24) : ?>
                        <a onclick="return confirm('Вы хотите отправить ссылку?');" href="?option=com_viaggio&view=manualpayments&task=sendLink&id=<?php echo $item->id; ?>">
                            (Отправить ссылку)
                        </a>

                        <?php endif; ?>
                    </td>


                    <td class="center">
                        <?php echo $item->amountEuro; ?> 

        
 
                      евро
					   <?php
                        if ($item->curs) {
                          echo ($item->amountRub/100).'<br/>';
							
                            echo 'курс '.$item->curs.' от '.$item->payTime.'<br/>';
                        }
                        ?><br>						<a href="#modal-example3" uk-toggle class="uk-icon-link uk-margin-small-right" uk-icon="file-edit"></a>
<div id="modal-example3" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
         
						<div class="uk-text-muted">  У тебя не правельно сумма</div> <br/>
							
							</div> 
													</div> 
                    </td>
				
                    <td class="center">
                        <?php
                        if ($item->status == 0 && $payTime>0 && $curTime < $payTime+3600*24){
                            echo 'ЖдЁт оплаты<br/>'.
                                'https://www.viaggio-russia.com/index.php?option=com_viaggio&task=prepareBuyManual&payment_id='.$item->paymentID.'<br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать Заказ</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=cancelmanual&id='.$item->id.'">Отменить Заказ</a><br/>'.
                                'от '.$item->timeCreatedLinl.'<br/>'.
                                'до '.date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+3600*24);

                            echo '<nav class="uk-navbar-container uk-margin uk-navbar" uk-navbar="mode: click">
                                    <div class="uk-navbar-left">
                                        <ul class="uk-navbar-nav">
                                            <li>
                                                <a  href="?option=com_viaggio&task=payedPartial&payment_id='.$item->id.'">Оплатили</a>
                                            </li>
                                        </ul>
                                
                                    </div>
                                </nav>';
                        }
                        elseif ($item->status == 0 && $payTime>0 && $curTime >= $payTime+3600*24){
                            echo '<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать ссылку</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=deletemanual&id='.$item->id.'">Удалить заявку.</a><br/>'.
                                'закончился<br/>'.
                                date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+3600*24);
                        }
                        elseif ($item->status == 1){
                            echo 'Оплатил по ссылке!!!';
                        }
                        elseif ($item->status == 2){
                            echo 'Оплатил не по ссылке :(';
                        }
                        $date = explode(' ',$item->timeCreated);
                        $date = explode('-',$date[0]);
                        $str = array(
                            'paymentID='.$item->paymentID,
                            'mp_d='.$date[2],
                            'mp_m='.$date[1],
                            'mp_y='.$date[0],
                            'order_id='.$item->order_id,
                            'amountEur='.$item->amountEuro
                        );
                        ?>
				 
 
							<div class="uk-inline">
    <button class="uk-button uk-button-default" type="button">Invoce</button>
	<div uk-dropdown="mode: click">
    <ul class="uk-nav uk-dropdown-nav">
        <li class="uk-active"> <li class="uk-nav-header">СовкомБанк</li>
        <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM&<?php echo implode('&',$str); ?>">Скачать Invoce</a></li>
        <li><a  onclick="return confirm('Вы хотите отправить ссылку?');"  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Отправить Invoce</a></li>
	 <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM2&<?php echo implode('&',$str); ?>">Скачать Invoce Client</a></li>
        <li><a  onclick="return confirm('Вы хотите отправить ссылку?');"  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM2&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Отправить Invoce Client</a></li>
       	
		<li class="uk-nav-header">Тинькофф</li>
         <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareT&<?php echo implode('&',$str); ?>">Скачать Invoce</a></li>
       <li><a  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareT&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Оправить Invoce</a></li>  

	   </ul>
</div>
    
</div> 
					</td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>