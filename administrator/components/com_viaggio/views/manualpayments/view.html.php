<?php
ini_set('display_errors',1);
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Viaggio.
 */
class ViaggioViewManualpayments extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $form;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->form = $this->get('Form');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        ViaggioHelpersViaggio::addSubmenu('manualpayments');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/viaggio.php';

        $state = $this->get('State');
        $canDo = ViaggioHelpersViaggio::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_TOURISTINVITE_TITLE_MANUALPAYMENTS'));

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/manualpayments';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolBarHelper::addNew('manualpayment.add', 'JTOOLBAR_NEW');
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_viaggio');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_viaggio&view=manualpayments');

        $this->extra_sidebar = '';
    }

	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		);
	}

}
