<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_viaggio/assets/css/viaggio.css');
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_viaggio');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_viaggio&task=orders.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'orderList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	};

	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('#filter_search').val('');
			jQuery('#adminForm').submit();
		});
	});

	window.toggleField = function (id, task, field) {

		var f = document.adminForm,
			i = 0, cbx,
			cb = f[ id ];

		if (!cb) return false;

		while (true) {
			cbx = f[ 'cb' + i ];

			if (!cbx) break;

			cbx.checked = false;
			i++;
		}

		var inputField   = document.createElement('input');
		inputField.type  = 'hidden';
		inputField.name  = 'field';
		inputField.value = field;
		f.appendChild(inputField);

		cb.checked = true;
		f.boxchecked.value = 1;
		window.submitform(task);

		return false;
	};

</script>

<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
	$this->sidebar .= $this->extra_sidebar;
}

?>
<link rel="stylesheet" href="/images/css/uikit.css" />
  
 <script src="/images/js/uikit.min.js"></script>
 <script src="/images/js/uikit-icons.min.js"></script>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=orders'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<label for="filter_search"
						   class="element-invisible">
						<?php echo JText::_('JSEARCH_FILTER'); ?>
					</label>
					<input type="text" name="filter_search" id="filter_search"
						   placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>"
						   value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
						   title="<?php echo JText::_('JSEARCH_FILTER'); ?>"/>
				</div>
				<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="directionTable"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_ORDERING_DESC'); ?>
					</label>
					<select name="directionTable" id="directionTable" class="input-medium"
							onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
						<option value="asc" <?php echo $listDirn == 'asc' ? 'selected="selected"' : ''; ?>>
							<?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?>
						</option>
						<option value="desc" <?php echo $listDirn == 'desc' ? 'selected="selected"' : ''; ?>>
							<?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?>
						</option>
					</select>
				</div>
				<div class="btn-group pull-right">
					<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
					<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
					</select>
				</div>
			</div>
			<div class="clearfix"></div>
			<table class="table table-striped" id="orderList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
						</th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th></th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_CREATED_BY', 'a.`created_by`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_CURRENCY', 'a.`currency`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_ORDERSTATUS', 'a.`orderstatus`', $listDirn, $listOrder); ?>
				</th>
 
 
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_viaggio');
					$canEdit    = $user->authorise('core.edit', 'com_viaggio');
					$canCheckin = $user->authorise('core.manage', 'com_viaggio');
					$canChange  = $user->authorise('core.edit.state', 'com_viaggio');

                    $db = JFactory::getDbo();
                    $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$item->id."' AND status <> 0");
                    $manualpayments = $db->loadObjectList();
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							
						<?php endif; ?>

                        <td>
                            <a href="/administrator/index.php?option=com_viaggio&view=order&layout=edit&id=<?php echo $item->id; ?>"><?php echo $item->id; ?></a>
							 <br> <?php echo $item->tour_id; ?><br> 
							                           <?php echo date('d/m/Y',strtotime($item->tour->from)) ; ?> -   <?php echo date('d/m/Y',strtotime($item->tour->to)) ; ?> <br>
                             <?php echo $item->telephone; ?><br>
                           <?php echo $item->nomecgome; ?><br>
                             <?php echo $item->email; ?><br> 
							 Persona <a href="/administrator/index.php?option=com_viaggio&view=clients&filter_search=<?php echo $item->id; ?>"><?php echo $item->clientcount; ?></a> <br>
                        
				        </td>
                        <!--вывод ссылок на скачивание pdf и отправку мыла-->
                        <td>
                            <table>
                                <tr>
                                    <td>
									    <button class="uk-button uk-button-default" type="button">Pagare</button>
	                                    <div uk-dropdown="mode: click">
                                            <ul class="uk-nav uk-dropdown-nav">
                                                <li class="uk-active"> <li class="uk-nav-header">Pagare</li>
                                                <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfPagare">Скачать Pagare</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									    <button class="uk-button uk-button-default" type="button">Contratto</button>
                                        <div uk-dropdown="mode: click">
                                            <ul class="uk-nav uk-dropdown-nav">
                                                <li class="uk-active"> <li class="uk-nav-header">Скачать Contratto</li>
                                                <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoM">Contratto для физ. лиц.</a></li>
                                                <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoMF">Contratto для юр.лиц</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>  <?php echo $item->description; ?>
                         <?php
                         $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualpayments[0]->paymentID."'";
                         $db->setQuery($query);
                         $manualPaymentDetails = $db->loadObjectList();
                         $details = '';
                         foreach ($manualPaymentDetails as $manualPaymentDetail)
                         {
                             if ($manualPaymentDetail->field_value != '')
                             {
                                 $prefix = '';
                                 $postfix = '';
                                 switch ($manualPaymentDetail->field_group_name) {
                                     case 'air-ticket':
                                         $prefix = 'Авиа - ';
                                         break;
                                     case 'rail-ticket':
                                         $prefix = 'ЖД - ';
                                         break;
                                 }
                                 switch ($manualPaymentDetail->field_name) {
                                     case 'visa-ita':
                                         $prefix .= 'Виза (ita): ';
                                         break;
                                     case 'visa-rus':
                                         $prefix .= 'Виза (rus): ';
                                         break;
                                     case 'visa-amount':
                                         $prefix .= 'Стоимость визы: ';
                                         $postfix = '€';
                                         break;
                                     case 'other-ita':
                                         $prefix .= 'Другое (ita): ';
                                         break;
                                     case 'other-rus':
                                         $prefix .= 'Другое (rus): ';
                                         break;
                                     case 'other-amount':
                                         $prefix .= 'Другое - стоимость: ';
                                         $postfix = '€';
                                         break;
                                     case 'to-city-ita':
                                         $prefix .= 'в город (ita): ';
                                         break;
                                     case 'from-city-ita':
                                         $prefix .= 'из города (ita): ';
                                         break;
                                     case 'to-city-rus':
                                         $prefix .= 'в город (rus): ';
                                         break;
                                     case 'from-city-rus':
                                         $prefix .= 'из города (rus): ';
                                         break;
                                     case 'begin':
                                         $prefix .= 'с: ';
                                         break;
                                     case 'end':
                                         $prefix .= 'по: ';
                                         break;
                                     case 'amount':
                                         $prefix .= 'стоимость: ';
                                         $postfix = '€';
                                         break;
                                 }

                                 $details .= $prefix.$manualPaymentDetail->field_value.$postfix.'<br/>';
                             }
                         }
                        echo $details;
                        ?>
                        </td>
                        <td>
                                 <?php
                                foreach ($item->clients as $client)
                                    echo '<a href="?option=com_viaggio&view=client&layout=edit&id='.$client->id.'">'.$client->nome.' '.$client->cognome.'</a>  </br>  '.date('d/m/Y',strtotime($client->birthday)).'   '.$client->numero_di_passaporto.'<br>';
                            ?>
                        </td>
                      
 
                        <td>
                           
                            <div id="offcanvas-flip<?php echo $item->id; ?>status" uk-offcanvas="flip: true; overlay: true">
                                <div class="uk-offcanvas-bar">
 
                                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                                    <h3>Статус платежа</h3>
 
                                    <?php
                                    foreach ($manualpayments as $k=>$manualpayment)
                                    {
                                        echo 'PaymentID: '.$manualpayment->paymentID.'; ';
                                        echo '<br>Curs: '.$manualpayment->curs.'; ';
                                        echo '<br>Date: '.$manualpayment->timeCreated.'; ';
                                        echo '<br>Amount: '.$manualpayment->amountEuro.'; ';
                                        echo '<br>Status: '.$manualpayment->status.'.<br/>';
										 echo 'Сумма в рублях: '.$manualpayment->amountRub.'. руб<br/><br/>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div id="offcanvas-flip2" uk-offcanvas="flip: true; overlay: true">
                                <div class="uk-offcanvas-bar">
                                    <button class="uk-offcanvas-close" type="button" uk-close></button>
                                    Тут редактируеться клиенты которые поедут по туры.
                                    <h3>Клиенты</h3>
                                    <button class="uk-button uk-button-default" type="button" uk-toggle="target: #toggle-usage5">Фамииля Имя Клиента</button>
                                    <div id="toggle-usage5" hidden >
                                        <fieldset class="adminform">
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_cognome-lbl" for="jform_cognome">Cognome</label>
                                                </div>
                                                <div class="controls"><input type="text" name="jform[cognome]" id="jform_cognome" value="" placeholder="Cognome"></div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_nome-lbl" for="jform_nome">Nome</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" name="jform[nome]" id="jform_nome" value="" placeholder="Nome">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_sex-lbl" for="jform_sex">Sex</label>
                                                </div>
                                                <div class="controls">
                                                    <select id="jform_sex" name="jform[sex]" style="display: none;">
                                                        <option value="m">Maschile</option>
                                                        <option value="f">Femminile</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_birthday-lbl" for="jform_birthday">Birthday</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" id="jform_birthday" name="jform[birthday]" value="" class="inputbox" placeholder="Birthday" data-alt-value="" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_nazionalita-lbl" for="jform_nazionalita">Nazionalita</label>
                                                </div>
                                                <div class="controls">
                                                    <select id="jform_nazionalita" name="jform[nazionalita]" style="display: none;">
                                                        <option value="italiani">italiani</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_luogo_di_nascita-lbl" for="jform_luogo_di_nascita">citta_di_residenza</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" name="jform[luogo_di_nascita]" id="jform_luogo_di_nascita" value="">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_numero_di_passaporto-lbl" for="jform_numero_di_passaporto">numero_di_passaporto</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" name="jform[numero_di_passaporto]" id="jform_numero_di_passaporto" value="">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_date_from-lbl" for="jform_date_from" class="hasPopover" title="" data-content="=" data-original-title="date_from">passaporto_date_from</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" id="jform_date_from" name="jform[date_from]" value="" class="inputbox" data-alt-value="" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="control-label">
                                                    <label id="jform_date_to-lbl" for="jform_date_to" class="hasPopover" title="" data-content="=" data-original-title="date_to">passaporto_date_to</label>
                                                </div>
                                                <div class="controls">
                                                    <input type="text" id="jform_date_to" name="jform[date_to]" value="" class="inputbox" data-alt-value="" autocomplete="off">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <br/>
		                    <?php
                            if (count($manualpayments))
                            {
                                echo $item->valute_amount.' € = ';
                                $left_amount = $item->valute_amount;
                                foreach ($manualpayments as $k=>$manualpayment)
                                {
                                    $left_amount -= $manualpayment->amountEuro;
                                    ?>
                                    <a type="button" uk-toggle="target: #offcanvas-flip<?php echo $item->id;?>status"> <?php echo $manualpayment->amountEuro; ?> €  </a>
                                    <?php
                                    if (count($manualpayments)-1 != $k)
                                        echo ' + ';
                                }
                                ?>
                                <br>
                                Остаток <a target="_blank" href="https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&data=<?php echo $item->id.'|'.$left_amount; ?>"><?php echo $left_amount; ?> € </a>
                                <?php
                            }
                            ?><br>
							<b>
                                <?php if (!$item->visa_id) { ?>
                                <a  href="/administrator/index.php?option=com_viaggio&task=sendToVisto&order_id=<?php echo $item->id; ?>">Отправить на visto</a>
                                <?php } else { ?>
                                    Виза: <a target="_blank" href="https://www.visto-russia.com/administrator/index.php?option=com_touristinvite&view=visaandinivte&layout=new&id=<?php echo $item->visa_id; ?>">visto-russia.com</a>
                                <?php } ?>
							Сумма в рублях <?php echo ($item->amount/100); ?>₽</b>
                        </td>

                        <td>
                           
							<a  type="button" class="uk-text-danger" uk-toggle="target: #offcanvas-flip2">.</a>
							
                        </td>
 
    
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>        
