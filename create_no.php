<?php
//Письмо приходит если нажат консультво, и проходит все параментры, и идет на оплату.
//$lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
//$participants = JFactory::getApplication()->input->get('participants',null,'array');

$Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'ho_registrazione_piter' => 'Registrazione San Pietroburgo',
    'ho_registrazione_moscow' => 'Registrazione Mosca'
);
if (!$lastRecord['urgent']) unset($Servizi['urgent']);
if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
if (!$lastRecord['ho_registrazione_piter']) unset($Servizi['ho_registrazione_piter']);
if (!$lastRecord['ho_registrazione_moscow']) unset($Servizi['ho_registrazione_moscow']);

$ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
//////////////////////
$add = array(
    'add_hotel' => 'hotel',
    'add_appartamento' => 'appartamento',
    'add_volo_treno' => 'volo/treno',
    'add_transfers' => 'transfers',
    'add_escursioni' => 'escursioni'
);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
$addTXT = count($add)?implode(',',$add):'';
$user = JFactory::getUser(); 
$userProfile = JUserHelper::getProfile( $user->get('id') );
?>
<?php $subject = "Richiesta pratica visto ricevuta e confermata."; ?>
<?php $body = '
<table class="  -webkit-text-stroke-width: 0px;"
        width="100%" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tbody>
          <tr align="center">
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><p><big><strong>
              Vi ringraziamo per la richiesta! A seguire un
                  riepilogo dei dati inseriti:</big></strong></p>
              
            </td>
          </tr>
           <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211); text-align: center;" valign="top">
              <p><span style="font-size: 20px;"><strong></strong></span><b><big>Costo

                della pratica ID № '.$lastRecord['id'].'</big></b>
			   <br><span style="font-size: 18px;"><strong><big>
                      ';
 if ($lastRecord['skidka_val']) : ?>
    <?php
	$body .= 'Promo code '.$lastRecord['skidka_name'].' <br> '.$lastRecord['total_cost'].' € - '.$lastRecord['skidka_val'].'% = ';
	$body .= $lastRecord['total_cost'] - $lastRecord['total_cost']*$lastRecord['skidka_val']/100;
    $body .= ' €';
    ?>
<?php else : ?>
    <?php	  
    $body .= '  '.$lastRecord['total_cost'].' € ';
    ?>
<?php endif; 
$body .= '
              </big></strong></span></p>
            </td>
          </tr>
          <tr>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di arrivo:<span
                  class="Apple-converted-space"> </span>
              <br>
              <strong><big>'.date('d/m/Y',strtotime($lastRecord['date_from'])).'</big></strong>
                
                
              </p>
            </td>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di partenza:<span
                  class="Apple-converted-space"> </span>
              
              <br><strong><big>'.date('d/m/Y',strtotime($lastRecord['date_to'])).'</big></strong>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><big><strong>Partecipanti al
                    viaggio</strong></big></em></td>
          </tr>';
    if (count($participants)) :
        foreach($participants as $participant) :
            $gender = ($participant['gender']=='f')?'female':'male';

            $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Nome: <strong>'.$participant['first_name'].'</strong> Cognome:<strong>'.$participant['second_name'].'</strong> Data

                di nascita: <strong>'.date('d/m/Y',strtotime($participant['birthdate'])).' </strong> Sesso: <strong>'.$gender.'</strong></div>
              <div>Nazionalita<strong> '.$participant['nationality'].'</strong> Numero di
                passaporto <strong>'.$participant['passport'].'</strong></div>
            </td>
          </tr>';
        endforeach;
		$body .= '<tr> <td  style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><div>Citta di Residenza: <strong> '.$participants[0]['citta_di_residenza'].'</strong></div></td>
          </tr>';
    endif;
	

        $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Riepilogo servizi
                    richiesti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Procedura completa:  '.$lastRecord['consulate'].'.</div>
              <div>Assicurazione: '.($lastRecord['insurance']?'Inclusa':'Non Inclusa').'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Citta` da visitare:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['cities_to_visit'].'</td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Servizi aggiuntivi:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>'.$ServiziTXT.'</div>
              <div>Spedizione: '.$lastRecord['shipping'].'</div>
              <div>Ho bisogno anche di: '.$addTXT.'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Commenti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['add_other'].'</td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Mail: <a moz-do-not-send="true"
                class="moz-txt-link-abbreviated"
                href="'.$user->get('email').'">'.$user->get('email').'</a></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Telefono:+  '.$userProfile->profile['phone'].' </td>
          </tr>

          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
<p style="text-align: justify;">
Utilizzando il nome utente e la password indicati al momento della registrazione, potrete accedere al <a href="https://www.visto-russia.com/ita/area-clienti"> vostro profilo personale nell\'area clienti</a>, da dove sara` possibile monitorare lo stato della richiesta attuale ed avviare nuove pratiche in futuro.

</p>

<p style="text-align: justify;">
    <b>Siamo lieti di informarla che siamo nei tempi utili per l’evasione della pratica nelle modalita` richieste, ed e` possibile procedere all’avvio della
    pratica per l’ottenimento del visto russo.
</b>
</p>
<p style="text-align: justify;">
    <strong>Attenzione</strong>
    : la pratica viene avviata soltanto una volta effettuato il pagamento, se i tempi sono stretti e` consigliabile procedere immediatamente. Se il pagamento
    avviene dopo un certo numero di giorni potrebbe poi essere necessaria una integrazione per la procedura urgente o si potrebbe non fare piu` in tempo ad
    evadere la richiesta.
</p>
<p>
    
     <a  target="_blank" href="https://www.visto-russia.com/?option=com_viaggio&view=step3&id='.$lastRecord['id'].'">Per procedere al pagamento cliccare qui.</a>
</p>

<div style="text-align: justify;">
    Una volta effettuato il pagamento riceverete l’indirizzo al quale consegnare o spedire la seguente documentazione, per avviare la pratica visto:

<ul type="disc">
    <li>
        Passaporto con validità di almeno 6 mesi superiore alla data di previsto rientro e con almeno due pagine libere. Il passaporto deve essere in buone
        condizioni.
    </li>
    <li>
        Due fototessere a colori, su sfondo bianco, formato 35x45 mm. Le foto devono essere scattate da non più di sei mesi ed il volto deve occupare circa il
        70-80% della foto.
    </li>
    <li>
	Una copia del modulo richiesta d\'ingresso, da compilare direttamente online sul sito del consolato. Il modulo sarà da noi compilato, dovrete al riguardo fornirci delle informazioni aggiuntive dopo il pagamento.
    </li>
    
	<li>
       Una copia del <a  target="_blank" href="https://www.visto-russia.com/images/richiesta_incarico.pdf">modulo richiesta d\'incarico</a>
    </li>
</ul></div>
<p style="text-align: justify;">
    Con la procedura ordinaria il visto sara` pronto in circa 15 giorni dal momento della ricezione della documentazione da parte dei nostri corrispondenti. La
    documentazione sara\' presentata in consolato il giorno lavorativo successivo alla ricezione, ed il visto sara\' pronto in 10 giorni lavorativi. Con la
    procedura urgente il visto sara` pronto in 4 giorni lavorativi.
</p>

<p>Cordiali saluti, </p>
<p>Lo Staff di Visto-Russia.com</p>

</td>
          </tr>
        </tbody>
      </table>

'; ?>