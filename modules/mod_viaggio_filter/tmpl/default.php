<?php
defined('_JEXEC') or die;
?>
<img   style="-webkit-filter: drop-shadow(2px 2px 1px #000); filter: drop-shadow(2px 2px 1px #000);" src="/images/homepage/logo_small3.png">
<p class=" uk-text-bold uk-text-uppercase uk-text-white"  >Lasciatevi sorprendere</p>
<link href="/images/js/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="/images/js/datepicker.js"></script>

<button class="uk-button" data-uk-modal="{target:'#my-id'}">Date </button>

<!-- This is the modal -->
<div id="my-id" class="uk-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <a class="uk-modal-close uk-close"></a>
     

<div class="uk-width-medium-1-1">

  <div class="uk-grid uk-width-medium-1-1">
<div class="uk-width-medium-1-2 uk-text-left">
<?php foreach($categories as $category) { 
	$current_category = 0;
	$subcategories_count = 0;
	$subcategories_html = '';
	$current_category = $category->id;
	$current_category_title = $category->title;
	$current_category_path = $category->path;
	$current_category_id = $category->id;
	if($category->level == 1){
		foreach ($categories as $category) {
			if($category->parent_id == $current_category){ 
				$subcategories_html .= '
				<li>
					<a href="/'.$category->path.'" data.id="'.$category->id.'" class="loadCalendars">'.$category->title.'</a>
				</li>';
				$subcategories_count++;
			}
		}
		
		if($subcategories_count)
		{
			echo $current_category_title.'<ul class="uk-nav uk-nav-side">'.$subcategories_html.'</ul>';
		}else{
			echo '<a href="/'.$current_category_path.'"  class="loadCalendars" data.id="'.$current_category_id.'">'.$current_category_title.'</a>'.'<ul class="uk-nav uk-nav-side">'.$subcategories_html.'</ul>';
		}
	} 
} ?>
  
</div>

<div class="uk-width-medium-1-2">
  <div class="uk-grid uk-width-medium-1-1"><div class="uk-width-medium-1-2">
<div class="datepicker2"></div></div>
    <div class="uk-width-medium-1-2">
  <div class="datepicker3"></div>

  </div>
    </div>

    
</div>
  </div>
</div>

    </div>
</div>
<style>
.datapicker-tour-date{
	background: #45bced;
	border-radius: 0;
}
</style>
<script>

jQuery(document).ready(function($) {
	var tourDays = []
	var linkToDates = []
	var toursToDates = []
	var min = 0
	var max = 0
	var tourID = 0
	//var tourDays = ['25.4.2017','26.4.2017','27.4.2017','28.4.2017','29.4.2017','30.4.2017', '4.4.2017','20.4.2017','31.3.2017','1.5.2017','2.5.2017']

	//categories onclick event
	$('.loadCalendars').on('click', function(){
		tourID = $(this).attr('data.id')
		
		$.ajax({
			url: 'https://www.viaggio-russia.com/index.php?option=com_viaggio&task=get_tours_dates_by_category&category_id='+tourID, 
			dataType: 'json',
			success: function(data){
				tourDays = data.dates
				toursToDates = data.datestotour
				linkToDates = data.datestolinks
				min = data.min
				max = data.max
				//update tours dates
				updateDatepickers()
			}
		});
		
		return false
	});
	
	//create datepickers
	var datepicker = $('.datepicker2').datepicker().data('datepicker')
	var datepicker2 = $('.datepicker3').datepicker().data('datepicker')

	// set min dates
	datepicker2.next()
	
	//update tours dates
	updateDatepickers()
	
	function updateDatepickers()
	{
		updateDatepicker(datepicker)
		updateDatepicker(datepicker2)
		var minDate = new Date(min);
		var maxDate = new Date(max);
		if(minDate > 0 && maxDate > 0){
			datepicker.update('minDate', minDate)
		datepicker2.update('minDate', minDate)
		datepicker.update('maxDate', maxDate)
		datepicker2.update('maxDate', maxDate)
		//console.log(addDays(minDate,30))
		datepicker.date = new Date(minDate);
		datepicker2.date = new Date(minDate);
		//datepicker.update('startDate', minDate)
		//datepicker2.update('startDate', addDays(minDate,30) )
		datepicker2.next()
		}
	}
	
	function updateDatepicker(dp){
		// ���������� ���������� ����������
		dp.update({
			onRenderCell: function (date, cellType) {
			var currentDate = date.getDate();

			// ��������� ��������������� �������, ���� ����� ���������� � `eventDates`
			//if (cellType == 'day' && tourDays.indexOf(currentDate) != -1) {
			//console.log(dateToNormalFormat(date))
			if (cellType == 'day' && tourDays.indexOf(dateToNormalFormat(date)) != -1) {
				//console.log(currentDate)
				return {
					//html: currentDate + '<span class="dp-note">.</span>'
					classes: 'datapicker-tour-date'
				}
			}else{
				return {
					disabled: true
				}
			}
		},
		onSelect: function onSelect(fd, date) {
			var title = '', content = ''
			var selectedDay = tourDays.indexOf(dateToNormalFormat(date))
			// ���� ������� ���� � ��������, �� ���������� ���
			if (date && selectedDay != -1) {
				title = fd;
				 window.location.href = linkToDates[selectedDay]
				//console.log(linkToDates[selectedDay])
				//alert(selectedDay)
				//alert(tourDays[selectedDay])
				//tourDataPicker1.destroy() 
			}
		},
		})
	}
	
	function dateToNormalFormat(date){
		return date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
	}
	
	function addDays(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() + days);
		return result;
	}
});
</script>