<?php

defined('_JEXEC') or die;

$cat_id = $params->get('cat_id', 0);
$text1 = $params->get('text1', '');
$itemid = $params->get('itemid','');
$ukmodaldialog = $params->get('uk-modal-dialog','');
$ukmodal = $params->get('uk-modal','');
$beforecontent = $params->get('before-content','');
$aftercontent = $params->get('after-content','');

$db = JFactory::getDbo();

$the_sql = "SELECT 
cats.id as catid, 
cats.parent_id, 
tours.* 
FROM `#__categories` as cats 
LEFT JOIN `#__viaggio_tours` as tours ON cats.id = tours.category 
WHERE (tours.category = ".intval($cat_id)." OR cats.parent_id = ".intval($cat_id).") 
AND tours.from > (NOW() + INTERVAL 10 DAY) 
AND `state` =1 
ORDER BY tours.from  ASC";


$db->setQuery($the_sql);
$items = $db->loadObjectList();

require JModuleHelper::getLayoutPath('mod_viaggio2');

