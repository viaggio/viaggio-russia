<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_whosonline
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHTML::_('behavior.modal');
?>
<div class="uk-width-medium-1-1 uk-text-justify">
	<?php echo $text1; ?>
<a class="uk-button uk-width-1-1" rel=”nofollow” id="open-modal<?php echo $cat_id; ?>" data-uk-modal="{target:'#modal<?php echo $cat_id; ?>'}">Calendario partenze e prezzi</a>
</div>

<div id="modal<?php echo $cat_id; ?>" class="uk-modal <?php echo $ukmodal; ?>">
		<?php echo $beforecontent; ?>
		<div class="uk-modal-dialog uk-modal-dialog-large <?php echo $ukmodaldialog; ?>"  id="modal-html<?php echo $cat_id; ?>">
			<a href="" class="uk-modal-close uk-close"></a>
            <?php if (isset($items)) {
                foreach ($items as $i => $item) {
                    ?>
            <a href="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>">
                <div class="uk-grid tm-slideset-avion">
                    <div class="uk-width-medium-3-10  ">
                        <img class="uk-align-left" src="<?php if($item->category_img){ echo $item->category_img; }else{
                            echo '/components/com_viaggio/media/images/noimage.gif';
                        } ?>" alt="<?php echo $item->category_img_alt; ?>" title="<?php echo $item->category_img_title; ?>>
    </div>
   <div class="uk-width-medium-7-10">
                        <h2><?php echo $item->name; ?></h2>
                        <div class="uk-grid  ">
                            <div class="uk-width-1-2 uk-text-left">
                                <span class="tm-article-date-day"><?php echo date('d', strtotime($item->from)); ?> </span>
                                <span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($item->from); ?> </span>
                                <span class="tm-article-date-day"><?php echo date('Y', strtotime($item->from)); ?></span>
                            </div>
                            <div class="uk-width-1-2 uk-text-right">

                                <span class="tm-article-date-day"><?php echo date('d', strtotime($item->to)); ?> </span>
                                <span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($item->to); ?> </span>
                                <span class="tm-article-date-day"> <?php echo date('Y', strtotime($item->to)); ?> </span>

                            </div>
                        </div>
                        <div class="uk-text-justify">
                            <?php echo $item->desc; ?>
                            <br>Duration: <?php echo ViaggioHelpersFrontend::detDuration($item->from,$item->to); ?> days
                        </div>
                        <div class="uk-text-right uk-text-large"><?php echo ViaggioHelpersFrontend::getCost( $item->id, 0, 2 )/2; ?>€</div>

                    </div>
                </div></a>
            <?php }
            } ?>
		</div>
		<?php echo $aftercontent; ?>
</div>

<?php if (!isset($items)) { ?>
<script>
jQuery(document).ready(function($) {            
    $('#open-modal<?php echo $cat_id; ?>').on('click', function(){
		$.ajax({
			url: '/index.php?option=com_viaggio&view=tours&Itemid=<?php echo $itemid; ?>&cat_id=<?php echo $cat_id.'&format=ajax'; ?>',
			success: function(data){
				$('#modal-html<?php echo $cat_id; ?>').html('<a href="" class="uk-modal-close uk-close"></a>'+data)
			}
		})
	})
}); 
</script>
<?php } ?>