<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_whosonline
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$months = array(
    'January' => 'Gennaio',
    'February' => 'Febbraio',
    'March' => 'Marzo',
    'April' => 'Aprile',
    'May' => 'Maggio',
    'June' => 'Giugno',
    'July' => 'Luglio',
    'August' => 'Agosto',
    'September' => 'Settembre',
    'October' => 'Ottobre',
    'November' => 'Novembre',
    'December' => 'Dicembre'
);
/*?>

<?php if ($showmode == 0 || $showmode == 2) : ?>
	<?php $guest = JText::plural('MOD_WHOSONLINE_GUESTS', $count['guest']); ?>
	<?php $member = JText::plural('MOD_WHOSONLINE_MEMBERS', $count['user']); ?>
	<p><?php echo JText::sprintf('MOD_WHOSONLINE_WE_HAVE', $guest, $member); ?></p>
<?php endif; ?>

<?php if (($showmode > 0) && count($names)) : ?>
	<ul  class="whosonline<?php echo $moduleclass_sfx ?>" >
	<?php if ($params->get('filter_groups')):?>
		<p><?php echo JText::_('MOD_WHOSONLINE_SAME_GROUP_MESSAGE'); ?></p>
	<?php endif;?>
	<?php foreach ($names as $name) : ?>
		<li>
			<?php echo $name->username; ?>
		</li>
	<?php endforeach;  ?>
	</ul>
<?php endif;
*/?>
 
<div class="uk-width-medium-1-1 uk-text-justify">
<?php echo $text1; ?>
    <?php foreach($tours as $tour){ ?>

	<?php //echo date('d', strtotime($tours[0]->from)); ?>
 
        <a class="uk-modal-close uk-close"></a>
        
  
<a href="<?php echo JRoute::_( 'index.php?option=com_viaggio&view=showtour&withoutcat=1&id='.$tour->id.$tour->Itemid ); ?>">
    <div class="tm-slideset-avion">
		<h2 class="tm-h2 uk-text-center"><?php echo $tour->name; ?></h2>
		<div class="uk-grid  ">
<div class="uk-width-1-2 uk-text-left">  
<span class="tm-article-date-day"><?php echo date('d', strtotime($tour->from)); ?> </span>   
<span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($tour->from); ?> </span>
  <span class="tm-article-date-month"><?php echo date('Y', strtotime($tour->from)); ?> </span>
  </div>
    <div class="uk-width-1-2 uk-text-right">  
  
<span class="tm-article-date-day"><?php echo date('d', strtotime($tour->to)); ?> </span>   
<span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($tour->to); ?> </span>
  <span class="tm-article-date-month"><?php echo date('Y', strtotime($tour->to)); ?> </span>

  </div>
	</div>
		
		
		
		<div class="tm-block"><?php echo $tour->desc; ?></div>
	</a>
  </div>
 
	<?php } ?>
 
</div>
