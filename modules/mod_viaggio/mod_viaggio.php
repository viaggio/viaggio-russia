<?php

defined('_JEXEC') or die;

// Include the whosonline functions only once
//JLoader::register('ModViaggioHelper', __DIR__ . '/helper.php');

/*
$showmode = $params->get('showmode', 0);

if ($showmode == 0 || $showmode == 2)
{
	$count = ModViaggio::getOnlineCount();
}

if ($showmode > 0)
{
	$names = ModWhosonlineHelper::getOnlineUserNames($params);
}

$linknames = $params->get('linknames', 0);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_whosonline', $params->get('layout', 'default'));
*/

$cat_id = $params->get('cat_id', 0);

$db = JFactory::getDbo();
//SELECT * FROM `dwhnz_viaggio_tours` WHERE `from` > (NOW() - INTERVAL 10 DAY)
$query2 = "SELECT * FROM `#__viaggio_tours` WHERE `from` > (NOW() - INTERVAL 10 DAY) AND `state` =1 LIMIT 6";

if($cat_id) $query2 = "SELECT cats.id as catid, cats.parent_id, tours.* FROM `#__categories` as cats LEFT JOIN `#__viaggio_tours` as tours ON cats.id = tours.category WHERE (tours.category = $cat_id OR cats.parent_id = $cat_id) AND tours.from > (NOW() + INTERVAL 10 DAY) AND `state` =1 LIMIT 6";

//echo $query2;

$db->setQuery($query2);
$tours = $db->loadObjectList();

if(count($tours)){
	foreach($tours as $key=>$tour){
		$tours[$key]->Itemid = '';
		$search = '{"cat_id":"'.$tour->category.'"%';
		$query = "SELECT id FROM `#__menu` WHERE link = 'index.php?option=com_viaggio&view=tours' AND params LIKE '$search'";
		$db->setQuery($query);
		$result = $db->loadResult();
		if($result) $tours[$key]->Itemid = '&Itemid='.$result;
	}
}

$text1 = $params->get('text1', '');
//$text2 = $params->get('text2', '');
if(count($tours)) require JModuleHelper::getLayoutPath('mod_viaggio');