<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Viaggio', JPATH_COMPONENT);
JLoader::register('ViaggioController', JPATH_COMPONENT . '/controller.php');

include('helpers/settings.php');

// Execute the task.
$controller = JControllerLegacy::getInstance('Viaggio');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
