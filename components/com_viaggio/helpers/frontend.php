<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
defined('_JEXEC') or die;

/**
 * Class ViaggioHelpersFrontend
 *
 * @since  1.6
 */

class ViaggioHelpersFrontend
{
    private $atolVersion = 'v4';
    private $atolUrl = 'https://online.atol.ru/possystem/';
    private $atolGroupCode = 'visto-russia-com_6865';//'v3-online-atol-ru_4178';
    private $atolLogin = 'visto-russia-com';//'v3-online-atol-ru';
    private $atolPass = '7yO4w0C03';//'ejc5iyl4g';
    private $atolInn = '7802853888';//'5544332219';
    private $atolPaymentAddress = 'https://v4.online.atol.ru';

	public static function getUserId($email)
	{
			// Initialise some variables
			$db = JFactory::getDbo();

			$query = 'SELECT id FROM #__users WHERE email = ' . $db->Quote($email);
			$db->setQuery($query, 0, 1);
			return $db->loadResult();
	}
	/*
-1
Заказ создан на сайте
0
Хотят оплатить по карте. Заказ зарегистрирован , но не оплачен
1
По карте. Предавторизованная сумма захолдирована (для двухстадийных платежей)
2
Оплатили по карте. Проведена полная авторизация суммы заказа
3
По карте. Авторизация отменена
4
По карте. По транзакции была проведена операция возврата
5
По карте. Инициирована авторизация через ACS банка-эмитента
6
По карте. Авторизация отклонена
7
резерв
8
Хотят оплатить по банк счету
9
Оплатили по банк счету
	*/
	public static function changeOrderStatus($order_id, $order_status)
	{
		$db = JFactory::getDbo();
		$query = "UPDATE `#__viaggio_orders` SET `orderstatus` = '$order_status' WHERE `id` = '$order_id'";
		$db->setQuery($query);
		return $db->execute();
	}

	public static function createClient($clientArray, $order_id, $user_id, $tour_id){
		$query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `tour_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`, `numero_di_passaporto`) VALUES (NULL, '', '".$user_id."', '$tour_id', '".$clientArray['cognome']."', '".$clientArray['nome']."', '".$clientArray['sex']."', '".ViaggioHelpersFrontend::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '', '', '$order_id', '".$clientArray['numero_di_passaporto']."');";// return $query;
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$result = $db->execute();

		return $db->insertid();
	}

	public static function changePwd($user_id, $password)
	{
		$object = new stdClass();
		$object->password = JUserHelper::hashPassword($password);
		$object->id = $user_id;

		$result = JFactory::getDbo()->updateObject('#__users', $object, 'id');

		return $result;
	}

	public static function registerUser($email,$password,$name,$phone)
	{
		if(!JUserHelper::getUserId($email))
		{
			$credentials = array( 'username'=>$email, 'password'=>$password );

            $user = JUser::getInstance(0);
            $data = array(
                'name' => $name,
                'username' => $email,
                'password' => $password,
                'password2' => $password,
                'email' => $email,
                'profile' => array('phone'=>$phone),
                'groups' => array(2)
            );
            // Bind the data.
            if (!$user->bind($data))
            {
                //var_dump($user->getError());exit;
            }

            // Store the data.
            if (!$user->save())
            {
                //var_dump($user->getError());exit;
            }

			//авторизация
			$options = array( 'remember'=>true ); //здесь параметры авторизации - запоминаем пользователя
			JFactory::getApplication()->login( $credentials, $options );
			//print_r($credentials);
			//echo $query; die();

			return JFactory::getUser( $user->id );
		}
			//echo JUserHelper::getUserId($email);
		if(false){
			echo '<pre>';
			//var_dump( $query);
			//var_dump($password);
			//var_dump($email);
			//var_dump($db->insertid());
			//var_dump($db->explain());
			echo '</pre>';
		}
		return false;
	}

	public static function getValuteRate()
	{
		$url = 'http://www.cbr.ru/scripts/XML_daily.asp';
		if($answer = @file_get_contents($url)){
			$index = new SimpleXMLElement($answer);
			foreach($index as $val)
			{
				if($val->NumCode == 978){
					$valute_rate = str_replace(',','.',$val->Value);

					//сохраняем значение на будущее
					$fd = fopen(JPATH_COMPONENT_SITE.'/media/txt/cbr978.txt', 'w');
					fwrite($fd, $valute_rate);
					fclose($fd);


					return $valute_rate;
				}
			}
		}

		//если не удалось получить курс с интернета, грузим сохраненный
		return file_get_contents(JPATH_COMPONENT_SITE.'/media/txt/cbr978.txt');
	}

	public static function getItemDefaultCost($item,$category=0)
	{
		//506(Стоимость гостиницы 3*)+28(Стоимость услуг)+65(Консульский сбор)+16(Стоимость страховки на 7 день)+(20/2)(Доставка визы делим на 2) = 625 евро

		//get settings to item
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$item->statndarvisa = $params->get('statndarvisa');
		//$item->urgentevisa = $params->get('urgentevisa');
		$item->sendvisa = $params->get('sendvisa');
		$item->visa7day = $params->get('visa7day');
		//$item->visa1day = $params->get('visa1day');
		$item->servicecost = $params->get('servicecost');

		$cost = 0;

		//Если нету 3 звезды то считать 4 звездночные!
		if( $item->inn3cost ){
			$cost = $item->inn3cost;
		}else{
			$cost = $item->inn4cost;
		}

		//если без звезд
		if( $item->nostell1 ) $cost = $item->nostell1;

		//506(Стоимость гостиницы 3*)+28(Стоимость услуг)+65(Консульский сбор)+16(Стоимость страховки на 7 день)+(20/2)(Доставка визы делим на 2) = 625 евро
		//return $cost.'+'.$item->servicecost.' + '.$item->statndarvisa.' + '.$item->visa7day.' + '.($item->sendvisa/2).'='.($cost + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa/2);
		if($category){
			$cost = $cost + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa/2;
		}else{
			$cost = $item->singola3 + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa;
		}

		return $cost;
	}

	public static function saveOrderArray($object)
	{
		$result = JFactory::getDbo()->updateObject('#__viaggio_orders', $object, 'id');
	}

	public static function saveClientObject($object)
	{
		$db = JFactory::getDbo();
		$query = "UPDATE `#__viaggio_clients` SET ";

		if(isset($object->cognome)) $query .= "`cognome` = '$object->cognome', ";

		if(isset($object->nome)) $query .= "`nome` = '$object->nome', ";

		if(isset($object->luogo_di_nascita)) $query .= "`luogo_di_nascita` = '$object->luogo_di_nascita', ";

		if(isset($object->serie_passaporto)) $query .= "`serie_passaporto` = '$object->serie_passaporto', ";

		if(isset($object->numero_di_passaporto)) $query .= "`numero_di_passaporto` = '$object->numero_di_passaporto', ";

		if(isset($object->date_from)) $query .= "`date_from` = '$object->date_from', ";

		if(isset($object->date_to)) $query .= "`date_to` = '$object->date_to', ";

		if(isset($object->numero_di_visite)) $query .= "`numero_di_visite` = '$object->numero_di_visite', ";

		if(isset($object->last_visit_from)) $query .= "`last_visit_from` = '$object->last_visit_from', ";

		if(isset($object->last_visit_to)) $query .= "`last_visit_to` = '$object->last_visit_to', ";

		if(isset($object->indirizzo_di_residenza)) $query .= "`indirizzo_di_residenza` = '$object->indirizzo_di_residenza', ";

		if(isset($object->nome_dell_organizzazione)) $query .= "`nome_dell_organizzazione` = '$object->nome_dell_organizzazione', ";

		if(isset($object->ufficio_dell_organizzazione)) $query .= "`ufficio_dell_organizzazione` = '$object->ufficio_dell_organizzazione', ";

		if(isset($object->indirizzo_dell_organizzazione)) $query .= "`indirizzo_dell_organizzazione` = '$object->indirizzo_dell_organizzazione', ";

		if(isset($object->telefono_dell_organizzazione)) $query .= "`telefono_dell_organizzazione` = '$object->telefono_dell_organizzazione', ";

		if(isset($object->birthday)) $query .= "`birthday` = '$object->birthday', ";

		if(isset($object->telephone)) $query .= "`telephone` = '$object->telephone', ";

		$query .= "`edited` = '1' ";

		$query .="
		WHERE `id` = '$object->id' AND 
		`created_by` = '$object->created_by'";
		$db->setQuery($query);
		return $db->execute();
	}

	public static function getOrderObject($order_number)
	{
		$db = JFactory::getDbo();
		$query2 = "SELECT * FROM `#__viaggio_orders` where id = ".intval($order_number);
		$db->setQuery($query2);
		$result = $db->loadObject();
		if($result){
			//получаем списки клиентов
			$query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = $order_number";
			$db->setQuery($query);
			$result->clients = $db->loadObjectList();
			//получаем информацию о туре
            $query = "SELECT * FROM `#__viaggio_tours` WHERE id = $result->tour_id";
            $db->setQuery($query);
            $result->tour = $db->loadObject();
            //получаем информацию о категории тура
            $query = "SELECT * FROM `#__categories` WHERE id = ".$result->tour->category;
            $db->setQuery($query);
            $result->category = $db->loadObject();
			return $result;
		}
		$result = new stdClass();
		$result->order_id = 0;
		$result->clients = array();
		return $result;
	}

	public static function paymentApiGetOrderStatus($orderId, $settings)
	{
		$paramsGetORder = '?userName='.$settings->paymentApi->userName.'&password='.$settings->paymentApi->password.'&orderId='.$orderId;
		//$url = 'https://web.rbsuat.com/rebpayment/rest/getOrderStatus.do'.$paramsGetORder;
		$url = 'https://3dsec.paymentgate.ru/ipay/rest/getOrderStatus.do'.$paramsGetORder;
		$answer = file_get_contents($url);
		$answer = json_decode($answer);
		return $answer;
	}
	public static function paymentApiRegisterOrder($orderNumber, $amount, $settings)
	{
		$postdata = implode('&',array(
		    'mode=1',
		    'lang=EN',
		    'amount='.$amount,
            'currency='.'643',
            'reference='.$orderNumber,
            'description='.'order number '.$orderNumber,
            'sector='.$settings->paymentApi->best2paySector,
            'url='. $settings->paymentApi->returnUrl.'/index.php?option=com_viaggio&view=paid&order_number='.$orderNumber.'&result=success',
            'failurl='. $settings->paymentApi->returnUrl.'/index.php?option=com_viaggio&view=paid&order_number='.$orderNumber.'&result=fail',
            'signature='.base64_encode(md5($settings->paymentApi->best2paySector.$amount.'643'.$settings->paymentApi->best2paySignaturePassword))
        ));
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'content' => $postdata
            )
        );

        file_put_contents('/home/viaggio/web/viaggio-russia.com/request.txt',date('Y-m-d H:i:s').' request: '.$postdata, FILE_APPEND);

        $context  = stream_context_create($opts);

        $answer = file_get_contents($settings->paymentApi->best2payUrl.'Register', false, $context);

		return $answer;
	}

	public static function tinkoffInitOrder($orderNumber, $amount, $settings, $email)
    {
        $postdata =
            array(
                'TerminalKey' => $settings->tinkoffTerminalKey,
                'Amount' => $amount,
                'OrderId' => $orderNumber,
                'Language' => 'en',
                'Receipt' => array(
                    'Email' => $email,
                    'Taxation' => 'usn_income_outcome',
                    'Items' => array(
                        array(
                            'Name' => 'Туристический продукт',
                            'Price' => $amount,
                            'Quantity' => 1,
                            'Amount' => $amount,
                            'Tax' => 'vat0'
                        )
                    )
                )
        );
        $forToken =  array(
            'TerminalKey' => $settings->tinkoffTerminalKey,
            'Amount' => $amount,
            'OrderId' => $orderNumber,
            'Language' => 'en',
            'Password' => $settings->tinkoffPassword
        );
        $token = '';
        ksort($forToken);

        foreach ($forToken as $arg) {
            if (!is_array($arg)) {
                $token .= $arg;
            }
        }

        //echo 'Token stgring before: '.$token;        echo '<hr/>';

        $token = hash('sha256', $token);

        //echo 'Token stgring after: '.$token;        echo '<hr/>';

        $postdata['Token'] = $token;

        $postdata = json_encode($postdata,JSON_UNESCAPED_UNICODE);

        //echo 'Postadata: '.$postdata;        echo '<hr/>';

        //echo 'Post url: '.$settings->tinkoffUrl;        echo '<hr/>';

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $settings->tinkoffUrl);
            // curl_setopt($curl, CURLOPT_PROXY, $proxy);
            // curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyAuth);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            ));

            $out = curl_exec($curl);
            $response = $out;

            $json = json_decode($out);

            //echo 'Response: ';            var_dump($json);
//            Debug::trace($out);
//            Debug::trace($json->Success, true);


            curl_close($curl);
        } else {
            var_dump('Can not create connection to ' . $settings->tinkoffUrl . ' with args ' . $postdata);
        }

        if ($json->Success && $json->Status == 'NEW')
            return $json->PaymentURL;
        else
        {
            var_dump($settings->tinkoffUrl,$postdata,$json);exit;
            return 'https://www.viaggio-russia.com?initOrder=false';
        }
    }

	public static function sendEmailByTemplate($template,$email,$settings, $item = '', $attachment = false)
	{
		//отправка email уведомления
		if (stripos($email,'viaggio@russiantour.com')===false){
            $email .= ',viaggio@russiantour.com';
        }
		/* сообщение */
		include(JPATH_COMPONENT_SITE.'/templates/'.$template.'.php');

		if(isset($settings->timur)){
			echo '<pre>';
			var_dump ($email);
			var_dump ($headers);
			var_dump ($send);
			var_dump ($subject);
			echo '</pre>';
			echo $body;
		}else{
			/* и теперь отправим из */
            $mailer = JFactory::getMailer();
            $emails = explode( ',', $email);
            if(count($emails)>1){
                $mailer->addRecipient($emails);
            }else{
                $mailer->addRecipient($email);
            }
            $mailer->setSubject($subject);
            $mailer->isHTML(true);
            $mailer->setBody($body);

            if($attachment) $mailer->AddAttachment($attachment);

            $send = $mailer->Send();
            if ($send !== true) {
                //echo 'Error sending email: '.$send->message;
                return false;
            } else {
                return true;
            }
		}
	}

	public static function sendEmail($body,$subject,$email,$settings, $item = '')
	{
		//отправка email уведомления
        if (stripos($email,'viaggio@russiantour.com')===false){
            $email .= ',viaggio@russiantour.com';
        }
		/* сообщение */
		if($settings->email->templatePath) include($settings->email->templatePath);


		/* и теперь отправим из */
		//mail($email, $subject, $body, $headers);
		$mailer = JFactory::getMailer();
		$mailer->addRecipient($email);
		$mailer->setSubject($subject);
		$mailer->isHTML(true);
		$mailer->setBody($body);

		$send = $mailer->Send();
		if ($send !== true) {
			//echo 'Error sending email: '.$send->message;
			return false;
		} else {
			return true;
		}
	}


	public static function sendSmsByTemplate($template, $telephone,$settings, $item)
	{
		include(JPATH_COMPONENT_SITE.'/templates/'.$template.'.php');

		$src = '<?xml version="1.0" encoding="UTF-8"?>
			<SMS>
				<operations>
				<operation>SEND</operation>
				</operations>
				<authentification>
				<username>'.$settings->sms->username.'</username>
				<password>'.$settings->sms->password.'</password>
				</authentification>
				<message>
				<sentdate></sentdate>
				<sender>'.$settings->sms->sender.'</sender>
				<text>'.$text.'</text>
				</message>
				<numbers>
				<number>'.$telephone.'</number>
				</numbers>
			</SMS>';

		// messageID="msg1"
		if(isset($settings->timur)){
			echo '<pre>';
			var_dump ($src);
			var_dump ($telephone);
			var_dump ($text);
			var_dump ($settings->sms);
			echo '</pre>';
		}else{
			/* и теперь отправим из */
            $Curl = curl_init();
            $CurlOptions = array(
                CURLOPT_URL=>'http://api.atompark.com/members/sms/xml.php',
                CURLOPT_FOLLOWLOCATION=>false,
                CURLOPT_POST=>true,
                CURLOPT_HEADER=>false,
                CURLOPT_RETURNTRANSFER=>true,
                CURLOPT_CONNECTTIMEOUT=>15,
                CURLOPT_TIMEOUT=>100,
                CURLOPT_POSTFIELDS=>array('XML'=>$src),
            );
            curl_setopt_array($Curl, $CurlOptions);
            if(false === ($Result = curl_exec($Curl))) {
                throw new Exception('Http request failed');
            }

            curl_close($Curl);
            //var_dump($Result);
            return $Result;
		}
	}

	public static function sendSms($text,$telephone,$settings)
	{
		$src = '<?xml version="1.0" encoding="UTF-8"?>
			<SMS>
				<operations>
				<operation>SEND</operation>
				</operations>
				<authentification>
				<username>'.$settings->sms->username.'</username>
				<password>'.$settings->sms->password.'</password>
				</authentification>
				<message>
				<sentdate></sentdate>
				<sender>'.$settings->sms->sender.'</sender>
				<text>'.$text.'</text>
				</message>
				<numbers>
				<number>'.$telephone.'</number>
				</numbers>
			</SMS>';
		// messageID="msg1"
		$Curl = curl_init();
		$CurlOptions = array(
			CURLOPT_URL=>'http://api.atompark.com/members/sms/xml.php',
			CURLOPT_FOLLOWLOCATION=>false,
			CURLOPT_POST=>true,
			CURLOPT_HEADER=>false,
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_CONNECTTIMEOUT=>15,
			CURLOPT_TIMEOUT=>100,
			CURLOPT_POSTFIELDS=>array('XML'=>$src),
		);
		curl_setopt_array($Curl, $CurlOptions);
		if(false === ($Result = curl_exec($Curl))) {
			throw new Exception('Http request failed');
		}

		curl_close($Curl);

		return $Result;
	}
	/**
	* Get category name using category ID
	* @param integer $category_id Category ID
	* @return mixed category name if the category was found, null otherwise
	*/
	public static function getCategoryNameByCategoryId($category_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('title')
			->from('#__categories')
			->where('id = ' . intval($category_id));

		$db->setQuery($query);
		return $db->loadResult();
	}
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_viaggio/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_viaggio/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'ViaggioModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item)
    {
        $permission = false;
        $user       = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_viaggio'))
        {
            $permission = true;
        }
        else
        {
            if (isset($item->created_by))
            {
                if ($user->authorise('core.edit.own', 'com_viaggio') && $item->created_by == $user->id)
                {
                    $permission = true;
                }
            }
            else
            {
                $permission = true;
            }
        }

        return $permission;
    }

	/*
	//38 параметров калькулятора
	//hotel
	$inn4cost = 500;
	$inn3cost = 400;
	$halfboard4 = 100;
	$halfboard3 = 70;
	$singola3 = 700;
	$singola4 = 900;

	//cruise
	$nostell1 = 1000;
	$nostell2 = 600;

	//transib
	$classic = 10000;
	$classicsingola = 18000;
	$superior = 20000;
	$superiorsingola = 38000;
	$superiorplus = 30000;
	$superiorplussingola = 58000;
	$nostalgic = 40000;
	$nostalgicsingola = 70000;
	$bolshoy4 = 50000;
	$bolshoy4singola = 95000;
	$platinum5 = 75000;
	$platinum5singola = 140000;
	$russiandays = 10;

	//component settings
	$servicecost = 28;//стоимость услуг
	$statndarvisa = 65;//консульский сбор
	$urgentevisa = 45;//срочный конс сбор
	$sendvisa = 20;//доставка
	$visa7day = 16; //Стандартная страховка выдаеться на 7 дней. Она входит в формулу. Очень важно в формуле :  от 0(лет) до 3(лет)  умножаем на 2 _____ от 3(лет) до 65(лет)   умножаем на 0 ____  от 65(лет) до 80(лет) умножаем на 2 ___  от 80 (лет) до  вечности умножаем на 3
	$visa1day = 2;
	$visamongolia = 20;
	$visachina = 50;

	//drive
	$type = 'hotel';
	$subtype = '4';
	$count = 2;
	$halfboard = 0;
	$age1 = 33;
	$age2 = 5;
	$needvisamongolia = 0;
	$needvisachina = 0;
	$urgent = 0; //срочность
	*/
	public static function getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula)
	{
		/*echo "<script type='text/javascript'>console.log('inn4cost:$inn4cost,inn3cost:$inn3cost,halfboard4:$halfboard4,halfboard3:$halfboard3,singola4:$singola4,singola3:$singola3, type:$type, subtype:$subtype, count:$count, halfboard:$halfboard, nostell1:$nostell1, nostell2:$nostell2, classic:$classic,classicsingola:$classicsingola,superior:$superior,superiorsingola:$superiorsingola,superiorplus:$superiorplus,superiorplussingola:$superiorplussingola,nostalgic:$nostalgic,nostalgicsingola:$nostalgicsingola,bolshoy4:$bolshoy4,bolshoy4singola:$bolshoy4singola,platinum5:$platinum5,platinum5singola:$platinum5singola,russiandays:$russiandays, servicecost:$servicecost,statndarvisa:$statndarvisa,urgentevisa:$urgentevisa,sendvisa:$sendvisa,visa7day:$visa7day,visa1day:$visa1day,visamongolia:$visamongolia,visachina:$visachina, age1:$age1, age2:$age2, needvisamongolia:$needvisamongolia, needvisachina:$needvisachina, urgent:$urgent');</script>";*/
		$result = 0;
		$log = "type($type)|subtype($subtype)|";
		if($type == 'hotel')
		{
			//$result = 'hotel';
			if($subtype == 3)
			{
				//$result = '3*';
				if($count == 1)
				{
					$log .= "+ singola3($singola3)";
					$result = $result + $singola3;
					if($halfboard){
						$result = $result + $halfboard3;
						$log .= "+ halfboard3($halfboard3)";
					}
				}else if($count == 2)
				{
					$log .= "+ inn3cost*2($inn3cost*2)";
					$result = $result + ( $inn3cost * 2 );
					if($halfboard){
						$result = $result + ( $halfboard3*2 );
						$log .= "+ halfboard3*2($halfboard3*2)";
					}
				}
			}else if($subtype == 4)
			{
				//$result = '4*';
				if($count == 1)
				{
					$log .= "+ singola4($singola4)";
					$result = $result + $singola4;
					if($halfboard){
						$result = $result + $halfboard4;
						$log .= "+ halfboard4($halfboard4)";
					}
				}else if($count == 2)
				{
					$log .= "+ inn4cost*2($inn4cost*2)";
					$result = $result + ( $inn4cost * 2 );
					if($halfboard){
						$result = $result + ( $halfboard4*2 );
						$log .= "+ halfboard4*2($halfboard4*2)";
					}
				}
			}
		}else if($type == 'cruise')
		{
			//$result = 'cruise';
			if($count == 1)
			{
				$log .= "+( nostell1($nostell1) )";
				$result = $result + $nostell1;
			}else if($count == 2)
			{
				$log .= "+( nostell2($nostell2) )";
				$result = $result + ( $nostell2 * 2 );
			}
		}else if($type == 'transib')
		{
			//$result = 'transib';
			if($subtype == 'classic')
			{
				//$result = 'classic';
				if($count == 1)
				{
					$log .= "+( classicsingola($classicsingola) )";
					$result = $result + $classicsingola;
				}else if($count == 2)
				{
					$log .= "+( classic*2($classic*2) )";
					$result = $result + ( $classic * 2 );
				}
			}else if($subtype == 'superior')
			{
				//$result = 'superior';
				if($count == 1)
				{
					$log .= "+( superiorsingola($superiorsingola) )";
					$result = $result + $superiorsingola;
				}else if($count == 2)
				{
					$log .= "+( superior*2($superior*2) )";
					$result = $result + ( $superior * 2 );
				}
			}else if($subtype == 'superiorplus')
			{
				//$result = 'superiorplus';
				if($count == 1)
				{
					$log .= "+( superiorplussingola($superiorplussingola) )";
					$result = $result + $superiorplussingola;
				}else if($count == 2)
				{
					$log .= "+( superiorplus*2($superiorplus*2) )";
					$result = $result + ( $superiorplus * 2 );
				}
			}else if($subtype == 'nostalgic')
			{
				//$result = 'nostalgic';
				if($count == 1)
				{
					$log .= "+( nostalgicsingola($nostalgicsingola) )";
					$result = $result + $nostalgicsingola;
				}else if($count == 2)
				{
					$log .= "+( nostalgic*2($nostalgic*2) )";
					$result = $result + ( $nostalgic * 2 );
				}
			}else if($subtype == 'bolshoy4')
			{
				//$result = 'bolshoy4';
				if($count == 1)
				{
					$log .= "+( bolshoy4singola($bolshoy4singola) )";
					$result = $result + $bolshoy4singola;
				}else if($count == 2)
				{
					$log .= "+( bolshoy4*2($bolshoy4*2) )";
					$result = $result + ( $bolshoy4 * 2 );
				}
			}else if($subtype == 'platinum5')
			{
				//$result = 'platinum5';
				if($count == 1)
				{
					$log .= "+( platinum5singola($platinum5singola) )";
					$result = $result + $platinum5singola;
				}else if($count == 2)
				{
					$log .= "+( platinum5*2($platinum5*2) )";
					$result = $result + ( $platinum5 * 2 );
				}
			}
		}

		//стоимость услуг
		if($count == 1)
		{
			$log .= "+servicecost($servicecost)";
			$result = $result + $servicecost;
		}else if($count == 2)
		{
			$log .= "+servicecost*2($servicecost*2)";
			$result = $result + ( $servicecost * 2 );
		}

		//доставка
		$result = $result + $sendvisa;
		$log .= "+sendvisa($sendvisa)";

		if($needvisamongolia)
		{
			if($count == 1)
			{
				$log .= "+visamongolia($visamongolia)";
				$result = $result + $visamongolia;
			}else if($count == 2)
			{
				$log .= "+visamongolia*2($visamongolia*2)";
				$result = $result + ( $visamongolia * 2 );
			}
		}

		if($needvisachina)
		{
			if($count == 1)
			{
				$log .= "+visachina($visachina)";
				$result = $result + $visachina;
			}else if($count == 2)
			{
				$log .= "+visachina*2($visachina*2)";
				$result = $result + ( $visachina * 2 );
			}
		}

		//если срочный консульский сбор
		if($urgent)
		{
			if($count == 1)
			{
				$log .= "+urgentevisa($urgentevisa)";
				$result = $result + $urgentevisa;
			}else if($count == 2)
			{
				$log .= "+urgentevisa*2($urgentevisa*2)";
				$result = $result + ( $urgentevisa * 2 );
			}
		}

		//консульский сбор
		if($count == 1)
		{
			$log .= "+statndarvisa($statndarvisa)";
			$result = $result + $statndarvisa;
		}else if($count == 2)
		{
			$log .= "+statndarvisa*2($statndarvisa*2)";
			$result = $result + ( $statndarvisa * 2 );
		}

		$log .= "|russiandays:$russiandays|";

		//если стандартная виза или если стандартная виза + доп. виза
		if($russiandays < 8)
		{
			$log .= "|стандартная виза 7 дней|";
			if($age1 < 3)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
			}else if($age1 < 65 && $age1 >= 3)
			{
				$log .= "+visa7day*1($visa7day*1)";
				$result = $result + ($visa7day * 1);
			}else if($age1 < 80 && $age1 >= 65)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
			}else if($age1 >= 80)
			{
				$log .= "+visa7day*3($visa7day*3)";
				$result = $result + ($visa7day * 3);
			}

			if($count == 2)
			{
				if($age2 < 3)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
				}else if($age2 < 65 && $age2 >= 3)
				{
					$log .= "+visa7day*1($visa7day*1)";
					$result = $result + ($visa7day * 1);
				}else if($age2 < 80 && $age2 >= 65)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
				}else if($age2 >= 80)
				{
					$log .= "+visa7day*3($visa7day*3)";
					$result = $result + ($visa7day * 3);
				}
			}
		}else if($russiandays > 7)
		{
			$log .= "|стандартная виза 7 дней+доп|";
			if($age1 < 3)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
			}else if($age1 < 65 && $age1 >= 3)
			{
				$log .= "+visa7day*1($visa7day*1)";
				$result = $result + ($visa7day * 1);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 1 );
			}else if($age1 < 80 && $age1 >= 65)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
			}else if($age1 >= 80)
			{
				$log .= "+visa7day*3($visa7day*3)";
				$result = $result + ($visa7day * 3);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 3 );
			}

			if($count == 2)
			{
				if($age2 < 3)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
				}else if($age2 < 65 && $age2 >= 3)
				{
					$log .= "+visa7day*1($visa7day*1)";
					$result = $result + ($visa7day * 1);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 1 );
				}else if($age2 < 80 && $age2 >= 65)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
				}else if($age2 >= 80)
				{
					$log .= "+visa7day*3($visa7day*3)";
					$result = $result + ($visa7day * 3);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 3 );
				}
			}
		}
		$user = JFactory::getUser();
		if(!$user->guest && in_array(8,$user->groups)){
			if(isset($_REQUEST['view']) ){
				if($_REQUEST['view'] == 'showtour') return "<script>console.log('$log');jQuery('#Kdata').html('$log');jQuery('#Kdata').show();</script>$result";
			}
			if(isset($_REQUEST['task']) ){
				if($_REQUEST['task'] == 'calcTourCost') return "<script>console.log('$log');</script>$result";
			}
			if(isset($formula) ){
				if($formula) return $log.' = '.$result;
			}
		}
		return $result;
	}

	public static function getCost($tour_id,$subtype=0,$count=1,$age1=18,$age2=18,$halfboard = 0,
		$needvisamongolia = 0,
		$needvisachina = 0, $formula = 0)
	{
		/*echo "<script type='text/javascript'>console.log('getCost()_tour_id:$tour_id,subtype:$subtype, count:$count, age1:$age1, age2:$age2, halfboard:$halfboard, needvisamongolia:$needvisamongolia, needvisachina:$needvisachina');</script>";*/
		//get tour data
		$query = "SELECT * FROM `#__viaggio_tours` WHERE id = '$tour_id';";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$item = $db->loadObject();

		//get settings to plugin
		$app = JFactory::getApplication();
		$params = $app->getParams();

		//38 параметров калькулятора
		//hotel
		$inn4cost = $item->inn4cost;
		$inn3cost = $item->inn3cost;
		$halfboard4 = $item->halfboard4;
		$halfboard3 = $item->halfboard3;
		$singola3 = $item->singola3;
		$singola4 = $item->singola4;

		//cruise
		$nostell1 = $item->nostell1;
		$nostell2 = $item->nostell2;

		//transib
		$classic = $item->classic;
		$classicsingola = $item->classicsingola;
		$superior = $item->superior;
		$superiorsingola = $item->superiorsingola;
		$superiorplus = $item->superiorplus;
		$superiorplussingola = $item->superiorplussingola;
		$nostalgic = $item->nostalgic;
		$nostalgicsingola = $item->nostalgicsingola;
		$bolshoy4 = $item->bolshoy4;
		$bolshoy4singola = $item->bolshoy4singola;
		$platinum5 = $item->platinum5;
		$platinum5singola = $item->platinum5singola;
		$russiandays = $item->russiandays;

		//component settings
		$servicecost = $params->get('servicecost');//стоимость услуг
		$statndarvisa = $params->get('statndarvisa');//консульский сбор
		$urgentevisa = $params->get('urgentevisa');//срочный конс сбор
		$sendvisa = $params->get('sendvisa');//доставка
		$visa7day = $params->get('visa7day'); //Стандартная страховка выдаеться на 7 дней. Она входит в формулу. Очень важно в формуле :  от 0(лет) до 3(лет)  умножаем на 2 _____ от 3(лет) до 65(лет)   умножаем на 0 ____  от 65(лет) до 80(лет) умножаем на 2 ___  от 80 (лет) до  вечности умножаем на 3
		$visa1day = $params->get('visa1day');
		$visamongolia = $params->get('visamongolia');
		$visachina = $params->get('visachina');

		//drive
		$type = 'hotel';
		$urgent = 0; //срочность

		//дней до начала тура
		$daysTo = (strtotime( $item->from )-time())/3600/24;

		//продолжительность тура
		$daysFromTo = ( ( strtotime( $item->to )-strtotime( $item->from ) ) / 3600 / 24 ) +1;

		//if($daysTo < 10) return 0;

		//срочность, 20 или меньше дней, то срочно
		if($daysTo <= 20 && $daysTo >= 10) $urgent = 1;

		//тип тура
		if($nostell1 > 1) $type = 'cruise';
		if($russiandays > 1){ $type = 'transib'; }else{ $russiandays = $daysFromTo; }


		if($subtype == NULL)
		{
			if($type == 'cruise'){
				$subtype = '';
				if($nostell1 == 0 OR $nostell2 == 0) return 0;
			}else if($type == 'transib')
			{
				if($classic > 0){
					$subtype = 'classic';
				}else{
					if($superior > 0){
						$subtype = 'superior';
					}else{
						if($superiorplus > 0){
							$subtype = 'superiorplus';
						}else{
							if($nostalgic > 0){
								$subtype = 'nostalgic';
							}else{
								if($bolshoy4 > 0){
								$subtype = 'bolshoy4';
								}else{
									if($platinum5 > 0){
										$subtype = 'platinum5';
									}else{
										return 0;
									}
								}
							}
						}
					}
				}
			}else if($type == 'hotel')
			{
				if($inn3cost > 0){
					$subtype = '3';
				}else{
					if($inn4cost > 0){
						$subtype = '4';
					}else{
						return 0;
					}
				}
			}
		}
		//echo $type;

		//echo '<pre>'.print_r($item,1).'</pre>';
		return ViaggioHelpersFrontend::getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula);
	}

	public static function dateToSQLformat($date)
	{
		$date = str_replace('/','-',$date);
		$date_timestamp = strtotime($date);
		return date('Y-m-d', $date_timestamp);
	}

	public static function calculate_age($birthday) {
		$birthday = str_replace('/','-',$birthday);
		$birthday_timestamp = strtotime($birthday);
		$age = date('Y') - date('Y', $birthday_timestamp);
		if (date('md', $birthday_timestamp) > date('md')) {
			$age--;
		}
		return $age;
	}

	public static function detDuration($from, $to)
	{
		return ( ( strtotime( $to )-strtotime( $from ) ) / 3600 / 24 ) +1;
	}

	public static function getCategory($id)
	{
		$db = JFactory::getDbo();
		$query3 = 'SELECT * FROM `#__categories` where id = "'.$db->escape($id).'"';

		$db->setQuery($query3);
		return $db->loadObject();
	}

	public static function getLinkById($id)
	{

	}

	public static function printPdf($template, $item)
	{
		include(JPATH_COMPONENT_SITE.'/templates/'.$template.'.php');

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output($name.'.pdf', 'I');
		$pdf->Output(JPATH_COMPONENT_SITE.'/pdf/'.$name.'.pdf', 'F');

		return $name;
	}

    public static function sendPdf($template, $item)
    {
        include(JPATH_COMPONENT_SITE.'/templates/'.$template.'.php');

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $path = JPATH_COMPONENT_SITE.'/pdf/'.$name.'.pdf';
        $pdf->Output($path, 'F');
        return $path;
    }

	public static function month($month=''){
        $months = array(
            'Jan' => 'Gennaio',
            'Feb' => 'Febbraio',
            'Mar' => 'Marzo',
            'Apr' => 'Aprile',
            'May' => 'Maggio',
            'Jun' => 'Giugno',
            'Jul' => 'Luglio',
            'Aug' => 'Agosto',
            'Sep' => 'Settembre',
            'Oct' => 'Ottobre',
            'Nov' => 'Novembre',
            'Dec' => 'Dicembre'
        );
        $date = date('M', strtotime($month));
        return $months[$date];
    }

    public function makeCheck($orderId,$userEmail,$userPhone,$price){
        if (strpos($userPhone,'+')===false)
            $userPhone = '+'.$userPhone;
        $token = $this->atolGetToken();
        if ($token->token != ''){
            $resp = $this->atolRegisterIncome($token->token,$orderId,$userEmail,$userPhone,$price);
            file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/images/nalog/'.$orderId.'_2.txt',var_export($resp,1));
        }
    }

    private function atolGetToken(){
        $path = 'https://online.atol.ru/possystem/'.$this->atolVersion.'/getToken?login='.$this->atolLogin.'&pass='.$this->atolPass;

        $resp = file_get_contents($path);

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/images/nalog/token.txt',$path.' - '.var_export($resp,1));

        return json_decode($resp);
    }

    private function atolRegisterIncome($token,$orderId,$userEmail,$userPhone,$price){
        /*$postdata = array(
            'external_id' => $orderId,
            'timestamp' => $date,//date('d.m.Y H:i:s'),
            'service' => array(
                'inn' => $this->atolInn,
                'payment_address' => $this->atolPaymentAddress
            ),
            'receipt' => array(
                'attributes' => array(
                    'email' => $userEmail,
                    'phone' => $userPhone
                ),
                'items' => array(
                    array(
                        'name' => 'Туристический продукт',
                        'price' => $price,
                        'quantity' => 1,
                        'sum' => $price,
                        'tax' => 'none'
                    )
                ),
                'payments' => array(
                    array(
                        'type' => 1,
                        'sum' => $price
                    )
                ),
                'total' => $price
            )
        );

        $opts = array('http' =>
            array(
                'ignore_errors' => TRUE,
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => json_encode((object)$postdata)
            )
        );

        $path = 'https://online.atol.ru/possystem/'.
            $this->atolVersion.'/'.$this->atolGroupCode.'/sell?tokenid='.$token;*/

        $postdata = array(
            'external_id' => $orderId,
            'receipt' => array(
                'client' => array(
                    'email' => $userEmail,
                    'phone' => $userPhone
                ),
                'company' => array(
                    'email' => 'check@russiantour.com',
                    'sno' => 'usn_income_outcome',
                    'inn' => '7802853888',
                    'payment_address' => 'www.visto-russia.com'
                ),
                'items' => array(
                    array(
                        'name' => 'Туристический продукт',
                        'price' => $price,
                        'quantity' => 1,
                        'sum' => $price,
                        'payment_method' => 'full_prepayment',
                        'payment_object' => 'service',
                        'vat' => array(
                            'type' => 'none'
                        )
                    )
                ),
                'payments' => array(
                    array(
                        'type' => 1,
                        'sum' => $price
                    )
                ),
                'total' => $price
            ),
            'timestamp' => date('d.m.Y H:i:s')
        );

        $opts = array('http' =>
            array(
                'ignore_errors' => TRUE,
                'method'  => 'POST',
                'header'  => 'Content-type: application/json; charset=utf-8',
                'content' => json_encode((object)$postdata)
            )
        );

        $path = 'https://online.atol.ru/possystem/'.
            $this->atolVersion.'/'.$this->atolGroupCode.'/sell?token='.$token;

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/images/nalog/'.$orderId.'frontreq.txt',var_export($postdata,1));

        $context  = stream_context_create($opts);

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/images/nalog/path.txt',$path);

        $resp = file_get_contents($path, false, $context);
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/images/nalog/'.$orderId.'_1.txt',var_export($resp,1));

        $json = json_decode($resp);

        return $json;
    }

    /*
    Read XML structure to associative array
    --
    Using:
    $xml = new XMLReader();
    $xml->open([XML file]);
    $assoc = xml2assoc($xml);
    $xml->close();
*/
    public static function xml2assoc($xml) {//vvduyhhhhhhhhhri tjk mcfvbvou
        $assoc = null;
        while($xml->read()){
            switch ($xml->nodeType) {
                case XMLReader::END_ELEMENT: return $assoc;
                case XMLReader::ELEMENT:
                    $assoc[$xml->name][] = array('value' => $xml->isEmptyElement ? '' : self::xml2assoc($xml));
                    if($xml->hasAttributes){
                        $el =& $assoc[$xml->name][count($assoc[$xml->name]) - 1];
                        while($xml->moveToNextAttribute()) $el['attributes'][$xml->name] = $xml->value;
                    }
                    break;
                case XMLReader::TEXT:
                case XMLReader::CDATA: $assoc .= $xml->value;
            }
        }
        return $assoc;
    }
}

