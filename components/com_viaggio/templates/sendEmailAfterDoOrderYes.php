<?php

$send = true;//false

$subject = 'Nuova richiesta di prenotazione.';

$body = '';
include 'top.php';

$body .= '
<b>Grazie per aver inviato una richiesta di prenotazione da www.viaggio-russia.com</b> <br>

<b>A seguire, i vostri dati personali ed i dettagli della vostra pratica:</b><br>
Numero ordine  '.$item->id.'<br/>
Tour: '.$item->tour->name.' <br>
Partenza:  '.$item->tour->from.' <br>
Ritorno:  '.$item->tour->to.' <br>
Nome e cognome: '.$item->nomecgome.' <br/>
Telefono: '.$item->telephone.' <br/>
Importo in euro:   '.$item->valute_amount.' € <br/>
Importo in rubli (per pagamenti con carta di credito):  '.($item->amount/100).' rub <br/><br/>
<b>Potete procedere al pagamento dell’ordine entro una giornata lavorativa, cliccando sul tasto <a href="https://www.viaggio-russia.com/profile?layout=pay&order='.$item->id.'"> PAGARE </a>.<br></b>
<br/>Per qualsiasi chiarimento o delucidazione, è possibile contattarci all’indirizzo viaggio@russiantour.com <br><br>


Cordiali saluti,<br>
Lo Staff di Viaggio-Russia<br>

<span style=" color: #fff; "> sendEmailAfterDoOrderYes.php </span>
';

include 'bottom.php';

/*
stdClass Object
(
    [id] => 1499029716
    [subid] => 0
    [order_id] => 
    [asset_id] => 0
    [created_by] => 744
    [currency] => 
    [orderstatus] => created
    [errorcode] => 0
    [errormessage] => 
    [amount] => 10243091
    [valute_amount] => 1481
    [valute_rate] => 67.8072
    [tour_id] => 41
    [clientcount] => 1
    [insurance] => on
    [stell] => 3
    [nomecgome] => ery erhre
    [telephone] => 79050077888
    [email] => timach-ufa@ya.ru
    [password] => 8491424
)
*/

?>