<?php
if (!isset($ooo))
    include '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
?>

<?php

$send = true;//false

$subject = "Pagamento pratica n. ".$payment->order->id;

$body = '';
include 'it-IT_top.php';

$body .= '
Gentile '.$payment->fio.',
<br>
Può procedere al pagamento di '.$payment->amountEuro.' €, relativamente ai servizi previsti dal contratto n. '.$payment->order->id.', visionato ed accettato online, con bonifico bancario alle seguenti coordinate:
<br><br>'.$bank.''.$payment->order->id.' '.$payment->clients[0]->cognome.' '.$payment->clients[0]->nome.'<br><br>
Istruzioni per il pagamento: si tratta di un bonifico internazionale, non è un bonifico SEPA, quindi non c’è un codice IBAN ma un numero di conto corrente russo. In caso di richieste durante il pagamento sul vostro online banking selezionate “Pagamento internazionale” e come nazione ricevente “Russia”. Le commissioni devono essere condivise: selezionare “SHA”. Una volta effettuato il pagamento vi preghiamo di notificarcelo via mail, allegando una copia della transazione. La mancata notifica del pagamento potrebbe comportare ritardi nell’avvio della pratica o problemi nella conferma della prenotazione.
<br>


Cordiali saluti.
<span style=" color: #fff; ">it-IT_sendEmailBonifico.php</span>
';
include 'it-IT_bottom.php';

/*

 il suo pagamento di euro '.$item->valute_amount.' €  '.($item->amount/100).' rubli per il tour '.$item->tour->name.'<br>
Abbiamo ricevuto il suo pagamento di '.($item->amount/100).' rubli per il tour '.$item->tour->name.'<br>
stdClass Object
 euro '.$item->valute_amount.' € (
(
    [id] => 1499031339
    [subid] => 1
    [order_id] => b4254739-8dc2-47cd-b93b-257449385e9f
    [asset_id] => 0
    [created_by] => 747
    [currency] => 643
    [orderstatus] => 2
    [errorcode] => 0
    [errormessage] => Успешно
    [amount] => 6155538
    [valute_amount] => 890
    [valute_rate] => 67.8072
    [tour_id] => 11
    [clientcount] => 1
    [insurance] => 0
    [stell] => 3
    [nomecgome] => Kryska Lariska
    [telephone] => 79050077888
    [email] => timach-ufa@ya.ru
)
*/

?>