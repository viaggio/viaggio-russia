<?php

require_once('/home/viaggio/web/viaggio-russia.com/public_html/tcpdf/config/tcpdf_config2.php');

require_once('/home/viaggio/web/viaggio-russia.com/public_html/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

include ('rti_it2.php');
include ('rti_ru2.php');
include ('translit.php');

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Viaggio Russia');
$pdf->SetTitle('Fattura ');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('arial', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



$name = 'pagare'.$item->id;

/*
stdClass Object
(
    [id] => 1496292383
    [subid] => 0
    [order_id] => 
    [asset_id] => 0
    [created_by] => 735
    [currency] => 
    [orderstatus] => created
    [errorcode] => 0
    [errormessage] => 
    [amount] => 7017662
    [valute_amount] => 1085
    [valute_rate] => 63.4107
    [tour_id] => 8
    [clientcount] => 1
    [insurance] => on
    [stell] => 3
    [telephone] => 89050077888
    [email] => timach-ufa@ya.ru
    [number] => 1496292383
	[nomecgome] =>
)
*/

$created_at = explode(' ',$item->created_at);
$created_at = explode('-',$created_at[0]);
$dru = '«'.$created_at[2].'»';
$dit = '"'.$created_at[2].'"';
$m = $created_at[1];
$y = $created_at[0];


function printtPdfPagareM_month($m,$lang)
{
    switch($m){
        case 1:
            $mru = 'января';
            $mit = 'gennaio';
            break;
        case 2:
            $mru = 'февраля';
            $mit = 'febbraio';
            break;
        case 3:
            $mru = 'марта';
            $mit = 'marzo';
            break;
        case 4:
            $mru = 'апреля';
            $mit = 'aprile';
            break;
        case 5:
            $mru = 'мая';
            $mit = 'maggio';
            break;
        case 6:
            $mru = 'июня';
            $mit = 'giugno';
            break;
        case 7:
            $mru = 'июля';
            $mit = 'luglio';
            break;
        case 8:
            $mru = 'августа';
            $mit = 'agosto';
            break;
        case 9:
            $mru = 'сентября';
            $mit = 'settembre';
            break;
        case 10:
            $mru = 'октября';
            $mit = 'ottobre';
            break;
        case 11:
            $mru = 'ноября';
            $mit = 'novembre';
            break;
        case 12:
            $mru = 'декабря';
            $mit = 'dicembre';
            break;
    }
    return ($lang=='ru')?$mru:$mit;
}

//перевод месяца
$mru = printtPdfPagareM_month($m,'ru');
$mit = printtPdfPagareM_month($m,'it');

$mpn = $_GET['paymentID'];
$mpd = $_GET['mp_d'];
$mpm = printtPdfPagareM_month($_GET['mp_m'],'ru');
$mpm_it = printtPdfPagareM_month($_GET['mp_m'],'it');
$mpy = $_GET['mp_y'];
$amountEur = $_GET['amountEur'];

$db = JFactory::getDbo();

$query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$mpn."'";
$db->setQuery($query);
$manualPayment = $db->loadObject();

$query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
$db->setQuery($query);
$manualPaymentDetails = $db->loadObjectList();

$details = '';
$airTickets = array();
$railTickets = array();
$isSetOther = false;
$isSetVisa = false;
foreach ($manualPaymentDetails as $manualPaymentDetail)
{
    if ($manualPaymentDetail->field_group_name == 'air-ticket')
        $airTickets[$manualPaymentDetail->field_group] = true;
    elseif ($manualPaymentDetail->field_group_name == 'rail-ticket')
        $railTickets[$manualPaymentDetail->field_group] = true;
    elseif ($manualPaymentDetail->field_group_name == 'other')
        $isSetOther = true;
    elseif ($manualPaymentDetail->field_group_name == 'visa')
        $isSetVisa = true;

    $details .= $manualPaymentDetail->field_group_name.' - '.$manualPaymentDetail->field_name.'['.$manualPaymentDetail->field_group.'] = '.$manualPaymentDetail->field_value.'<br/>';
}

$isSetDetails = 'Air tickets: '.count($airTickets).'; ';
$isSetDetails .= 'Rail tickets: '.count($railTickets).'; ';
$isSetDetails .= 'Other: '.($isSetOther?'Yes':'No').'; ';
$isSetDetails .= 'Visa: '.($isSetVisa?'Yes':'No').'.<br/><br/>';

// Set some content to print
$html = <<<EOD
<table border="1" style="padding: 3px;">
	<tr>
		<td>
			<b>Инвойс №  $mpn от « $mpd » $mpm $mpy г.</b><br>

 
по договору-оферте о реализации туристского продукта  <b>$item->id</b> от $dru $mru $y г. (далее – Договор)<br>
<b>Туроператор:</b> ООО Международная Компания Русский Тур<br>

<b>Заказчик:</b> $item->nomecgome<br>

В соответствии с Договором оплате подлежат следующие услуги<br>
 
<table border="1" style="padding: 5px;">
<tr>
<td  style="width: 10%;">
№ п/п
</td>
<td  style="width: 70%;">
Наименование услуги
</td>
<td  style="width: 20%;">
Цена, Евро
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Предоплата за турпакет на
  
EOD;

$html .= count($item->clients).' человека<br>';
$html.=$item->tour->name_rus.'<br>'; 
$html.=date('d/m/Y',strtotime($item->tour->from)).' - '; 
$html.=date('d/m/Y',strtotime($item->tour->to)).'<br>'; 
foreach ($item->clients as $k=>$v)
{
    $html.='
         '.translitText($v->nome).' '.translitText($v->cognome).'<br>';

}


$html .= <<<EOD


</td>
<td>
$amountEur
</td>
</tr>
<tr>
<td colspan="2">
ИТОГО к оплате
</td>
<td>
$amountEur

</td>
</tr>
</table><br>
Срок оплаты: в соответствии с условиями Договора.<br>
<b>Банковские реквизиты:</b><br>
$rti_ru
<br/>
В назначении платежа должен быть указан № инвойса и его дата.

<p><b>Настоящий инвойс признается Сторонами в качестве акта выполненных работ.</b></p>

<p>Генеральный директор ООО Международная Компания Русский Тур</p>

<p>О. Н. Черемшенко</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		</td>
		<td>
		
		
		<b>Fattura № " $mpn " del $mpd $mpm_it $mpy</b>
In base al contratto-offerta di realizzazione di un prodotto turistico № <b>$item->id</b> del $dit $mit $y (di seguito: Contratto). 
 
<br>
Tour operator:<b> Russian Tour International Ltd.</b><br>

<b>Committente:</b> $item->nomecgome<br>

Ai sensi del Contratto sono soggetti a pagamento i seguenti servizi<br>

 
<table border="1" style="padding: 5px;">
<tr >
<td style="width: 10%;">
№
</td>
<td style="width: 70%;">
Denominazione del servizio
</td>
<td style="width: 20%;">
Prezzo, Euro
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Acconto di un pacchetto turistico 
EOD;
$html .= count($item->clients).' Person <br>';
$html.=$item->tour->name.'<br>'; 
$html.=date('d/m/Y',strtotime($item->tour->from)).' - '; 
$html.=date('d/m/Y',strtotime($item->tour->to)).'<br>'; 

foreach ($item->clients as $k=>$v)
{
    $html.='
         '.$v->nome.' '.$v->cognome.'<br>';
}


$html .= <<<EOD

</td>
<td>
$amountEur
</td>
</tr>
<tr>
<td colspan="2">
TOTALE da pagare
</td>
<td>
$amountEur
</td>
</tr>
</table>

Termine di pagamento: in ottemperanza alle condizioni del Contratto.<br>

<b>Coordinate bancarie:</b><br>

$rti_it
<p>Nella causale del pagamento deve essere indicato il № della fattura e la data.</p>

<p><b>
La presente fattura è riconosciuta dalle Parti come atto di esecuzione dei lavori.</b></p>

<p>Il Direttore Generale di Russian Tour International Ltd.</p>

<p>O. N. Cheremshenko</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		
		
		
		</td>
	</tr>
</table>
EOD;

$pdf->writeHTML($html, true, false, true, false, '');

?>