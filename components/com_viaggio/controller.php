<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class ViaggioController
 *
 * @since  1.6
 */
class ViaggioController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
	    if (isset($_GET['OrderId']))
        {
            return $this->thanks();
        }
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'tours');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	
	public function test()
	{
		//echo '<br/><br/><br/>';
		//$this->setRedirect('index.php?option=com_viaggio&view=profile', false);
		include('helpers/settings.php');
		$item = new stdClass();
		$item->number = 83467457547547;
		//ViaggioHelpersFrontend::sendEmail('test', 'test', 'timach-ufa@ya.ru',$settings, $item);
		//ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailAfterDoOrder','timach-ufa@ya.ru',$settings, $item);
		//ViaggioHelpersFrontend::sendSmsByTemplate('sendSmsAfterDoOrder', '+79050077888', $settings, $item); 
		
		var_dump(ViaggioHelpersFrontend::getValuteRate());
	}
	
	public function editclient()
	{
		include('helpers/settings.php');
		$app = JFactory::getApplication();
		$item = new stdClass();
		$item->error = 0;
		//получение паролей
		$item->client_id = $app->input->getInt('client_id', 0);$item->delete = 0;
		if(isset($_REQUEST['delete'])) $item->delete = 1;
		$item->cognome = $app->input->getString('cognome', '');
		$item->nome = $app->input->getString('nome', '');
		$item->sex = $app->input->getString('sex', '');
		$item->nascita = $app->input->getString('nascita', '');
		
		//приводим дату к sql формату
		$item->nascita = ViaggioHelpersFrontend::dateToSQLformat($item->nascita);
		
		$item->user   = JFactory::getUser();
		if(!$item->user->guest)
		{
			$item->user_id = $item->user->id;
			$item->email = $item->user->email;
		}else{
			$item->error = 1; //не залогинен
		}
		
		//!проверка статуса заказа, если больше -1, то ошибка
		
		$query = "UPDATE `#__viaggio_clients` SET `cognome` = '$item->cognome', `nome` = '$item->nome', `sex` = '$item->sex', `birthday` = '$item->nascita' WHERE `id` = $item->client_id AND `created_by` = $item->user_id";
		
		if($item->delete) $query = "DELETE FROM `#__viaggio_clients` WHERE `id` = $item->client_id AND `created_by` = $item->user_id";
		
		if($item->error OR $item->delete)
		{
			$db = JFactory::getDbo();
			$db->setQuery($query);
			$item->result = $db->execute();
		}

		if($item->delete){
			$this->setRedirect('index.php?option=com_viaggio&view=profile', JText::_('COM_VIAGGIO_CLIENT_DELETED_'.$item->error) );
		}else{
			$this->setRedirect('index.php?option=com_viaggio&view=profile', JText::_('COM_VIAGGIO_CLIENT_EDITED_'.$item->error) );
		}
	}
	
	public function changepwd()
	{
		include('helpers/settings.php');
		$app = JFactory::getApplication();
		$item = new stdClass();
		$item->error = 0;
		//получение паролей
		$item->pw1 = $app->input->getString('pw1', '');
		$item->pw2 = $app->input->getString('pw2', '');
		
		//if logged
		$item->user   = JFactory::getUser();
		if(!$item->user->guest)
		{
			$item->user_id = $item->user->id;
			$item->email = $item->user->email;
			if($item->pw1 != $item->pw2) $item->error = 2; //пароли не совпадают
			if(strlen($item->pw1) < 3) $item->error = 3; //короткий пароль
		}else{
			$item->error = 1; //не залогинен
		}
		
		if(!$item->error)
		{
			$result = ViaggioHelpersFrontend::changePwd($item->user->id,$item->pw1);
			//echo '<pre>'.print_r($result,true).'</pre>';
			if(!$result) $item->error = 4; //ошибка при сохранении в БД
			
			//отправка письма о смене пароля
			ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailPasswordChanged',$item->email,$settings, $item);
		}
		
		//echo '<pre>'.print_r($item->email,true).'</pre>';	
		
		$this->setRedirect('index.php?option=com_viaggio&view=profile', JText::_('COM_VIAGGIO_PASSWORD_CHANGED_'.$item->error) );
	}
	
	public function print_pagare()
	{
		include('helpers/settings.php');
		$item = new stdClass();
		
		$app = JFactory::getApplication(); 
		$item->number = $app->input->getInt('number', 0);
		
		$item = ViaggioHelpersFrontend::getOrderObject( $item->number );
		
		$item->user   = JFactory::getUser();
		
		//проверяем доступ
		if($item->created_by != $item->user->id)
		{
			$this->setRedirect('/', JText::_('COM_VIAGGIO_NO_ACCESS') );
		}else{
			
			if(isset($item->id)) $item->number = $item->id;
			
			//echo '<pre>'.print_r($item,1).'</pre>'; die();
			
			ViaggioHelpersFrontend::printPdf('printPdfPagare', $item);
			
			ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailPagare',$item->email,$settings, $item, JPATH_COMPONENT.'/pdf/'.'pagare'.$item->id.'.pdf');

			
			//обновляем статус заказа
			ViaggioHelpersFrontend::changeOrderStatus($item->id, 8); //8 хотят оплатить на расчетный счет
			
			die();
		}
	}
	
		public function print_pagare_card()
	{
		include('helpers/settings.php');
		$item = new stdClass();
		
		$app = JFactory::getApplication(); 
		$item->number = $app->input->getInt('number', 0);
		
		$item = ViaggioHelpersFrontend::getOrderObject( $item->number );
		
		$item->user   = JFactory::getUser();
		
		//проверяем доступ
		if($item->created_by != $item->user->id)
		{
			$this->setRedirect('/', JText::_('COM_VIAGGIO_NO_ACCESS') );
		}else{
			
			if(isset($item->id)) $item->number = $item->id;
			
			//echo '<pre>'.print_r($item,1).'</pre>'; die();
			
			//ViaggioHelpersFrontend::printPdf('printPdfPagareCard', $item);
			
			ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailPagareCart',$item->email,$settings, $item/*, JPATH_COMPONENT.'/pdf/'.'pagarecard'.$item->id.'.pdf'*/);

			
			//обновляем статус заказа
			ViaggioHelpersFrontend::changeOrderStatus($item->id, 8); //8 хотят оплатить на расчетный счет
			
			die();
		}
	}
	private function getDates($startTime, $endTime) {
		
		$days = array($startTime,$endTime);
		
		$day = 86400;
		$format = 'Y-m-d';
		$startTime = strtotime($startTime);
		$endTime = strtotime($endTime);
		//$numDays = round(($endTime - $startTime) / $day) + 1;
		$numDays = round(($endTime - $startTime) / $day); // без +1

		for ($i = 1; $i < $numDays; $i++) { 
			$days[] = date($format, ($startTime + ($i * $day)));
		}

		return $days;
	}
	
	public function get_tours_dates_by_category()
	{
		$app = JFactory::getApplication(); 
		$category_id = $app->input->getInt('category_id', 0);
		$data = new StdClass();
		$data->dates = array();
		$data->datestotour = array();
		$data->min = 0;
		$data->max = 0;
		
		if($category_id){
			$db = JFactory::getDbo();
			
			$queryFrom = "SELECT `from` FROM `#__viaggio_tours` WHERE category = '$category_id' AND state = '1' ORDER BY `from` ASC LIMIT 1";
			$db->setQuery($queryFrom);
			$data->min = $db->loadResult();
			
			$queryTo = "SELECT `to` FROM `#__viaggio_tours` WHERE category = '$category_id' AND state = '1' ORDER BY `to` DESC LIMIT 1";
			$db->setQuery($queryTo);
			$data->max = $db->loadResult();
			
			$query = "SELECT * FROM `#__viaggio_tours` WHERE category = '$category_id' AND state = '1'";

			$db->setQuery($query);
			$tours = $db->loadObjectList();
			foreach($tours as $tour){
				$dates = $this->getDates($tour->from, $tour->to);
				
				//get itemid
				$tour->Itemid = '';
				$search = '{"cat_id":"'.$tour->category.'"%';
				$query = "SELECT id FROM `#__menu` WHERE link = 'index.php?option=com_viaggio&view=tours' AND params LIKE '$search'";
				$db->setQuery($query);
				$result = $db->loadResult();
				if($result) $tour->Itemid = '&Itemid='.$result;
				
				//set dates per days
				foreach($dates as $date)
				{
					//echo '<pre>'.date('j.n.Y',strtotime($date)).'</pre>';
					$data->dates[] = date('j.n.Y',strtotime($date));
					$data->datestotour[] = $tour->id;
					$data->datestolinks[] = JRoute::_( 'index.php?option=com_viaggio&view=showtour&withoutcat=1&id='.$tour->id.$tour->Itemid );
				}
			}
		}
		echo json_encode($data);
		die();
	}
	
	public function calcTourCost()
	{
		//1000Array ( [option] => com_viaggio [task] => calcTourCost [stellcount] => 4 [tour_id] => 2 [] => 0count=2 [bithday2] => 2010-03-29 [bithday1] => 1990-03-31 [Itemid] => 275 [id] => [view] => showtour )
		$app = JFactory::getApplication();
		$subtype = 0;
		$tour_id = $app->input->getString('tour_id', 0);
		$stellcount = $app->input->getString('stellcount', '3');
		$count = $app->input->getString('count', 0);
		$insurance = $app->input->getString('insurance', 0);
		$needvisamongolia = $app->input->getString('needvisamongolia', 0);
		$needvisachina = $app->input->getString('needvisachina', 0);
		$birthday1 = $app->input->getString('birthday1', 0);
		$birthday2 = $app->input->getString('birthday2', 0);
		$transib = $app->input->getString('transib', '');
		
		if($stellcount) $subtype = $stellcount;
		
		if($transib) $subtype = $transib;
		//$subtype = $stellcount;
		
		$age1 = 18;
		$age2 = 18;
		if($birthday1) $age1 = ViaggioHelpersFrontend::calculate_age($birthday1);
		if($birthday2) $age2 = ViaggioHelpersFrontend::calculate_age($birthday2);
		//getCost($tour_id,$subtype=0,$count=1,$age1=18,$age2=0,$halfboard = 0, $needvisamongolia = 0, $needvisachina = 0)
		echo ViaggioHelpersFrontend::getCost( $tour_id, $subtype, $count, $age1, $age2,$insurance, $needvisamongolia, $needvisachina);
		//print_r($_REQUEST);
		die();
	}
	
	public function sendorder()
	{
		include('helpers/settings.php');
		//var_dump($settings);
		$db = JFactory::getDbo();
		$item = new stdClass();
		$passwordToEmail = '';
		$item->password = '';
		$app = JFactory::getApplication();
		$item->subtype = 0;
		$ajax = $app->input->getString('ajax', 0);
		$item->tour_id = $app->input->getString('tourid', 0);
		$item->stellcount = $app->input->getString('stellcount', '3');
		$item->clientscount = $app->input->getString('count', 0);
		$item->insurance = $app->input->getString('insurance', 0);
		$item->needvisamongolia = $app->input->getString('needvisamongolia', 0);
		$item->needvisachina = $app->input->getString('needvisachina', 0);
		$item->ajax = $app->input->getString('ajax', 0);
		$item->birthday1 = $app->input->getString('birthday1', 0);
		$item->birthday2 = $app->input->getString('birthday2', 0);
		$item->transib = $app->input->getString('transib', '');
		$item->email = $app->input->getString('email');
		
		$item->clients = array();
		
		$params = $app->getParams();
		$item->managersemails = $params->get('managersemails');
		
		//print_r($_REQUEST);
		
		//get clients count from request
		if(isset($_REQUEST['nome']))
		{
			if(is_array($_REQUEST['nome'])) $item->clientscount = count($_REQUEST['nome']);
		}
		
		//echo '<pre>'.print_r($_REQUEST['nascita'][0],1).'</pre>';
		
		if( isset($_REQUEST['nascita']) && /*isset($_REQUEST['nazionalita']) &&*/ isset($_REQUEST['sex']) && isset($_REQUEST['nome']) && isset($_REQUEST['cognome']) )
		{
			if($item->clientscount == 1)
			{
				$item->clients[0]['nascita'] = $_REQUEST['nascita'][0];
				$item->clients[0]['nazionalita'] = '';//$_REQUEST['nazionalita'][0];
				$item->clients[0]['sex'] = $_REQUEST['sex'][0];
				$item->clients[0]['nome'] = $_REQUEST['nome'][0];
				$item->clients[0]['cognome'] = $_REQUEST['cognome'][0];
				$item->clients[0]['numero_di_passaporto'] = $_REQUEST['passport'][0];
			}else if ($item->clientscount == 2){
				$item->clients[0]['nascita'] = $_REQUEST['nascita'][0];
				$item->clients[1]['nascita'] = $_REQUEST['nascita'][1];
				
				$item->clients[0]['nazionalita'] = '';//$_REQUEST['nazionalita'][0];
				$item->clients[1]['nazionalita'] = '';//$_REQUEST['nazionalita'][1];
				
				$item->clients[0]['sex'] = $_REQUEST['sex'][0];
				$item->clients[1]['sex'] = $_REQUEST['sex'][1];
				
				$item->clients[0]['nome'] = $_REQUEST['nome'][0];
				$item->clients[1]['nome'] = $_REQUEST['nome'][1];
				
				$item->clients[0]['cognome'] = $_REQUEST['cognome'][0];
				$item->clients[1]['cognome'] = $_REQUEST['cognome'][1];

				$item->clients[0]['numero_di_passaporto'] = $_REQUEST['passport'][0];
				$item->clients[1]['numero_di_passaporto'] = $_REQUEST['passport'][1];
			}
		}
		
		if($item->stellcount) $item->subtype = $item->stellcount;
		
		if($item->transib) $item->subtype = $item->transib;

		$age1 = 18;
		$age2 = 18;
		if($item->birthday1) $age1 = ViaggioHelpersFrontend::calculate_age($item->birthday1);
		if($item->birthday2) $age2 = ViaggioHelpersFrontend::calculate_age($item->birthday2);
		
		//get cost in valute
		$item->valute_amount = ViaggioHelpersFrontend::getCost( $item->tour_id, $item->subtype, $item->clientscount, $age1, $age2,$item->insurance, $item->needvisamongolia, $item->needvisachina, 0);
		
		//get or register user
		$item->user_id = 0;
		$item->telephone = '';
		
		$item->telephone = $app->input->getString('telefone', '');
		$item->nomecgome = $app->input->getString('nomecgome', '');
		
		//€  61,6987 руб (курс )   1.02(наценка за банк)  915 (стоимость тура)  * 100 копеек
		$item->valute_rate = ViaggioHelpersFrontend::getValuteRate();
		
		//get cost in rub/100
		$item->cost = round( $item->valute_rate * 1.00 * $item->valute_amount * 100 );
		
		//get cost in rub
		$item->costx100 = $item->cost/100;
		
		//get formula
		$item->formula = ViaggioHelpersFrontend::getCost( $item->tour_id, $item->subtype, $item->clientscount, $age1, $age2,$item->insurance, $item->needvisamongolia, $item->needvisachina, 1);
		
		//set order number like timestamp
		$item->orderNumber = time();
		
		//отправка письма менеджерам и остановка выполнения скрипта заказа
		if(1==2&& $item->managersemails)
		{
			//уведомление о заказе по email
			
		//print_r($item);
		ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailToManagers',$item->managersemails,$settings, $item);
		
		//ставим куки, что кнопка нажата
		setcookie('ViaggioTour'.$item->tour_id.'BayPushed', 1, time()+60*60*24*365);
		
			//перенаправление или ответ 1 если аякс
			if($item->ajax)
			{
				echo '1'; die();
			}else{
				$this->setRedirect($settings->fastOrderPage, $settings->order->redirectMsg);
			}
		}else{
			//if logged
			$item->user   = JFactory::getUser();
			if(!$item->user->guest)
			{
				$item->user_id = $item->user->id;
				$item->email = $item->user->email;
				$guest = false;
			}else{
			    $guest = true;
				//print_r($_REQUEST);
				$item->email = $app->input->getString('email');
				if(preg_match('/([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z\.]{2,6})/', $item->email)){
					if(!ViaggioHelpersFrontend::getUserId($item->email))
					{
						$item->password = rand(5, 15555555);
						$item->password = substr( md5($item->password) , 0, 6);
                        $passwordToEmail = $item->password;
						//echo $password;
						$item->user = ViaggioHelpersFrontend::registerUser($item->email,$item->password,$item->nomecgome,$item->telephone);
						$item->user_id = $item->user->id;
						//print_r($user_id);
					}else{
						$item->user_id = ViaggioHelpersFrontend::getUserId($item->email);
						//var_dump($user_id); die();
					}	
				}else{
					$item->email = '';
				}
			}
			
			//создание заказа
			$doOrderQuery = "INSERT INTO `#__viaggio_orders` (`id`, `asset_id`, `created_by`, `currency`, `orderstatus`, `errorcode`, `errormessage`, `amount`, `valute_amount`, `valute_rate`, `tour_id`, `clientcount`, `insurance`, `stell`, `nomecgome`, `telephone`, `email`) VALUES ($item->orderNumber, '0', '$item->user_id', '', '-1', '0', '', '$item->cost', '$item->valute_amount', '$item->valute_rate', '$item->tour_id', '$item->clientscount', '$item->insurance', '$item->stellcount', '$item->nomecgome', '$item->telephone', '$item->email');";
			var_dump($doOrderQuery);
			
			
			//if not test set to true
			if(true){	
				$db->setQuery($doOrderQuery);
				$doOrderResult = $db->execute();
				//$item->orderNumber = $db->insertid();

				//create clients
				foreach ($item->clients as $clientArray)
				{
					$createClientsResult = ViaggioHelpersFrontend::createClient($clientArray, $item->orderNumber, $item->user_id, $item->tour_id);
				}
				
				//формируем письмо для менеджеров. начало
				$emailBodyToManagers = "<table>
				<tr><td>Order id: </td><td><a href='https://www.viaggio-russia.com/administrator/index.php?option=com_viaggio&view=clients&filter_search=$item->orderNumber' >$item->orderNumber</a></td></tr>
				<tr><td>Status: </td><td>created</td></tr>
				<tr><td>Username(id): </td><td><a href='https://www.viaggio-russia.com/administrator/index.php?option=com_users&task=user.edit&id=$item->user_id'>".$item->user->name."($item->user_id)</a></td></tr>
				<tr><td>Cost: </td><td>".($item->cost/100)." rub ( $item->valute_amount eur, $item->valute_rate rub/eur )</td></tr>
				<tr><td>Formula: </td><td>".$item->formula."</td></tr>
				<tr><td>Tour Id: </td><td><a href='".$_SERVER['HTTP_REFERER']."'>".$item->tour_id."</a></td></tr>";

				if($item->transib) $emailBodyToManagers .= "<tr><td>Transib: </td><td>".$item->transib."</td></tr>";
				
				if($item->insurance) $emailBodyToManagers .= "<tr><td>Insurance: </td><td>Yes</td></tr>";
				
				if($item->stellcount) $emailBodyToManagers .= "<tr><td>Stell: </td><td>".$item->stellcount."</td></tr>";
				
				if($item->needvisamongolia) $emailBodyToManagers .= "<tr><td>Visa Mongolia: </td><td>Yes</td></tr>";
				
				if($item->needvisachina) $emailBodyToManagers .= "<tr><td>Visa China: </td><td>Yes</td></tr>";

				$emailBodyToManagers .= "
				<tr><td>Nome e cognome: </td><td>$item->nomecgome</td></tr>
				<tr><td>Email: </td><td>$item->email</td></tr>
				<tr><td>Telephone: </td><td>$item->telephone</td></tr>
				";
				
				$emailBodyToManagers .= "<tr><td>Clients count: </td><td>$item->clientscount</td></tr>";
				
				foreach ($item->clients as $clientArray){
					$emailBodyToManagers .= "<tr><td>Client: </td>
					<td>
						<table>
							<tr>
								<td>nome:</td>
								<td>".$clientArray['nome']."</td>
							</tr>
							<tr>
								<td>cognome:</td>
								<td>".$clientArray['cognome']."</td>
							</tr>
							<tr>
								<td>sex:</td>
								<td>".$clientArray['sex']."</td>
							</tr>
							<tr>
								<td>nascita:</td>
								<td>".$clientArray['nascita']."</td>
							</tr>
							<tr>
								<td>nazionalita:</td>
								<td>".$clientArray['nazionalita']."</td>
							</tr>
						</table>
					</td></tr>";
				}
				
				echo $emailBodyToManagers;
				//формируем письмо для менеджеров. конец
				
				//уведомление менеджеров
				//ViaggioHelpersFrontend::sendEmail($emailBodyToManagers, JText::sprintf( 'COM_VIAGGIO_EMAIL_NOTIFY_AFTER_DO_ORDER_TITLE', $item->orderNumber ), $settings->email->managers,$settings);
				
				//получаем данные заказа из БД
				$item = ViaggioHelpersFrontend::getOrderObject( $item->orderNumber );
				//сохраняем пароль, если новый пользователь
				$item->password = $passwordToEmail;
				
				//уведомление о заказе по email
                if ($guest)
				    ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailAfterDoOrder',$item->email,$settings, $item);
                else
                    ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailAfterDoOrderYes',$item->email,$settings, $item);
				
				//уведомление о заказе по смс
				ViaggioHelpersFrontend::sendSmsByTemplate('sendSmsAfterDoOrder', $item->telephone, $settings, $item);
			}
			
			echo '<pre>'.print_r($item,1).'</pre>';

			if(1==1 || !$ajax){
				//редирект на страницу спасибо за заявку, если не ajax
				$this->setRedirect($settings->order->redirectUrl, $settings->order->redirectMsg);
			}else{
				die();
			}
		}
	}

	public function prepareBuyManual()
    {
        $app = JFactory::getApplication();
        $payment_id = $app->input->getString('payment_id', 0);
        if ($payment_id != '0')
        {
            $db = JFactory::getDbo();
            $query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$payment_id."'";

            $db->setQuery($query);
            $manualPayment = $db->loadObject();

            setcookie('ViaggiobuyManualTourId', $manualPayment->tour_id, time()+60*60*24*365);
            setcookie('ViaggiobuyManualPaymentID', $manualPayment->paymentID, time()+60*60*24*365);

            $query2 = 'SELECT cats.path as path, cats.alias as catalias, t.alias as touralias, cats.id as cat_id FROM `#__categories` AS cats JOIN `#__viaggio_tours` as t ON t.category = cats.id  where t.id = "'.$db->escape($manualPayment->tour_id).'"';

            $db->setQuery($query2);
            $result= $db->loadObject();

            $query3 = "SELECT `path` FROM `#__menu` WHERE component_id = 10074 and params like '%cat_id\":\"".$result->cat_id."\"%'";

            $db->setQuery($query3);
            $menu = $db->loadObject();

            $this->setRedirect('/'.$menu->path.'/'.$result->touralias, false);
        }
    }

	public function buyManual()
    {
        //получаем настройки
        include('helpers/settings.php');
        //var_dump($settings);

        //получаем объекты джумла
        $db = JFactory::getDbo();
        $app = JFactory::getApplication();

        //создаем пустые объекты
        $item = new stdClass();
        $manualPayment = new stdClass();

        //получае данные request
        $manualPayment->paymentID = $app->input->getString('payment_id', 0);

        //если не авторизован
        $user   = JFactory::getUser();
        if($user->guest) $this->setRedirect('index.php?option=users&view=login', false);

        //если в запросе не определен номер заказа
        $return_url = $_SERVER['HTTP_REFERER'];
        if( !$manualPayment->paymentID ) $this->setRedirect($return_url, 'No tour id');

        //получаем данные о заказе
        $query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$manualPayment->paymentID."'";

        $db->setQuery($query);
        $manualPayment = $db->loadObject();

        //если заказ не найден
        if( !isset($manualPayment->id) ) echo $return_url; $this->setRedirect($return_url, 'Wrong tour id!');

        /*if ($settings->paymentApi->best2payIsTest)
            $manualPayment->amountRub = intval($manualPayment->amountRub/100)*100;

        //регистрируем заказ в ПС
        $answer = ViaggioHelpersFrontend::paymentApiRegisterOrder($manualPayment->paymentID, $manualPayment->amountRub, $settings);

        $manualPayment->outOrderID = $answer;

        JFactory::getDbo()->updateObject('#__viaggio_manualpayments', $manualPayment, 'id');

        //редирект на ПС
        $formUrl = $settings->paymentApi->best2payUrl.'Purchase?sector='.$settings->paymentApi->best2paySector.'&id='.$answer.'&signature='.base64_encode(md5($settings->paymentApi->best2paySector.$answer.$settings->paymentApi->best2paySignaturePassword));*/

        $formUrl = ViaggioHelpersFrontend::tinkoffInitOrder($manualPayment->paymentID, $manualPayment->amountRub, $settings, $manualPayment->email);
        $this->setRedirect($formUrl, false);
    }

    public function tinkoffcallback()
    {
        include('helpers/settings.php');
        //$post = $_POST;
        $data = file_get_contents("php://input");
        //LOG!!!
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' First: '.var_export($data,1), FILE_APPEND);
        ////////
        $data = json_decode($data);
        $post = (array) $data;
        //LOG!!!
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' Array: '.var_export($post,1), FILE_APPEND);
        ////////
        $token = $post['Token'];
        unset($post['Token']);
        $post['Password'] = $settings->tinkoffPassword;
        $post['Success'] = ($post['Success'])?'true':'false';
        ksort($post);
        //LOG!!!
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' Second: '.var_export(implode('',$post),1), FILE_APPEND);
        ////////
        //LOG!!!
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' Third: '.var_export(hash('sha256',implode('',$post)),1), FILE_APPEND);
        ////////
        if (hash('sha256',implode('',$post)) == $token)
        {
            $paymentID = $post['OrderId'];
            $db = JFactory::getDbo();
            if ($post['Status'] == 'CONFIRMED')
            {
                if (strpos($paymentID,'m')!==false)
                    $query = "UPDATE `#__viaggio_manualpayments` SET status = 1 WHERE paymentID = '".$paymentID."'";
                else
                    $query = "UPDATE `#__viaggio_orders` SET orderstatus = 2 WHERE id = '".$paymentID."'";
                //LOG!!!
                file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' Query: '.$query, FILE_APPEND);
                ////////
                $db->setQuery($query);
                $db->execute();
            }
            //LOG!!!
            file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' OK ', FILE_APPEND);
            ////////
            echo 'OK';
            //ViaggioHelpersFrontend::makeCheck($paymentID,$user->email,$visaAndInviteUserProfile->profile['phone'],$resp->Amount/100);
        }
        else
        {
            //LOG!!!
            file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' FAIL ', FILE_APPEND);
            echo 'FAIL';
        }
        exit;
    }

    public function best2paycallback()
    {
        include('helpers/settings.php');
        $xml = new XMLReader();
        $xml->open('php://input');
        $assoc = ViaggioHelpersFrontend::xml2assoc($xml);
        $xml->close();
        $txt = var_export($assoc,1);
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' First: '.$txt, FILE_APPEND);

        $str = '';
        foreach ($assoc['operation'][0]['value'] as $key=>$row)
        {
            if ($key != 'signature')
                $str .= $row[0]['value'];
        }

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' $str: '.var_export($str,1), FILE_APPEND);

        $pass = $settings->paymentApi->best2paySignaturePassword;
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' $pass: '.var_export($pass,1), FILE_APPEND);

        $signature = $assoc['operation'][0]['value']['signature'][0]['value'];
        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' $signature: '.var_export($signature,1), FILE_APPEND);

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' calculated: '.base64_encode(md5($str.$pass)), FILE_APPEND);
        $txt = '----------'.var_export((base64_encode(md5($str.$pass)) == $signature),1);

        if (base64_encode(md5($str.$pass)) == $signature)
        {
            $paymentID = $assoc['operation'][0]['value']['reference'][0]['value'];
            file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' $paymentID: '.var_export($paymentID,1), FILE_APPEND);

            $state = $assoc['operation'][0]['value']['state'][0]['value'];
            file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' $state: '.var_export($state,1), FILE_APPEND);

            if ($state == 'APPROVED')
            {
                $db = JFactory::getDbo();
                $query = "UPDATE `#__viaggio_manualpayments` SET status = 1 ".
                    " WHERE paymentID = '".$paymentID."'";
                $txt .= '+++++++++'.$query;
                $db->setQuery($query);
                $db->execute();
            }
            echo 'ok';
        }
        else
            echo 'FAIL';

        file_put_contents('/home/viaggio/web/viaggio-russia.com/public_html/test3.txt',date('Y-m-d H:i:s').' Second: '.$txt,FILE_APPEND);

        exit;
    }

	public function buy(){
		//получаем настройки
		include('helpers/settings.php');
		//var_dump($settings);
		
		//получаем объекты джумла
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		
		//создаем пустые объекты
		$item = new stdClass();
		$order = new stdClass();
		
		//получае данные request
		$order->id = $app->input->getString('order_id', 0);

		//если не авторизован
		$user   = JFactory::getUser();
		if($user->guest) $this->setRedirect('index.php?option=users&view=login', false);
		
		//если в запросе не определен номер заказа
		$return_url = $_SERVER['HTTP_REFERER'];
		if( !$order ) $this->setRedirect($return_url, 'No tour id');
		
		//получаем данные о заказе
		$query = "SELECT * FROM `#__viaggio_orders` WHERE id = $order->id";

		$db->setQuery($query);
		$order = $db->loadObject();
		
		$order->subId++;
		
		//update order data
		ViaggioHelpersFrontend::saveOrderArray($order);
		
		//если заказ не найден
		if( !isset($order->id) ) echo $return_url; $this->setRedirect($return_url, 'Wrong tour id!');

        $formUrl = ViaggioHelpersFrontend::tinkoffInitOrder($order->id, $order->amount, $settings, $order->email);
        $this->setRedirect($formUrl, false);
		/*
		//регистрируем заказ в ПС
		$answer = ViaggioHelpersFrontend::paymentApiRegisterOrder($order->id, $order->amount, $settings);
		
		//update order data
        $order->order_id = $answer;
		ViaggioHelpersFrontend::saveOrderArray($order);

		//редирект на ПС
        $formUrl = 'https://test.best2pay.net/webapi/Authorize?sector='.$settings->paymentApi->best2paySector.'&id='.$answer.'&signature='.base64_encode(md5($settings->paymentApi->best2paySector.$answer.$settings->paymentApi->best2paySignaturePassword));

		$this->setRedirect($formUrl, false);*/
	}

	public function inviare_i_dati(){
		//получаем настройки
		include('helpers/settings.php');
		//var_dump($settings);
		
		//получаем объекты джумла
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
	if(isset($_REQUEST['client_id']))
	{
		if(is_array($_REQUEST['client_id'])){
			foreach($_REQUEST['client_id'] as $key=>$value)
			{
				//подготовка объекта к сохранению в бд
				$object = new stdClass();
				$object->created_by = $user->id;
				
				//сохранение post данных в объект для сохранения
				echo "client_id[$key]";
				$object->id = $app->input->getInt('client_id', 0);
				$object->id = $object->id[$key];
				
				//$object->cognome = $app->input->getString('cognome', '');
				//$object->cognome = $object->cognome[$key];
				
				//$object->nome = $app->input->getString('nome', '');
				//$object->nome = $object->nome[$key];
				
				$object->luogo_di_nascita = $app->input->getString("luogo_di_nascita", '');
				$object->luogo_di_nascita = $object->luogo_di_nascita[$key];
				
				//$object->serie_passaporto = $app->input->getString("serie_passaporto", '');
				//$object->serie_passaporto = $object->serie_passaporto[$key];
				
				$object->numero_di_passaporto = $app->input->getString("numero_di_passaporto", '');
				$object->numero_di_passaporto = $object->numero_di_passaporto[$key];
				
				$object->date_from = $app->input->getString("date_from", '');
				$object->date_from = $object->date_from[$key];
				
				$object->date_to = $app->input->getString("date_to", '');
				$object->date_to = $object->date_to[$key];
				
				$object->numero_di_visite = $app->input->getInt("numero_di_visite", 0);
				$object->numero_di_visite = $object->numero_di_visite[$key];
				
				$object->last_visit_from = $app->input->getString("last_visit_from", '');
				$object->last_visit_from = $object->last_visit_from[$key];
				
				$object->last_visit_to = $app->input->getString("last_visit_to", '');
				$object->last_visit_to = $object->last_visit_to[$key];
				
				$object->indirizzo_di_residenza = $app->input->getString("indirizzo_di_residenza", '');
				$object->indirizzo_di_residenza = $object->indirizzo_di_residenza[$key];
				
				$object->nome_dell_organizzazione = $app->input->getString("nome_dell_organizzazione", '');
				$object->nome_dell_organizzazione = $object->nome_dell_organizzazione[$key];
				
				$object->ufficio_dell_organizzazione = $app->input->getString("ufficio_dell_organizzazione", '');
				$object->ufficio_dell_organizzazione = $object->ufficio_dell_organizzazione[$key];
				
				$object->indirizzo_dell_organizzazione = $app->input->getString("indirizzo_dell_organizzazione", '');
				$object->indirizzo_dell_organizzazione = $object->indirizzo_dell_organizzazione[$key];
				
				$object->telefono_dell_organizzazione = $app->input->getString("telefono_dell_organizzazione", '');
				$object->telefono_dell_organizzazione = $object->telefono_dell_organizzazione[$key];
				
				$object->birthday = $app->input->getString("birthday", '');
				$object->birthday = $object->birthday[$key];
				
				$object->telephone = $app->input->getString("numero_di_telefono", '');
				$object->telephone = $object->telephone[$key];
				
				//переводим даты в формат sql
				$object->birthday = ViaggioHelpersFrontend::dateToSQLformat($object->birthday);
				
				$object->date_from = ViaggioHelpersFrontend::dateToSQLformat($object->date_from);
				
				$object->date_to = ViaggioHelpersFrontend::dateToSQLformat($object->date_to);
				
				$object->last_visit_from = ViaggioHelpersFrontend::dateToSQLformat($object->last_visit_from);
				
				$object->last_visit_to = ViaggioHelpersFrontend::dateToSQLformat($object->last_visit_to);
	
				
				ViaggioHelpersFrontend::saveClientObject($object);
			}
		}	
	}
		
		echo '<pre>';
		print_r($object);
		//print_r($_POST);
		echo '</pre>';
		
		$this->setRedirect('index.php?option=com_viaggio&view=profile', JText::_('COM_VIAGGIO_SAVED'));
	}

	public function thanks()
    {
        $OrderId = $_GET['OrderId'];
        //var_dump($OrderId);
        if ($OrderId)
        {
            include('helpers/settings.php');
            $db = JFactory::getDbo();
            if (strpos($OrderId,'m')!==false)
            {
                $query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$OrderId."'";

                $db->setQuery($query);
                $manualPayment = $db->loadObject();

                $_SESSION['ViaggiobuyManualTourId'] = $manualPayment->tour_id;
                $_SESSION['ViaggiobuyManualPaymentID'] = $manualPayment->paymentID;

                $helper = new ViaggioHelpersFrontend();
                $helper->makeCheck($OrderId,$manualPayment->email,$manualPayment->telefon,$manualPayment->amountRub/100);

                $query = "SELECT * FROM `#__viaggio_orders` WHERE id = ".$manualPayment->order_id;
                $db->setQuery($query);
                $order = $db->loadObject();

                $query = "SELECT * FROM `#__viaggio_tours` WHERE id = ".$manualPayment->tour_id;
                $db->setQuery($query);
                $order->tour = $db->loadObject();

                ViaggioHelpersFrontend::sendEmailByTemplate('sendEmailAfterPaymentM',$manualPayment->email,null, $order);
            }
            else
            {
                $query = "SELECT * FROM `#__viaggio_orders` WHERE id = '".$OrderId."'";

                $db->setQuery($query);
                $manualPayment = $db->loadObject();

                $_SESSION['ViaggiobuyTourId'] = $manualPayment->tour_id;
                $_SESSION['ViaggiobuyID'] = $manualPayment->id;
                $helper = new ViaggioHelpersFrontend();
                $helper->makeCheck($OrderId,$manualPayment->email,$manualPayment->telephone,$manualPayment->amount/100);
            }

            $query2 = 'SELECT cats.path as path, cats.alias as catalias, t.alias as touralias, cats.id as cat_id FROM `#__categories` AS cats JOIN `#__viaggio_tours` as t ON t.category = cats.id  where t.id = "'.$db->escape($manualPayment->tour_id).'"';

            $db->setQuery($query2);
            $result= $db->loadObject();

            $query3 = "SELECT `path` FROM `#__menu` WHERE component_id = 10074 and params like '%cat_id\":\"".$result->cat_id."\"%'";

            $db->setQuery($query3);
            $menu = $db->loadObject();
            //var_dump($query3);
            return $this->setRedirect('/'.$menu->path.'/'.$result->touralias, false);
        }

    }
}

