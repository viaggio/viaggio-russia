<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
//require_once(JPATH_COMPONENT.'/helpers/viaggio.php');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelTours extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'ordering', 'a.ordering',
				'state', 'a.state',
				'created_by', 'a.created_by',
				'modified_by', 'a.modified_by',
				'name', 'a.name',
				'alias', 'a.alias',
				'desc', 'a.desc',
				'category', 'a.category',
				'from', 'a.from',
				'to', 'a.to',
				'inn4count', 'a.inn4count',
				'inn3count', 'a.inn3count',
				'inn4cost', 'a.inn4cost',
				'inn3cost', 'a.inn3cost',
				'halfboard4', 'a.halfboard4',
				'halfboard3', 'a.halfboard3',
				'singola4', 'a.singola4',
				'singola3', 'a.singola3',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app  = JFactory::getApplication();
		$list = $app->getUserState($this->context . '.list');

		$ordering  = isset($list['filter_order'])     ? $list['filter_order']     : null;
		$direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;

		$list['limit']     = (int) JFactory::getConfig()->get('list_limit', 20);
		$list['start']     = $app->input->getInt('start', 0);
		$list['ordering']  = $ordering;
		$list['direction'] = $direction;

		$app->setUserState($this->context . '.list', $list);
		$app->input->set('list', null);

		// List state information.
		parent::populateState($ordering, $direction);

        $app = JFactory::getApplication();

        $ordering  = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

        if ($limit == 0)
        {
            $limit = $app->get('list_limit', 0);
        }
		$limit = 0;
        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
	}
	
	function getCategory()
	{
		$category = new stdClass();
		
		// category by MENU
		$menu_category = 0;
		$params = 0;
		$app = JFactory::getApplication();
		// Get active menu
		$currentMenuItem = $app->getMenu()->getActive();
		if($currentMenuItem){
			// Get params for active menu
			$params = $currentMenuItem->params;
		}
		if($params)	{
			
			// Access param you want
			$menu_category = $params->get('cat_id', 0);
		}
		
		//get category id from request
		if(isset($_REQUEST['cat_id'])) $menu_category = intval($_REQUEST['cat_id']);
		//var_dump($menu_category);
		
		//var_dump($menu_category);
		if ($menu_category){
			$db    = $this->getDbo();
			$query = "SELECT * FROM `#__categories` where id = '$menu_category'";
			$db->setQuery($query);
			return $db->loadObject();
		}

		return $category;
	}

	public function getItem()
    {

    }

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query
			->select(
				$this->getState(
					'list.select', 'DISTINCT a.*'
				)
			);

		$query->from('`#__viaggio_tours` AS a');
		
		// Join over the users for the checked out user.
		$query->select('uc.name AS uEditor');
		$query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		// Join over the created by field 'created_by'
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');

		// Join over the created by field 'modified_by'
		$query->join('LEFT', '#__users AS modified_by ON modified_by.id = a.modified_by');
		// Join over the category 'category'
		$query->select('categories_2622291.title AS category');
		$query->join('LEFT', '#__categories AS categories_2622291 ON categories_2622291.id = a.category');
		
		if (!JFactory::getUser()->authorise('core.edit', 'com_viaggio'))
		{
			$query->where('a.state = 1');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.name LIKE ' . $search . '  OR categories_2622291.title LIKE ' . $search . '  OR  a.inn4count LIKE ' . $search . '  OR  a.inn3count LIKE ' . $search . '  OR  a.inn4cost LIKE ' . $search . '  OR  a.inn3cost LIKE ' . $search . '  OR  a.halfboard4 LIKE ' . $search . '  OR  a.halfboard3 LIKE ' . $search . '  OR  a.singola4 LIKE ' . $search . '  OR  a.singola3 LIKE ' . $search . ' )');
			}
		}
		

		// Filtering category
		$filter_category = $this->state->get("filter.category");
		if ($filter_category)
		{
			$query->where("a.category = '".$db->escape($filter_category)."'");
		}
		
		// Filtering category by MENU
		$menu_category = 0;
		$params = 0;
		$app = JFactory::getApplication();
		// Get active menu
		$currentMenuItem = $app->getMenu()->getActive();
		if($currentMenuItem){
			// Get params for active menu
			$params = $currentMenuItem->params;
		}
		if($params)	{
			
			// Access param you want
			$menu_category = $params->get('cat_id', 0);
		}

		//get category id from request
		if(isset($_REQUEST['cat_id'])) $menu_category = intval($_REQUEST['cat_id']);
		//var_dump($menu_category);
		
		//var_dump($menu_category);
		if ($menu_category)
		{
		    $search = false;
		    if (isset($_POST['filter_search']))
                $search = $db->Quote('%' . $db->escape($_POST['filter_search'], true) . '%');
			//echo "SELECT cats.id as catid, cats.parent_id, tours.* FROM `#__categories` as cats LEFT JOIN `#__viaggio_tours` as tours ON cats.id = tours.category WHERE (tours.category = '".$db->escape($menu_category)."' OR cats.parent_id = '".$db->escape($menu_category)."') AND tours.from > (NOW() + INTERVAL 10 DAY) AND `published` =1";
			return "SELECT cats.id as catid, cats.parent_id, tours.* FROM `#__categories` as cats LEFT JOIN `#__viaggio_tours` as tours ON cats.id = tours.category WHERE (tours.category = '".$db->escape($menu_category)."' OR cats.parent_id = '".$db->escape($menu_category)."') AND tours.from > (NOW() + INTERVAL 10 DAY) AND `state` =1 ".($search?" AND (name LIKE ".$search." OR `desc` LIKE ".$search.") ":"")." ORDER BY tours.from  ASC";
			//return 'SELECT t.* FROM `#__categories` AS cats JOIN `#__viaggio_tours` as t ON t.category = cats.id  where t.from > (NOW() - INTERVAL 10 DAY) AND t.state = 1 AND cats.parent_id = "'.$db->escape($menu_category).'" OR t.category = "'.$db->escape($menu_category).'"';
			//$query->where("a.category = '""' ");
		}

		// Filtering from
		// Checking "_dateformat"
		$filter_from_from = $this->state->get("filter.from_from_dateformat");
		$filter_Qfrom_from = (!empty($filter_from_from)) ? $this->isValidDate($filter_from_from) : null;

		if ($filter_Qfrom_from != null)
		{
			$query->where("a.from >= '" . $db->escape($filter_Qfrom_from) . "'");
		}

		$filter_from_to = $this->state->get("filter.from_to_dateformat");
		$filter_Qfrom_to = (!empty($filter_from_to)) ? $this->isValidDate($filter_from_to) : null ;

		if ($filter_Qfrom_to != null)
		{
			$query->where("a.from <= '" . $db->escape($filter_Qfrom_to) . "'");
		}

		// Filtering to
		// Checking "_dateformat"
		$filter_to_from = $this->state->get("filter.to_from_dateformat");
		$filter_Qto_from = (!empty($filter_to_from)) ? $this->isValidDate($filter_to_from) : null;

		if ($filter_Qto_from != null)
		{
			$query->where("a.to >= '" . $db->escape($filter_Qto_from) . "'");
		}

		$filter_to_to = $this->state->get("filter.to_to_dateformat");
		$filter_Qto_to = (!empty($filter_to_to)) ? $this->isValidDate($filter_to_to) : null ;

		if ($filter_Qto_to != null)
		{
			$query->where("a.to <= '" . $db->escape($filter_Qto_to) . "'");
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}
	//print_r($query);
		$query->order($db->quoteName('a.from') . ' ASC');  
		return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		
		foreach ($items as $item)
		{


			if (isset($item->category))
			{

				// Get the title of that particular template
					$title = ViaggioHelpersFrontend::getCategoryNameByCategoryId($item->category);

					// Finally replace the data object with proper information
					$item->category = !empty($title) ? $title : $item->category;
				}
		}

		return $items;
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 *
	 * @return void
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null)
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_VIAGGIO_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
	 *
	 * @param   string  $date  Date to be checked
	 *
	 * @return bool
	 */
	private function isValidDate($date)
	{
		$date = str_replace('/', '-', $date);
		return (date_create($date)) ? JFactory::getDate($date)->format("Y-m-d") : null;
	}
}
