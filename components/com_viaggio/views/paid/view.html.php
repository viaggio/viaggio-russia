<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class ViaggioViewPaid extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
//        JFactory::getApplication()->redirect('profile');
//        exit;
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');
		$this->item   = $this->get('Data');
		$this->params = $app->getParams('com_viaggio');

		if (!empty($this->item))
		{
			
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_viaggio');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		/*
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_VIAGGIO_DEFAULT_PAGE_TITLE'));
		}
		
		$title = $this->params->get('page_title', '');

		//settings by item
		if($this->item->name) $title = $this->item->name;
		
		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);
		
		$metadesc = false;
		if ($this->params->get('menu-meta_description'))
		{
			$metadesc = $this->params->get('menu-meta_description');
		}
		if( $this->item->metadesc) $metadesc = $this->item->metadesc;
		if($metadesc) $this->document->setDescription($metadesc);

		$keywords = false;
		if ($this->params->get('menu-meta_keywords'))
		{
			$keywords = $this->params->get('menu-meta_keywords');
		}
		if( $this->item->metakey ) $keywords = $this->item->metakey;
		if($keywords) $this->document->setMetadata('keywords', $keywords);
		
		$robots = false;
		if ($this->params->get('robots'))
		{
			$robots = $this->params->get('robots');
		}
		if( $this->item->robots ) $robots = $this->item->robots;
		if( $robots ) $this->document->setMetadata('robots', $robots);
		*/
	}
}
