<?php
//https://www.viaggio-russia.com/index.php?option=com_viaggio&view=tours&cat_id=24&format=ajax
foreach ($this->items as $i => $item) :
?>
<a href="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>">
       <div class="uk-grid tm-slideset-avion">
      <div class="uk-width-medium-3-10  ">
      <img class="uk-align-left" src="<?php if($item->category_img){ echo $item->category_img; }else{
					echo '/components/com_viaggio/media/images/noimage.gif';
				} ?>" alt="<?php echo $item->category_img_alt; ?>" title="<?php echo $item->category_img_title; ?>>
    </div>
   <div class="uk-width-medium-7-10">
      <h2><?php echo $item->name; ?></h2>
     <div class="uk-grid  ">
<div class="uk-width-1-2 uk-text-left">  
<span class="tm-article-date-day"><?php echo date('d', strtotime($item->from)); ?> </span>   
<span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($item->from); ?> </span>
  <span class="tm-article-date-day"><?php echo date('Y', strtotime($item->from)); ?></span>    
  </div>
    <div class="uk-width-1-2 uk-text-right">  
  
<span class="tm-article-date-day"><?php echo date('d', strtotime($item->to)); ?> </span>   
<span class="tm-article-date-day"><?php echo ViaggioHelpersFrontend::month($item->to); ?> </span>
    <span class="tm-article-date-day"> <?php echo date('Y', strtotime($item->to)); ?> </span>   

  </div>
	</div>
     <div class="uk-text-justify">
        <?php echo $item->desc; ?>
		<br>Duration: <?php echo ViaggioHelpersFrontend::detDuration($item->from,$item->to); ?> days 
     </div>
       <div class="uk-text-right uk-text-large"><?php echo ViaggioHelpersFrontend::getCost( $item->id, 0, 2 )/2; ?>€</div>
    
    </div>
    </div></a>
<?php endforeach; ?>