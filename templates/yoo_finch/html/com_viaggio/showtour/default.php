<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;
$subcat = explode('/',$_SERVER['REQUEST_URI'])[1];
$word = '';
switch ($subcat) {
    case 'mosca-sanpietroburgo':
        $word = 'stanza doppia';
        break;
    case 'crociera-sul-volga':
        $word = 'cabina doppia';
        break;
    case 'transiberiana':
        $word = 'cuccetta 2/4 posti';
        break;
    case 'anello-doro':
        $word = 'stanza doppia';
        break;
}
 ;
$doc = JFactory::getDocument();
$baseUrl = JUri::base();
//$doc->addStyleSheet($baseUrl.'templates/'.$this->template.'/css/style.css');
$doc->addStyleSheet('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
//print_r( $this->item )
$cost = ViaggioHelpersFrontend::getCost( $this->item->id, 0, 2 );

$db = JFactory::getDbo();

$query1 = "select sum(rating)/count(*) rating, count(*) num from votes where id_product = ".$this->item->id;
$db->setQuery($query1);
$rating = $db->loadObject();
if ($rating->rating)
    {
        $votes = $rating->num;
        $rating = round ($rating->rating,2);
    }
else
{
    $votes = 0;
    $rating = 0;
}
?>

<!-- 


<script  type="text/javascript" src="/components/com_viaggio/media/js/disqusloader.js"></script>
<script src="/images/js/jquery-ui.js"></script> <a id="top">

<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/1.jpg" class="uk-overlay-scale" alt="<?php echo $this->item->image_alt_1; ?>" title="<?php echo $this->item->image_title_1; ?>"   style="">



-->

<ol itemscope itemtype="http://schema.org/BreadcrumbList" class="uk-breadcrumb" style=" margin-top: -20px; ">
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a  itemprop="item"  href="/">
             <span itemprop="name">Home</span>
	     <meta itemprop="position" content="1" />
        </a>
    </li>
    <?php foreach ($this->item->pathway as $k=>$row ) { ;?>

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo $row->link; ?>">
             <span itemprop="name"><?php echo $row->name; ?> </span>
	     <meta itemprop="position" content="<?php echo $k+2; ?>" />
        </a>
    </li>
    <?php } ?>
    <li>
        <?php echo $this->item->name; ?>
    </li>
</ol>
<div class="uk-hidden">
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/1.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_1; ?>" title="<?php echo $this->item->image_title_1; ?>"   style="">
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/2.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_2; ?>" title="<?php echo $this->item->image_title_2; ?>"   style=""
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/3.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_3; ?>" title="<?php echo $this->item->image_title_3; ?>"   style="">
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/4.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_4; ?>" title="<?php echo $this->item->image_title_4; ?>"   style="">
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/5.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_5; ?>" title="<?php echo $this->item->image_title_5; ?>"   style="">
<img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/6.jpg" class="uk-overlay-scale" date-image alt="<?php echo $this->item->image_alt_6; ?>" title="<?php echo $this->item->image_title_6; ?>"   style="">



</div>


<?php if ($this->item->to < date('Y-m-d')) { echo ''; ?>
<div class="uk-panel-box uk-text-danger uk-text-center uk-text-bold" style=" background: #fff;"> <?php echo JText::_('COM_VIAGGIO_GIA_STATO_EFFETЕUATO'); ?> <a href="mailto:viaggio@russiantour.com">viaggio@russiantour.com</a></div>
    <?php echo ''; } ?>

<form action="/index?option=com_viaggio&task=sendorder" method="post"  class="uk-form" id="1viaggioOpenTourForm" >
<input type="hidden" name="tourid" value="<?php echo $this->item->id; ?>">
<div class="uk-spb-100 tm-opacity-90 uk-panel-box item-page  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> close_text <?php echo ''; } ?>" >
<div  itemtype="http://schema.org/Product" itemscope >
 
 
	 
		<div class="uk-grid uk-width-medium-1-1 uk-first-column uk-padding-remove ">
            <div class="uk-width-medium-1-2" style="padding-left: 26px;">
                <div class="uk-slidenav-position">
                    <a   itemprop="image" href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/1.jpg" alt="<?php echo $this->item->image_alt_1; ?>" title="<?php echo $this->item->image_title_1; ?>" data-uk-lightbox="{group:'group1'}" class=""  >
                        <img  src="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/1_small.jpg" class="uk-overlay-scale" alt="<?php echo $this->item->image_alt_1; ?>" title="<?php echo $this->item->image_title_1; ?>"   style="">
                    </a>
                    <div style=" background-color: #0000008c; " class="uk-overlay-panel uk-overlay-bottom">
                        <ul class="uk-thumbnav   uk-flex-center">
                            <li data-uk-slideshow-item="1">
                                <a  href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/2.jpg" alt="<?php echo $this->item->image_alt_2; ?>" title="<?php echo $this->item->image_title_2; ?>" data-uk-lightbox="{group:'group1'}">
                                    <img src="/images/<?php echo $_SERVER ['REDIRECT_URL']; ?>/2_small.jpg"class="uk-thumbnail-mini" alt="<?php echo $this->item->image_alt_2; ?>" title="<?php echo $this->item->image_title_2; ?>"  >
                                </a>
                            </li>
                            <li data-uk-slideshow-item="2">
                                <a  href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/3.jpg" data-uk-lightbox="{group:'group1'}"  alt="<?php echo $this->item->image_alt_3; ?>" title="<?php echo $this->item->image_title_3; ?>">
                                    <img src="/images/<?php echo $_SERVER ['REDIRECT_URL']; ?>/3_small.jpg"class="uk-thumbnail-mini  " alt="<?php echo $this->item->image_alt_3; ?>" title="<?php echo $this->item->image_title_3; ?>"  >
                                </a>
                            </li>
                            <li data-uk-slideshow-item="3">
                                <a  href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/4.jpg" data-uk-lightbox="{group:'group1'}"alt="<?php echo $this->item->image_alt_4; ?>" title="<?php echo $this->item->image_title_4; ?>">
                                    <img src="/images/<?php echo $_SERVER ['REDIRECT_URL']; ?>/4_small.jpg"class="uk-thumbnail-mini  " alt="<?php echo $this->item->image_alt_4; ?>" title="<?php echo $this->item->image_title_4; ?>"  >
                                </a>
                            </li>
                            <li data-uk-slideshow-item="4"><a  href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/5.jpg" data-uk-lightbox="{group:'group1'}"alt="<?php echo $this->item->image_alt_5; ?>" title="<?php echo $this->item->image_title_5; ?>">
                                    <img src="/images/<?php echo $_SERVER ['REDIRECT_URL']; ?>/5_small.jpg" class="uk-thumbnail-mini  uk-overlay-scale" alt="<?php echo $this->item->image_alt_5; ?>" title="<?php echo $this->item->image_title_5; ?>" >
                                </a>
                            </li>
                            <li data-uk-slideshow-item="5"><a  href="/images<?php echo $_SERVER ['REDIRECT_URL']; ?>/6.jpg" data-uk-lightbox="{group:'group1'}"alt="<?php echo $this->item->image_alt_6; ?>" title="<?php echo $this->item->image_title_6; ?>">
                                    <img src="/images/<?php echo $_SERVER ['REDIRECT_URL']; ?>/6_small.jpg" class="uk-thumbnail-mini " alt="<?php echo $this->item->image_alt_6; ?>" title="<?php echo $this->item->image_title_6; ?>" >
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

	        <div  class="uk-width-medium-1-2 uk-text-center " >
            
				 <h1 class="uk-article-title uk-padding-top-remove"     itemprop="name"><?php echo $this->item->name; ?> </h1>
			<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<div    class="uk-icon-large" >
                <div style=" color: #ffca00; " id="isvet_the_star" tour_id="<?php echo $this->item->id; ?>">
                    <span class="uk-icon-star-o" num=1> </span>
                    <span class="uk-icon-star-o" num=2></span>
                    <span class="uk-icon-star-o" num=3></span>
                    <span class="uk-icon-star-o" num=4></span>
                    <span class="uk-icon-star-o" num=5></span>
                    <div class="message"></div>
                </div>
            </div>
			    
                <div class=" " >Valutazione <span itemprop="ratingValue" id="isvet_rating"><?php echo $rating; ?></span> / Voti <span  <span itemprop="reviewCount" id="isvet_votes"><?php echo $votes; ?></span></div></div>
                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        jQuery('#isvet_the_star span').click(function(e){
                            jQuery(this).toggleClass('fixed');
                            jQuery('#isvet_the_star span').attr('class','uk-icon-star-o');
                            for (let i = 1;i<=jQuery(this).attr('num');i++)
                            {
                                jQuery('#isvet_the_star span[num='+i+']').attr('class','uk-icon-star');
                            }
                            jQuery.post('/components/com_viaggio/change_rating.php', {
                                obj_id: jQuery('#isvet_the_star').attr('tour_id'),
                                rating: jQuery(this).attr('num')
                            }, isvet_notice);
                        });

                        jQuery('#isvet_the_star span').on('mousemove', function(e){
                            jQuery('#isvet_the_star span').attr('class','uk-icon-star-o');
                            for (let i = 1;i<=jQuery(this).attr('num');i++)
                            {
                                jQuery('#isvet_the_star span[num='+i+']').attr('class','uk-icon-star');
                            }
                        });

                        jQuery('#isvet_the_star').on('mouseout', function (e) {
                            recount();
                        });

                    });

                    function recount() {
                        jQuery('#isvet_the_star span').attr('class','uk-icon-star-o');
                        let rating = parseFloat(jQuery('#isvet_rating').text());
                        let full = Math.floor(rating);
                        let half = (full != rating);
                        for (let i = 1;i<=full;i++)
                        {
                            jQuery('#isvet_the_star span[num='+i+']').attr('class','uk-icon-star');
                        }
                        if (half)
                        {
                            jQuery('#isvet_the_star span[num='+(full+1)+']').attr('class','uk-icon-star-half-empty');
                        }
                    }
                    recount();
                    function isvet_notice(text){
                        if (text == '-')
                        {
                            jQuery('#isvet_the_star .message').fadeOut(500, function(){ jQuery(this).text('<?php echo JText::_('COM_VIAGGIO_ALREADY_VOTED'); ?>'); }).fadeIn(2000);
                        }
                        else
                        {
                            jQuery('#isvet_rating').text(text);
                            recount();
                            jQuery('#isvet_votes').text(parseFloat(jQuery('#isvet_votes').text())+1);
                            jQuery('#isvet_the_star .message').fadeOut(500, function(){ jQuery(this).text('<?php echo JText::_('COM_VIAGGIO_GRAZIE_VALUTAZIONE'); ?>'); }).fadeIn(2000);
                        }
                    }

                </script>
                <h2   itemprop="description" class="text-transform uk-h4 uk-width-large-1-1  uk-text-justify uk-align-center"><p><?php echo $this->item->desc; ?></p></h2>
           

 


 
		<div   ">
    <h6 class="uk-text-justify" itemprop="address"  ">
		<h2 class="uk-hidden"   title="<?php echo $this->item->citys; ?>"> <?php echo $this->item->citys; ?></h2>
	<div class="uk-hidden"  >Russia</div> 
    <h2 class="uk-h4 uk-text-justify text-transform"  >
        <?php echo JText::_('COM_VIAGGIO_DA_VISITARE'); ?>   <span     ><?php  echo '  '.str_replace(',',', ',$this->item->citys).'
					'; ?></span>
        </h2>
</h6>

</div>


            </div>
        </div>
        <div style="display: none; text-align: center" id="Kdata"></div>
    <div  itemprop="offers" itemtype="http://schema.org/Offer" itemscope class="uk-width-large-9-10 uk-text-justify uk-align-center " >
	<br>
		 <?php echo $this->item->tab3; ?>

            <h4 class="uk-heading-large uk-dropdown-top uk-text-center uk-article-title uk-heading-line uk-margin-bottom-remove  hide-on-manual-pay hide-on-manual-pay1">

                <span id="cost">
                    <table>
                        <tbody>
                        <tr ">
						
						     <link itemprop="url" href="<?php echo $_SERVER ['REDIRECT_URL']; ?>" />
      <meta itemprop="availability" content="https://schema.org/InStock" />
 
     
                            <td  id="cost1up"><span itemprop="priceCurrency" content="EUR"><?php echo ($cost/2); ?><span >€</span> </td>
                            <td id="cost2up" style="font-size: 33px;line-height: 23px;text-transform: none;font-weight: bold;padding-left: 5px;"> x 2 = </td>
                            <td id="cost3up"><span itemprop="price" content="<?php echo $cost; ?>"><?php echo $cost; ?>€

 <meta itemprop="itemCondition" content="https://schema.org/UsedCondition" />
<meta   itemprop="priceValidUntil" content="<?php echo date('Y-m-d', strtotime($this->item->from)); ?>"> </meta>

	<?php if ($this->item->to < date('Y-m-d')) { echo ''; ?>
<meta itemprop="availability" content="http://schema.org/OutOfStock" />
    <?php echo ''; } elseif ($this->item->to > date('Y-m-d')) {echo ' 
	<link itemprop="url" href="'.$_SERVER ['REDIRECT_URL'].'" >';} ?> 

							</span>

							</td>
                        </tr>
                        <tr style="font-size: 16px;line-height: 23px;text-transform: none;font-weight: bold;">
                            <td id="cost1down"><?php echo JText::_('COM_VIAGGIO_PREZZO_A_PERSONA'); ?></td>
                            <td id="cost2down"></td>
                            <td id="cost3down"><?php echo JText::_('COM_VIAGGIO_PREZZO_COMPLESSIVO'); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </span>
            </h4>
            <div class="uk-text-medium uk-text-center  hide-on-manual-pay hide-on-manual-pay1" id="bottomText"> <?php echo JText::_('COM_VIAGGIO_SE_VIAGGIATE_DA_SOLI'); ?> </div><br>

			<div class="uk-grid ">
				<div class="uk-width-1-2 uk-text-center uk-text-large ">
                    <?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_MOSCA'); ?>
					<span  class="uk-icon-plane rotate90 uk-icon-small"></span> <span      id="tourItemfrom"><?php echo date('d/m/Y', strtotime($this->item->from)); ?></span> </div>
			    <div class="uk-width-1-2 uk-text-center uk-text-large  ">
			 <?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_ITALIA'); ?> <span class="uk-icon-plane uk-icon-small"> </span>
			          
					<span  content="<?php echo date('Y-m-d H:i', strtotime($this->item->to)); ?>" id="tourItemTo"><?php echo date('d/m/Y', strtotime($this->item->to)); ?></span>
                </div>
            </div>
			
			
			
			
			</div>
			    <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
      <meta itemprop="name" content="Russian Tour International" />
    </div>
			  <meta itemprop="sku" content="460631078<?php echo $this->item->id; ?>" />
	    <div itemprop="review" itemtype="http://schema.org/Review" itemscope>
      <div itemprop="author" itemtype="http://schema.org/Person" itemscope>
        <meta itemprop="name" content="Macro Maggi" />
      </div>	 </div>	
	   <meta itemprop="mpn" content="rt_<?php echo $this->item->id; ?>" />
			
			           <?php
                if (isset($_SESSION['ViaggiobuyManualTourId']) && $_SESSION['ViaggiobuyManualTourId'] == $this->item->id)
                {
                    setcookie('ViaggiobuyManualTourId', '', time()+60*60*24*365);
                    setcookie('ViaggiobuyManualPaymentID', '', time()+60*60*24*365);
                    $payment_id = $_SESSION['ViaggiobuyManualPaymentID'];

                    $db = JFactory::getDbo();
                    $query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$payment_id."'";

                    $db->setQuery($query);
                    $manualPayment = $db->loadObject();

                    $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
                    $db->setQuery($query);
                    $manualPaymentDetails = $db->loadObjectList();

                                       echo '<div class="uk-width-large-9-10 uk-text-justify uk-align-center">
					<h4 class="uk-heading-large uk-dropdown-top uk-text-center uk-article-title uk-heading-line uk-margin-bottom-remove "> <span ><table> <tbody> <tr> <td  >Pagamento effettuato  '.$manualPayment->amountEuro.' €</td>   </tr>  </tbody> </table> </span> </h4>
					<div class="uk-text-medium uk-text-center"  >Gentile  <b>'.$manualPayment->fio.'</b>  pagamento della fattura proforma <b>'.$manualPayment->paymentID.'</b>     , collegata al contratto <b>'.$manualPayment->order_id.'</b> , e` avvenuto con successo. La ringraziamo per la fiducia e le auguriamo buon viaggio!</div> 
					 </div> 
					';
echo '<style>.hide-on-manual-pay1 {display:none;}</style>';
                }
                elseif (isset($_SESSION['ViaggiobuyTourId']) && $_SESSION['ViaggiobuyTourId'] == $this->item->id)
                {
                    $order_id = $_SESSION['ViaggiobuyID'];

                    $db = JFactory::getDbo();
                    $query = "SELECT * FROM `#__viaggio_orders` WHERE id = '".$order_id."'";

                    $db->setQuery($query);
                    $order = $db->loadObject();

                    echo '<div class="uk-width-large-9-10 uk-text-justify uk-align-center">
					<h4 class="uk-heading-large uk-dropdown-top uk-text-center uk-article-title uk-heading-line uk-margin-bottom-remove "> <span ><table> <tbody> <tr> <td  >Pagamento effettuato  '.$order->valute_amount.' €</td>   </tr>  </tbody> </table> </span> </h4>
					<div class="uk-text-medium uk-text-center"  >Gentile <b>'.$order->nomecgome.'</b> l pagamento della fattura proforma  <b>'.$order->id.'</b>    , collegata al contratto <b>'.$order->id.'</b>  , e` avvenuto con successo. La ringraziamo per la fiducia e le auguriamo buon viaggio! </div> 
					 </div> 
					';
                }
                elseif (isset($_COOKIE['ViaggiobuyManualTourId']) && $_COOKIE['ViaggiobuyManualTourId'] == $this->item->id)
                {
                    $payment_id = $_COOKIE['ViaggiobuyManualPaymentID'];

                    $db = JFactory::getDbo();
                    $query = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$payment_id."'";

                    $db->setQuery($query);
                    $manualPayment = $db->loadObject();

                    $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
                    $db->setQuery($query);
                    $manualPaymentDetails = $db->loadObjectList();

                    echo '<div class="uk-width-large-9-10 uk-text-justify uk-align-center">
					<h4 class="uk-heading-large uk-dropdown-top uk-text-center uk-article-title uk-heading-line uk-margin-bottom-remove "> <span ><table> <tbody> <tr> <td  >'.$manualPayment->amountEuro.' €</td>   </tr>  </tbody> </table> </span> </h4>
					<div class="uk-text-medium uk-text-center"  >Gentile <b>'.$manualPayment->fio.'</b>, può procedere al pagamento della fattura proforma numero <b>'.$manualPayment->paymentID.'</b>  collegata al contratto numero <b>'.$manualPayment->order_id.'</b> , cliccando sul pulsante sottostante.</div> 
					<div class=" uk-text-center"><a class="uk-button uk-margin-small-top uk-button-primary uk-width-medium-1-4" href="https://www.viaggio-russia.com/index.php?option=com_viaggio&task=buyManual&payment_id='.$payment_id.'">Pagamento</a></div></div>  '.$this->item->citys.'
					';
 				//var_dump($manualPayment);


//var_dump($manualPaymentDetails);







					echo '<style>.hide-on-manual-pay {display:none;}</style>';

                }
                ?>
		 <div class="hide-on-manual-pay hide-on-manual-pay1">
		<?php if($this->item->russiandays){ ?>
			<div class="uk-width-medium-1-1 uk-text-center">
			<br><b class="uk-text-uppercase"><?php echo JText::_('COM_VIAGGIO_CATEGORIA'); ?> </b>  &nbsp;&nbsp;
			<select name="transib" id="transib" class="calc">
                <?php if($this->item->category == 31){ ?>
                    <option id="classic" value="classic"><?php echo JText::_('COM_VIAGGIO_CLASSIC_1'); ?></option>
                    <option id="superior" value="superior"><?php echo JText::_('COM_VIAGGIO_SUPERIOR_1'); ?></option>
                    <option id="superiorplus" value="superiorplus"><?php echo JText::_('COM_VIAGGIO_SUPERIOR_PLUS_1'); ?></option>
                    <option id="nostalgic" value="nostalgic"><?php echo JText::_('COM_VIAGGIO_NOSTALGIC_1'); ?></option>
                <?php } else { ?>
                    <option id="classic" value="classic"><?php echo JText::_('COM_VIAGGIO_CLASSIC'); ?></option>
                    <option id="superior" value="superior"><?php echo JText::_('COM_VIAGGIO_SUPERIOR'); ?></option>
                    <option id="superiorplus" value="superiorplus"><?php echo JText::_('COM_VIAGGIO_SUPERIOR_PLUS'); ?></option>
                    <option id="nostalgic" value="nostalgic"><?php echo JText::_('COM_VIAGGIO_NOSTALGIC'); ?></option>
                <?php } ?>
<!-- 				<option id="bolshoy4" value="bolshoy4"><?php echo JText::_('COM_VIAGGIO_BOLSHOY_IV'); ?></option>
				<option id="platinum5" value="platinum5"><?php echo JText::_('COM_VIAGGIO_PLATINUM_5'); ?></option>-->
			</select>
			</div><br><b ><?php echo JText::_('COM_VIAGGIO_SUPPORTO'); ?></b> <br>
			<label> <input name="needvisamongolia" type="checkbox" id="visamongolia"  class="calc" <?php if(!$this->item->visamongolia){ echo 'disabled'; } ?>>   <?php echo JText::_('COM_VIAGGIO_VISA_MONGOLIA'); ?>  </label>
			<label> <input name="needvisachina" type="checkbox" id="visachina"  class="calc" <?php if(!$this->item->visachina){ echo 'disabled'; } ?>>  <?php echo JText::_('COM_VIAGGIO_VISA_CHINA'); ?>   </label>
		<?php } ?>

		<?php if(!$this->item->nostell1 && !$this->item->russiandays){ ?>
		<div class="uk-grid   uk-grid-small   uk-width-medium-1-1"  ">
			<div class="uk-width-medium-1-1 uk-text-center" id="4steelButton">  <a data-uk-tooltip title="<?php echo $this->item->viaggio4stars; ?>"  href="#">
				<span class="uk-icon-star"></span><span class="uk-icon-star"></span><span class="uk-icon-star"></span><span class="uk-icon-star"></span>
  <br> <span class="uk-h4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></span></a>
			</div><!--
			<div class="uk-width-medium-1-2 uk-text-center" id="3steelButton"><!-- <a data-uk-tooltip title="<?php echo $this->item->viaggio3stars; ?>" href="#">
				<span class="uk-icon-calendar"></span>
	 <br>
				<span class="uk-h4"><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></span></a>
			</div> -->
		</div>
		<div style="clear:both;"></div>

		<div style="display: none;width:100%">
		<select name="stellcount" id="stellcount" class="calc">
			<?php if($this->item->inn3cost){ ?>
			<option id="stellcount3" value="3" <?php if(!$this->item->inn3cost){ echo 'disabled'; } ?>><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></option>
			<option id="stellcount4" value="4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></option>
			<?php }else{ ?>
			<option id="stellcount4" value="4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></option>
			<option id="stellcount3" value="3" <?php if(!$this->item->inn3cost){ echo 'disabled'; } ?>><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></option>
			<?php } ?>
		</select>
		</div>
 
		<?php } ?>


	<?php/*oninvalid="this.setCustomValidity('Feel it')"*/?>



		</div>
    <div style="width:100%">
			<?php if(!$this->item->nostell1 && !$this->item->russiandays){ ?>
			<b class="uk-text-uppercase"><?php echo JText::_('COM_VIAGGIO_SUPPLEMENTI'); ?></b>
 <br>
		<label> <input name="insurance" type="checkbox" id="insurance"  class="calc" disabled>   <?php echo JText::_('COM_VIAGGIO_MEZZA_PENSIONE'); ?> </label>&nbsp;&nbsp;
               
			 
				<?php if(!in_array($this->item->id,[54,55,56,60,61,62,93,94,97,98]) && $this->item->category != 23) { ?>
		<label> <input name="insurance2" type="checkbox" id="insurance2" cost="<?php echo intval($this->item->insurance_cost); ?>" class="calc">   <?php echo JText::_('COM_VIAGGIO_GIORNOAGGIUNTIVO'); ?></label>
                <?php } ?>
                <br>
		<?php } ?>
		<br>
    <b class="uk-text-uppercase"><?php echo JText::_('COM_VIAGGIO_INVIAREUNARICHIESTA'); ?></b>
	</div>


 <div class="hide-on-manual-pay hide-on-manual-pay1" >
 <?php if( $this->item->user->guest ){ ?>
  <!--<div ><h3>
    <?php echo JText::_('COM_VIAGGIO_REGISTRATION'); ?>
    </h3> </div>-->
  <!-- <div>
   <?php echo JText::_('COM_VIAGGIO_INDICARE_I_DATI_DELLA_PERSONA'); ?>
    </div>-->
 <?php  } else ?>
        <div  >
            <style>
                input.error {border-color: red !important;}
            </style>
            <?php
            if (!$this->item->user->guest) { $type='hidden'; ?>
                <?php echo JText::_('COM_VIAGGIO_NOMECOGNOME'); ?>: <?php echo $this->item->user->name; ?> |
                <?php echo JText::_('COM_VIAGGIO_TELEFONE'); ?>: <?php echo $this->item->telephone; ?> |
                E-mail: <?php echo $this->item->user->email; ?>
            <?php } else { $type = 'text'; }?>
            <input required <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="nomecgome" placeholder="<?php echo JText::_('COM_VIAGGIO_NOMECOGNOME'); ?>" value="<?php //echo $this->item->user->nomecgome; ?>" class=" uk-margin-small-top  uk-width-medium-3-10">
            <input required <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="telefone" placeholder="<?php echo JText::_('COM_VIAGGIO_TELEFONE'); ?>" value="<?php //echo $this->item->user->telephone; ?>" class=" uk-margin-small-top  uk-width-medium-2-10">
            <input required <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="email" <?php if(!$this->item->user->guest){ echo 'value="'.$this->item->user->email.'" '; } ?> placeholder="E-mail" class="uk-margin-small-top uk-width-medium-2-10">
<button  onClick="fbq('track', 'Purchase');" class="uk-button uk-margin-small-top uk-button-primary uk-width-medium-1-4" <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> id="MaggioriButton"><?php echo JText::_('COM_VIAGGIO_MAGGIORI'); ?></button>
            <script>
                jQuery(document).ready(function($) {
                    $('#MaggioriButton').on('click', function(){
                        var from = $('#tourItemfrom').text();
                        var to = $('#tourItemTo').text();
                        if (jQuery('#MaggioriButton').parents('form').find('input[name=email]').val()=='')
                        {
                            jQuery('#MaggioriButton').parents('form').find('input[name=email]').addClass('error');
                        }
                        else
                        {
                            jQuery('#MaggioriButton').parents('form').find('input[name=email]').removeClass('error');
                            UIkit.modal("#MAGGIORI").show();
                            $.ajax({
                                url: '/index.php?from='+from+'&to='+to+'&option=com_viaggio&maggioriSend=1&russiantour_trip_id=<?php echo $this->item->russiantour_trip_id; ?>&url='+document.location.origin + document.location.pathname+'&title=<?php echo addslashes($this->item->name); ?>&cost='+jQuery('#cost').text()+'&email='+jQuery('input[name="email"]').val()+'&name='+jQuery('input[name="nomecgome"]').val()+'&phone='+jQuery('input[name="telefone"]').val(),
                                success: function(data){
                                    //$('#maggioriResult').html(data)
                                }
                            });
                        }
                    })
                });
            </script>

<div id="MAGGIORI1" class="uk-modal">
    <div class="uk-modal-dialog uk-block-default" >
        <a class="uk-modal-close uk-close"></a>

    </div>
</div>

<div id="MAGGIORI" class="uk-modal " aria-hidden="true" style="display: none; overflow-y: scroll;">
                                <div class=" uk-block-default uk-modal-dialog">
                                    <button type="button" class="uk-modal-close uk-close"></button>
                                    <div class="uk-modal-header">
                                        <span class="uk-h2"> <?php echo JText::_('COM_VIAGGIO_THANKS'); ?> </span>
                                    </div>
                                    <div id="maggioriResult"></div>
                                    <p><?php echo JText::_('COM_VIAGGIO_REQUEST_INFORMATION'); ?></p>
                                    <div class="uk-modal-footer uk-text-right">

                                        <button type="button" class="uk-button uk-modal-close uk-button-primary"><?php echo JText::_('COM_VIAGGIO_CLOSE'); ?></button>
                                    </div>
                                </div>
                            </div>



    </div>

  <?php echo JText::_('COM_VIAGGIO_COMPILATE_CAMPI_DUE'); ?> <?php echo $word ;?> . <?php echo JText::_('COM_VIAGGIO_COMPILATE_CAMPI_SOLI'); ?><br>
<br>
<b><?php echo JText::_('COM_VIAGGIO_NOMINATIVI'); ?></b>
    </div>

			<div   class="hide-on-manual-pay hide-on-manual-pay1"    id="toggle1" >
			<div id="tourClientsBlock" class="uk-grid">

			<div class="uk-width-1-1 uk-form tourClientsElement">

				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  name="cognome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 ">
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="nome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10 ">
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  name="passport[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_PASSAPORTO'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 ">
				<select <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  name="sex[]" class="uk-margin-small-top  uk-width-medium-1-10" autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option>
				<option <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select>
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="nascita[]" style=" margin-top: 5px; " class=" uk-width-medium-1-6  participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" required placeholder="Data di nascita" data-uk-datepicker="{format:'DD/MM/YYYY'}">
				<select <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  style="display: none;" name="nazionalita[]" class="uk-margin-small-top uk-width-medium-1-6  " autocomplete="off"><option name="nazionalita[]" value="">- <?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?> -</option></select>  	 <a href="#" style="display: none;" id="addTourClient" class="calc uk-margin-small-top uk-button "><?php echo JText::_('COM_VIAGGIO_ADD'); ?></a>

			</div>
			<div class="uk-width-1-1  uk-form tourClientsElement">
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="cognome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top  uk-width-medium-2-10 ">
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="nome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10 ">
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  name="passport[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_PASSAPORTO'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 ">
				<select <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  name="sex[]" class="uk-margin-small-top uk-width-medium-1-10 " autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option>
				<option <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select>
				<input  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" name="nascita[]" style=" margin-top: 5px; " class=" uk-width-medium-1-6  participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" required placeholder="Data di nascita" data-uk-datepicker="{format:'DD/MM/YYYY'}">
				<select  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  style="display: none;" name="nazionalita[]" class="uk-margin-small-top  uk-width-medium-1-6 " autocomplete="off"><option name="nazionalita[]" value="">- <?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?> -</option></select>
				<button  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>" class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient"><i class="uk-icon-remove"></i></button>
			</div>

			</div><br></div>
					<button  <?php if ($this->item->to < date('Y-m-d')) { echo ''; ?> disabled <?php echo ''; } ?> type="<?php echo $type; ?>"  type="submit" class="uk-button uk-button-primary " id="bay-button" <?php if(isset($_COOKIE['ViaggioTour'.$this->item->id.'BayPushed'])) ?>><?php echo JText::_('COM_VIAGGIO_PRENOTA'); ?></button>

				<div id="BAYWINDOW" class="uk-modal " aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class=" uk-block-default uk-modal-dialog">


                        <div id="maggioriResult"></div>
                        <p  class="uk-text-center"><i class="uk-icon-spinner uk-icon-spin uk-icon-large"></i></p>

                    </div>
                </div>
				<div class="uk-width-1-1 "><br>
			 <?php echo JText::_('COM_VIAGGIO_INFORMAZIONI_RELATIVE'); ?> <a href="mailto:viaggio@russiantour.com">viaggio@russiantour.com</a> . </div>
		 <?php echo $this->item->tab2; ?>
<?php echo $this->item->fulltext; ?>

		</div>


	</div>

	<button type="submit" class="uk-button uk-hidden uk-margin-small-top uk-button-primary uk-text-small  "><?php echo JText::_('COM_VIAGGIO_PROSEGUIRE'); ?> </button>
<!--<input type="submit" value="<?php echo JText::_('COM_VIAGGIO_ORDER'); ?>" class=" uk-margin-small-top  uk-width-medium-4-10">-->
</form>

<script>
	//нажатие кнопки купить
	jQuery(document).ready(function($) {//waitBack
        $('#1viaggioOpenTourForm').submit(function(e){
            $('#bay-button').prop('disabled', true);
            UIkit.modal("#BAYWINDOW").show();
        });
		$('#viaggioOpenTourForm').submit(function(e){
			//отменяем стандартное действие при отправке формы
			e.preventDefault();

			//берем из формы метод передачи данных
			var m_method=$(this).attr('method');

			//получаем адрес скрипта на сервере, куда нужно отправить форму
			var m_action=$(this).attr('action');

			//получаем данные, введенные пользователем в формате input1=value1&input2=value2...,
			//то есть в стандартном формате передачи данных формы
			var m_data=$(this).serialize();

			$.ajax({
				type: m_method,
				url: m_action+'&ajax=1',
				data: m_data,
				success: function(result){
					$('#bay-button').prop('disabled', true);
					//$('#test_form').html(result);
					UIkit.modal('#modal-bay').show();
				}
			});
		});
	});
</script>
<div id="modal6" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
	<div class="uk-modal-dialog">
		<button type="button" class="uk-modal-close uk-close"></button>
		<form action="/index.php?option=com_viaggio&task=register" id="viaggioRegisterForm">
		<div class="uk-modal-header">
			<h2>Password</h2>
		</div>
		<p><?php echo JText::_('COM_VIAGGIO_LOREM_IPSUM'); ?>

			<input name="password1" required  type="text"  placeholder="Password"  class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<input name="password2" required  type="text"  placeholder="Password"  class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<span id="validpass"></span>
		</p>
		<div class="uk-modal-footer uk-text-right">
			<button type="button" class="uk-button uk-modal-close"><?php echo JText::_('COM_VIAGGIO_CANCEL'); ?></button>
			<button type="submit" id="viaggioRegisterSubmit" class="uk-button uk-button-primary" disabled><?php echo JText::_('COM_VIAGGIO_SAVE'); ?></button>
		</div>
		</form>
	</div>
</div>
		 
<?php
	//определяем идентификатор тура для disqusа
	$disqus_identifier = 0;
	if( $this->item->comment_id ){
		$disqus_identifier = $this->item->comment_id;
	}else{
		$disqus_identifier = $this->item->id;
	}
?>

 	</div>
 <?php //print_r($this->item->jsItem); ?>
<script>
jQuery(document).ready(function($) {
	$( ".datepicker-here" ).datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: "dd/mm/yy",yearRange:'c-117:c'
	});
	var passFields = $('.edinfopass')
    //validResult = $("#validpass");
	passFields.on('input', comparingPasswords)
	$('#viaggioRegisterForm').on('submit', comparingPasswords)

	function comparingPasswords (e){
		var output = 'Ok',
			err = false,
			p1 = $.trim(passFields.eq(0).val()),
			p2 = $.trim(passFields.eq(1).val());
		if(p1 == '' || p2 == '') {
			//output = 'Заполните поля!';
			err = true;
		} else {
			if(p1 != p2) {
				//output = 'Пароли не совпадают!'
				err = true;
			}
		}
		if(err){
			$('#viaggioRegisterSubmit').prop('disabled', true)
		}else{
			$('#viaggioRegisterSubmit').prop('disabled', false)
		}
		//validResult.text(output)
		if(err)  e.preventDefault()
	}

	//clientform
	var html = '<div class="uk-width-1-1 uk-form tourClientsElement"><input required name="cognome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top  uk-width-medium-2-10"> <input required name="nome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10"><input  name="passport[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_PASSAPORTO'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 "> <select name="sex[]" class="uk-margin-small-top uk-width-medium-1-10" autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option><option value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select> <input required name="nascita[]" style=" margin-top: 5px; " class="uk-width-medium-1-6 participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_DATA_DI_NASCITA'); ?>" > <select style="display: none;" name="nazionalita[]" class="uk-margin-small-top uk-width-medium-1-6" autocomplete="off"><option name="nazionalita[]" value=""><?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?>-  -</option></select>   <button class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient"><i class="uk-icon-remove"></i></button></div>';

	$( '#4steelButton' ).click(function() {
		$('#stellcount').val(4); calc();
		return false;
	});

	$( '#3steelButton' ).click(function() {
		$('#stellcount').val(3); calc();
		return false;
	});

	$( "#addTourClient" ).click(function() {
		$('#tourClientsBlock').append(html);
		$('.datepicker-here').datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: 'dd/mm/yy',yearRange:'c-117:c'
	});
		if( $('.tourClientsElement').length > 1 ) $( "#addTourClient" ).hide(); calc();
		return false;
	});

	$( '.deleteTourClient' ).live('click', function() {
		$(this).parent().remove()
		if( $('.tourClientsElement').length < 2 ) $( "#addTourClient" ).show(); calc();
		return false;
	});

	$('.participants-birthdate').live('change', function() {
		calc();
	});

	$( '.calc' ).click(function() {
		calc();
	});

	$( '.calc' ).live('change', function() {
		calc();
	});

	var item = {};

	item.inn4cost = <?php echo intval($this->item->inn4cost); ?>;
	item.inn3cost = <?php echo intval($this->item->inn3cost); ?>;

	item.halfboard4 = <?php echo intval($this->item->halfboard4); ?>;
	item.halfboard3 = <?php echo intval($this->item->halfboard3); ?>;

	item.singola4 = <?php echo intval($this->item->singola4); ?>;
	item.singola3 = <?php echo intval($this->item->singola3); ?>;

	item.classic = <?php echo intval($this->item->classic); ?>;
	item.classicsingola = <?php echo intval($this->item->classicsingola); ?>;
	item.superior = <?php echo intval($this->item->superior); ?>;
	item.superiorsingola = <?php echo intval($this->item->superiorsingola); ?>;
	item.superiorplus = <?php echo intval($this->item->superiorplus); ?>;
	item.superiorplussingola = <?php echo intval($this->item->superiorplussingola); ?>;
	item.nostalgic = <?php echo intval($this->item->nostalgic); ?>;
	item.nostalgicsingola = <?php echo intval($this->item->nostalgicsingola); ?>;
	item.bolshoy4 = <?php echo intval($this->item->bolshoy4); ?>;
	item.bolshoy4singola = <?php echo intval($this->item->bolshoy4singola); ?>;
	item.platinum5 = <?php echo intval($this->item->platinum5); ?>;
	item.platinum5singola = <?php echo intval($this->item->platinum5singola); ?>;
	item.russiandays = <?php echo intval($this->item->russiandays); ?>;

	item.statndarvisa = <?php echo intval($this->item->statndarvisa); ?>;
	item.urgentevisa = <?php echo intval($this->item->urgentevisa); ?>;
	item.sendvisa = <?php echo intval($this->item->sendvisa); ?>;
	item.visa7day = <?php echo intval($this->item->visa7day); ?>;
	item.visa1day = <?php echo intval($this->item->visa1day); ?>;

	checkSettings()

	function checkSettings()
	{
		var clientsCount = $('.tourClientsElement').length;
		if(clientsCount == 1)
		{
			if(!item.singola3)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.singola4)
			{
				$('#4steelButton').addClass('disabledstell');
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').attr('disabled','disabled');
				$('#stellcount4').removeAttr('disabled');
			}


			if(!item.classicsingola)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}

			if(!item.superiorsingola)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}

			if(!item.superiorplussingola)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}

			if(!item.nostalgicsingola)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}

			if(!item.bolshoy4singola)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}

			if(!item.platinum5singola)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}else if(clientsCount == 2)
		{
			if(!item.inn3cost)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.inn4cost)
			{
				$('#4steelButton').hide();
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').show();
				$('#stellcount4').removeAttr('disabled');
			}


			if(!item.classic)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}

			if(!item.superior)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}

			if(!item.superiorplus)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}

			if(!item.nostalgic)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}

			if(!item.bolshoy4)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}

			if(!item.platinum5)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}

		var stellcount = $('#stellcount').val();
		if(stellcount == 4)
		{
			if(item.halfboard4)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
            $('#3steelButton').addClass('notselectedstell');
            $('#4steelButton').removeClass('notselectedstell');
		}else if(stellcount == 3)
		{
			if(item.halfboard3)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
            $('#4steelButton').addClass('notselectedstell');
            $('#3steelButton').removeClass('notselectedstell');
		}else{
			$('#insurance').attr('disabled','disabled');
		}
	}

	function calc(){
		checkSettings()
		var clientsCount = $('.tourClientsElement').length;
		var insurance = 0;
		if( $('#insurance').is(':checked') ) {
		    insurance = 1;
        }
		var stellcount = $('#stellcount').val();
		if(!stellcount) stellcount = 0;
		var birthday1 = 0;
		var birthday2 = 0;
		var transib =  $('#transib').val();
		if(!transib) transib = 0;
		var needvisamongolia = 0;
		if( $('#visamongolia').is(':checked') ) needvisamongolia = 1;
		var needvisachina = 0;
		if( $('#visachina').is(':checked') ) needvisachina = 1;
		$('.participants-birthdate').each(function(index, element) {
			//alert(index)
			if(index == 0) birthday1 = $(this).val();
			if(index == 1) birthday2 = $(this).val();
		});
		if(!birthday1) birthday1 = 0;
		if(!birthday2) birthday2 = 0;
		$.post('/index.php?option=com_viaggio&task=calcTourCost&tour_id=<?php echo $this->item->id; ?>&stellcount='+stellcount+'&insurance='+insurance+'&count='+clientsCount+'&birthday2='+birthday2+'&birthday1='+birthday1+'&transib='+transib+'&needvisamongolia='+needvisamongolia+'&needvisachina='+needvisachina, function(data) {
            let insurance2 = 0;
		    if ($('#insurance2:checked').length)
                insurance2 = parseInt($('#insurance2:checked').attr('cost'));
		    if (clientsCount > 1)
            {
                let cost1up = (parseInt(data) + insurance2*clientsCount)/clientsCount;
                $('#cost1up').show();
                $('#cost1up').html(cost1up +'€');
                $('#cost1down').show();
                $('#cost2up').show();
                $('#cost2down').show();
                $('#cost3up').html(parseInt(data)+ insurance2*clientsCount+'€');
                $('#bottomText').show();
            }
            else
            {
                $('#cost1up').hide();
                $('#cost1down').hide();
                $('#cost2up').hide();
                $('#cost2down').hide();
                $('#cost3up').html(parseInt(data)+insurance2+'€');
                $('#bottomText').hide();
            }

		});
	}
});
</script>
 