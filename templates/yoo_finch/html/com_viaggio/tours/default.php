<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// подключение класса 
//jimport('joomla.html.pagination');
if(isset($_GET['test']))
{
    echo '<pre>';
    //print_r();
    echo '</pre>';
}
$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_viaggio') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tourform.xml');
$canEdit    = $user->authorise('core.edit', 'com_viaggio') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tourform.xml');
$canCheckin = $user->authorise('core.manage', 'com_viaggio');
$canChange  = $user->authorise('core.edit.state', 'com_viaggio');
$canDelete  = $user->authorise('core.delete', 'com_viaggio'); 
?>
<ol itemscope itemtype="http://schema.org/BreadcrumbList" class="uk-breadcrumb" style=" margin-top: -20px; ">
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a  itemprop="item"  href="/">
             <span itemprop="name">Home</span>
         <meta itemprop="position" content="1" />
        </a>
    </li>
    <?php foreach ($this->item->pathway as $k=>$row ) { ;?>

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo $row->link; ?>">
             <span itemprop="name"><?php echo $row->name; ?> </span>
         <meta itemprop="position" content="<?php echo $k+2; ?>" />
        </a>
    </li>
    <?php } ?>

    <?php
    // Filtering category by MENU
    $menu_category = 0;
    $params = 0;
    $app = JFactory::getApplication();
    // Get active menu
    $currentMenuItem = $app->getMenu()->getActive();
    if($currentMenuItem){
        // Get params for active menu
        $params = $currentMenuItem->params;
    }
    if($params)	{

        // Access param you want
        $menu_category = $params->get('cat_id', 0);
    }?><br>

</ol>
    <li class="uk-form">
        <input placeholder="  " class="uk-width-1-1" type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape((isset($_POST['filter_search'])?$_POST['filter_search']:'')); ?>" title="<?php echo JText::_('COM_CATEGORIES_ITEMS_SEARCH_FILTER'); ?>" />
    </li>
<script>
    jQuery( document ).ready(function() {
        var found_article = false;
        jQuery('#filter_search').on('keyup change mouseup',function () {
            var text = jQuery(this).val();
            jQuery('article').show();
            if (text.length > 2)
            {
                jQuery('article').each(function () {
                    let article = jQuery(this);
                    found_article = false;
                    jQuery(article.find('h2').text().split("\n")).each(function(){
                        if (this.toLowerCase().indexOf(text) > -1)
                            found_article = true;
                    });
                    if (!found_article)
                        article.hide();
                });
            }
        });
    });
</script>
<h1><?=$this->category->description?></h1>
<div class="uk-grid tm-leading-article" data-uk-grid-match="" data-uk-grid-margin="">
<div class="uk-width-medium-1-1 uk-row-first">  
<?php foreach ($this->items as $i => $item) : ?>
<article class="uk-article" uk-grid="" data-permalink="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>">
     <div  class="uk-grid  uk-panel  uk-panel-box uk-panel-hover " itemscope itemtype="http://schema.org/Event"> 
    <div class="uk-width-medium-3-10 uk-padding-remove  ">
<a  itemprop="url" href="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>" > <img itemprop="image" alt="<?php echo $item->name; ?>" title="<?php echo $item->name; ?>" class=" uk-align-left uk-border-rounded"  src="<?php if($item->category_img){ echo $item->category_img; }else{
		    echo '/components/com_viaggio/media/images/noimage.gif';
		} ?>" ></a>
</div>
 
	    <div class="uk-width-medium-7-10  ">		 
	 
 <a  class="uk-h2 uk-text-bold" style="color: #3a3a32;" href="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>" title="<?php echo $item->name; ?>">
     <h2 itemprop="name"  class="uk-text-center"> 	 <?php echo $item->name; ?></h2> </a>
<?php //print_r($item); ?>
<div class="uk-grid "><div class="uk-width-medium-1-2  uk-h3  "> 
<?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_MOSCA'); ?> <span class="uk-icon-plane rotate90 uk-icon-small"></span> <span  itemprop="startDate" content="<?php echo date('Y-m-d H:i', strtotime($item->from)); ?>"    > <?php echo date('d/m/Y', strtotime($item->from)); ?>  </span></div>
<div class="uk-h3 uk-width-medium-1-2"><?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_ITALIA'); ?> <span class="uk-icon-plane uk-icon-small"> </span> <span  itemprop="endDate" content="<?php echo date('Y-m-d H:i', strtotime($item->to)); ?>" > <?php echo date('d/m/Y', strtotime($item->to)); ?> </span> </div></div>

<!--
Duration:  <span class="uk-icon-sun-o"> </span>  7 days   <span class="uk-icon-moon-o"> </span>  8 notte  <br>
Hotels :            
         4 stell   ,  3 stell 
--><div itemprop="description" class="uk-text-justify  tm-block">
  <?php echo $item->desc; ?> 
 </div>
              <div  itemprop="location" itemscope itemtype="http://schema.org/Place">
    <h6 class="uk-text-justify" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
    <div   class="uk-hidden" title="<?php echo $item->name; ?>"   > <?php echo $item->name; ?></div>
    <div class="uk-hidden"  >Russia</div> 
           Citta` da visitare:  <span itemprop="name" ><?php  echo '
 '.str_replace(',',', ',$item->citys).''; ?></span> 
        </h6>
 
  </div>
	    

    
	<!--<p><a href="/mosca-sanpietroburgo/viaggio-mosca/capodanno?task=article.edit&amp;a_id=53&amp;return=aHR0cHM6Ly93d3cudmlhZ2dpby1ydXNzaWEuY29tL21vc2NhLXNhbnBpZXRyb2J1cmdv" title="Modifica articolo"><span class="hasTooltip icon-edit tip" title="" data-original-title="<strong>Modifica articolo</strong><br />Pubblicato<br />Martedì, 29 Novembre 2016<br />Scritto da Super User"></span>Modifica</a></p>-->
    <div class="uk-grid"  itemprop="offers" itemscope itemtype="http://schema.org/Offer" >
    <div class="uk-width-medium-1-3  ">
     
    
	 <a class="uk-h4 "  href="<?php
	    $subcategory = ViaggioHelpersFrontend::getCategory($item->catid);
	    echo $subcategory->path;
	?>"><?php
	    echo $subcategory->title;
	?></a>
        <br>
 <span class="uk-text-bottom"><?php echo ViaggioHelpersFrontend::detDuration($item->from,$item->to); ?> giorni </span>
     </div>
    <div class="uk-width-medium-1-3 uk-text-center"   >
	 <span   class="uk-text-muted uk-text-lowercase"><?php echo JText::_('COM_VIAGGIO_PREZZO_A_PERSONA_IN_STANZA_DOPPIA'); ?></span> <h3   style="color: #0063AF;" itemprop="price"  content="<?php echo ViaggioHelpersFrontend::getCost( $item->id, 0, 2 )/2; ?>"  class="uk-h1 uk-margin-remove  "> <?php echo ViaggioHelpersFrontend::getCost( $item->id, 0, 2 )/2; ?>  €   </h3>   <span itemprop="priceCurrency" content="EUR">
     </div>
     <div class="uk-width-medium-1-4 uk-text-right uk-text-bottom      "><br>
	<a itemprop="url" href="<?php echo JRoute::_('index.php?option=com_viaggio&view=showtour&id='.(int) $item->id); ?>" class="uk-button  uk-text-right uk-text-bottom uk-button-primary "  title="<?php echo $item->name; ?>"><?php echo JText::_('COM_VIAGGIO_LEGGI_TUTTO'); ?></a>
     </div>
     
      
     
    </div> </div>

</article>
<?php endforeach; ?>
</div>
 </div>
 
<?php //echo $this->pagination->getListFooter(); ?>
<div class="pagination">
    <p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
    <?php echo $this->pagination->getPagesLinks(); ?>
</div>